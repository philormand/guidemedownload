﻿using Microsoft.Extensions.Logging;
using MiloLibrary.Helpers.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DeskTopUi
{
    public class DownloadWrapper
    {
        private readonly IEosToGuideMe _eosToGuideMe;
        private readonly IHtmlToGuideMe _htmlToGuideMe;
        private readonly ILogger<DownloadWrapper> _logger;

        public DownloadWrapper(ILogger<DownloadWrapper> logger, IEosToGuideMe eosToGuideMe, IHtmlToGuideMe htmlToGuideMe)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eosToGuideMe = eosToGuideMe ?? throw new ArgumentNullException(nameof(eosToGuideMe));
            _htmlToGuideMe = htmlToGuideMe ?? throw new ArgumentNullException(nameof(htmlToGuideMe));
        }

        public async Task Run(Uri miloUri, string saveDirectory)
        {
            if (miloUri == null) throw new ArgumentNullException(nameof(miloUri));

            try
            {
                await _eosToGuideMe.GetTease(miloUri, saveDirectory).ConfigureAwait(false);
                await _htmlToGuideMe.GetTease(miloUri, saveDirectory).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                Exception ex1 = ex;
                while (ex1 != null)
                {
                    _logger.LogError($"Exception {ex1.GetType().ToString()} Message {ex1.Message} Trace {ex1.StackTrace}");
                    ex1 = ex1.InnerException;
                }
            }
        }
    }


}
