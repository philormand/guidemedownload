﻿using System;
using System.IO;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Logging.Serilog;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DeskTopUi
{
    class Program
    {
        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        public static void Main(string[] args)
        {
            BuildAvaloniaApp().Start(AppMain, args);
        }

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
        { 
            return AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToDebug();
        }

        // Your application's entry point. Here you can initialize your MVVM framework, DI
        // container, etc.
        private static void AppMain(Application app, string[] args)
        {
            var config = LoadConfiguration();
            //IMiloDeskTopConfig appConfig = config.GetSection("MiloDnl").Get<MiloDeskTopConfig>();

            var services = InjectorSetup.ConfigureServices(config);
            var serviceProvider = services.BuildServiceProvider();
            var serviceApp = serviceProvider.GetService<MainWindow>();
            app.Run(serviceApp);
        }

        public static IConfiguration LoadConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true,
                             reloadOnChange: true);
            return builder.Build();
        }


    }
}
