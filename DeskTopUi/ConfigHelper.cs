﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace DeskTopUi
{
    public static class ConfigHelper
    {
        public static void AddOrUpdateAppSetting<T>(string key, T value)
        {
            var filePath = Path.Combine(AppContext.BaseDirectory, "appsettings.json");
            dynamic jsonObj = JsonConvert.DeserializeObject(File.ReadAllText(filePath));

            var sectionPath = key.Split(":")[0];
            if (!string.IsNullOrEmpty(sectionPath))
            {
                var keyPath = key.Split(":")[1];
                jsonObj[sectionPath][keyPath] = value;
            }
            else
            {
                jsonObj[sectionPath] = value;
            }
            File.WriteAllText(filePath, JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented));

        }
    }
}
