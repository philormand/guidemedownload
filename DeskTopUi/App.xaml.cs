﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace DeskTopUi
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
