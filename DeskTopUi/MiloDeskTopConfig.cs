﻿namespace DeskTopUi
{
    public class MiloDeskTopConfig : IMiloDeskTopConfig
    {
        public int Timeout { get; set; }
        public string LogLevel { get; set; }

    }
}
