﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Threading;
using log4net;
using Microsoft.Extensions.Logging;
using MiloLibrary.Helpers.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace DeskTopUi
{
    public class MainWindow : Window
    {

        private readonly IEosToGuideMe _eosToGuideMe;
        private readonly IHtmlToGuideMe _htmlToGuideMe;
        private readonly ILogger<MainWindow> _logger;
        private readonly IMiloLibraryConfig _miloLibraryConfig;

        public MainWindow(ILogger<MainWindow> logger, IEosToGuideMe eosToGuideMe, IHtmlToGuideMe htmlToGuideMe, IMiloLibraryConfig miloLibraryConfig)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eosToGuideMe = eosToGuideMe ?? throw new ArgumentNullException(nameof(eosToGuideMe));
            _htmlToGuideMe = htmlToGuideMe ?? throw new ArgumentNullException(nameof(htmlToGuideMe));
            _miloLibraryConfig = miloLibraryConfig ?? throw new ArgumentNullException(nameof(miloLibraryConfig));
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            this.FindControl<TextBox>("DownLoadFolder").Text = _miloLibraryConfig.SaveDirectory;

            UiAppendar uiAppendar = new UiAppendar
            {
                _mainWindow = this
            };

            var root = ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly())).Root;
            root.AddAppender(uiAppendar);

        }
        private async void OnFolderChangeButtonClick(object sender, RoutedEventArgs e)
        {
            var folderDialog = new OpenFolderDialog
            {
                Title = "Choose output folder",
                InitialDirectory = this.FindControl<TextBox>("DownLoadFolder").Text
            };

            var result = await folderDialog.ShowAsync(this);
            if (result != null)
            {
                this.FindControl<TextBox>("DownLoadFolder").Text = result;
                ConfigHelper.AddOrUpdateAppSetting<string>("MiloLibrary:savedirectory", result);
            }


        }

        private async void OnDownloadButtonClick(object sender, RoutedEventArgs e)
        {
            this.FindControl<TextBox>("DebugOutput").Text = "";
            var miloUri = new Uri(this.FindControl<TextBox>("DownloadUrl").Text);
            if (miloUri == null) throw new ArgumentNullException(nameof(miloUri));

            string saveDirectory = this.FindControl<TextBox>("DownLoadFolder").Text;

            try
            {
                List<Task> tasks = new List<Task>
                {
                    _eosToGuideMe.GetTease(miloUri, saveDirectory),
                    _htmlToGuideMe.GetTease(miloUri, saveDirectory)
                };
                await Task.WhenAll(tasks);
                UpdateDebugOutput("Finished");
            }
            catch (Exception ex)
            {
                Exception ex1 = ex;
                while (ex1 != null)
                {
                    _logger.LogError($"Exception {ex1.GetType().ToString()} Message {ex1.Message} Trace {ex1.StackTrace}");
                    ex1 = ex1.InnerException;
                }
            }

        }

        public void UpdateDebugOutput(string message)
        {
            TextBox debugOutput = this.FindControl<TextBox>("DebugOutput");
            if (debugOutput.CheckAccess())
            {
                debugOutput.Text = $"{message}{Environment.NewLine}{debugOutput.Text}";
            }
            else 
            {
                Dispatcher.UIThread.InvokeAsync(delegate(){ debugOutput.Text = $"{message}{Environment.NewLine}{debugOutput.Text}"; });
            }
        }

    }
}
