﻿using Avalonia.Controls;
using log4net.Appender;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeskTopUi
{
    public class UiAppendar : AppenderSkeleton
    {
        public MainWindow _mainWindow;
        protected override void Append(LoggingEvent loggingEvent)
        {
            try
            {
                _mainWindow.UpdateDebugOutput($"{loggingEvent.RenderedMessage}");
            }
            catch
            {
                //swallow as we don't want an error updating the debug window to cause an issue with the main app
            }

        }
    }
}
