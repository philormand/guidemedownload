﻿namespace DeskTopUi
{
    public interface IMiloDeskTopConfig
    {
        int Timeout { get; set; }
        string LogLevel { get; set; }
    }
}