﻿using MiloLibrary.Helpers.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using HtmlAgilityPack;
using System.Net.Http;
using System.Linq;

namespace MiloLibrary.Helpers
{
    public class HtmlToGuideMe : IHtmlToGuideMe
    {
        private readonly XmlDocument _doc = new XmlDocument();
        private readonly IMediaDownloader _mediaDownloader;
        private readonly ILogger<HtmlToGuideMe> _logger;
        private readonly HttpClient _client;
        private string _saveDirectory;

        public HtmlToGuideMe(IMediaDownloader mediaDownloader, ILogger<HtmlToGuideMe> logger, HttpClient client)
        {
            _mediaDownloader = mediaDownloader ?? throw new ArgumentNullException(nameof(mediaDownloader));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task GetTease(Uri htmlUri, string saveDirectory)
        {
            _saveDirectory = saveDirectory;
            var htmlresponse = await _client.GetAsync(htmlUri).ConfigureAwait(false);
            var htmlcontent = await htmlresponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(htmlcontent);
            var docData = document.DocumentNode.SelectSingleNode("//body");
            if (docData.Attributes["class"].Value == "mini")
            {
                HtmlTease htmlTease = await ConvertTeaseHeaderToXml(htmlUri, saveDirectory).ConfigureAwait(false);
                string fileName = $"{_saveDirectory}{Path.DirectorySeparatorChar}{htmlTease.MediaDirectory}.xml";

                _logger.LogWarning($"HtmlTease {htmlTease.Title} Processed");
                SaveXml(htmlTease.Tease, fileName);
            }
        }

        private class HtmlTease
        {
            public string MediaDirectory { get; set; }
            public string Title { get; set; }
            public Tease Tease { get; set; }
        }

        private async Task<HtmlTease> ConvertTeaseHeaderToXml(Uri teaseUri, string saveDirectory)
        {
            if (teaseUri == null) throw new ArgumentNullException(nameof(teaseUri));

            string queryString = teaseUri.Query;
            var query = System.Web.HttpUtility.ParseQueryString(queryString);
            string teaseId = query.Get("id");

            var htmlresponse = await _client.GetAsync(teaseUri).ConfigureAwait(false);
            var htmlcontent = await htmlresponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(htmlcontent);
            var docData = document.DocumentNode.SelectSingleNode("//body");

            string titleText = docData.SelectSingleNode("//*[@id='tease_title']").FirstChild.InnerText.Trim();
            HtmlTease htmlTease = new HtmlTease
            {
                Title = titleText,
                MediaDirectory = MiscHelpers.SanitisePath(titleText)
            };
            Directory.CreateDirectory($"{saveDirectory}{Path.DirectorySeparatorChar}{htmlTease.MediaDirectory}");

            Tease tease = new Tease
            {
                id = teaseId,
                scriptVersion = @"v0.1"
            };

            var authorNode = docData.SelectSingleNode("//*[@id='tease_title']/span/a");

            TeaseAuthor author = GetTeaseAuthor(authorNode.Attributes["href"].Value.Split('=')[1], authorNode.InnerText);
            TeaseSettings settings = GetTeaseSettings();
            TeasePages teasePages = await GetTeasePages(document, $"{htmlTease.MediaDirectory}{Path.DirectorySeparatorChar}", teaseUri).ConfigureAwait(false);

            tease.Items = new object[]
                {
                    author,
                    htmlTease.MediaDirectory,
                    settings,
                    htmlTease.Title,
                    teaseUri.OriginalString,
                    teasePages
                };
            tease.ItemsElementName = new ItemsChoiceType3[]
                {
                    ItemsChoiceType3.Author,
                    ItemsChoiceType3.MediaDirectory,
                    ItemsChoiceType3.Settings,
                    ItemsChoiceType3.Title,
                    ItemsChoiceType3.Url,
                    ItemsChoiceType3.Pages
                };

            htmlTease.Tease = tease;

            return htmlTease;
        }

        private void SaveXml(Tease tease, string fullFilename)
        {
            XmlSerializer ser = new XmlSerializer(typeof(Tease));
            using (TextWriter writer = new StreamWriter(fullFilename))
            {
                ser.Serialize(writer, tease);
            }
            _logger.LogWarning($"Xml Written {fullFilename}");
        }

        private TeaseAuthor GetTeaseAuthor(string authorId, string authorName)
        {
            TeaseAuthor author = new TeaseAuthor() { id = authorId };
            author.Items = new string[] { authorName };
            author.ItemsElementName = new ItemsChoiceType[] { ItemsChoiceType.Name };
            return author;
        }

        private TeaseSettings GetTeaseSettings()
        {
            TeaseSettings settings = new TeaseSettings
            {
                Items = new bool[] { true },
                ItemsElementName = new ItemsChoiceType1[] { ItemsChoiceType1.AutoSetPageWhenSeen }
            };
            return settings;
        }

        private async Task<TeasePages> GetTeasePages(HtmlDocument document, string mediaDir, Uri teaseUri)
        {
            TeasePages teasePages = new TeasePages
            {
                Items = await GetTeasePagesPages(document, mediaDir, teaseUri).ConfigureAwait(false)
            };
            return teasePages;
        }

        private async Task<object[]> GetTeasePagesPages(HtmlDocument document, string mediaDir, Uri teaseUri)
        {
            Uri nextPageUri;
            List<object> teasePages = new List<object>();
            bool anotherPage = true;
            HtmlDocument nextDocument = document;
            int pageNo = 1;
            string thisPage = "start";
            string nextPage = $"Page{pageNo}";
            while (anotherPage)
            {
                var teaseDiv = nextDocument.DocumentNode.SelectSingleNode("//*[@id='cm_wide']");
                var page = GetPagesPage(nextDocument, thisPage, nextPage, mediaDir);
                teasePages.Add(page);

                var ended = teaseDiv.SelectSingleNode("//div[@class='item']/a[@name='rate']");
                if (ended != null)
                {
                    anotherPage = false;
                }
                else
                {
                    var continuePage = teaseDiv.SelectSingleNode("//*[@id='continue']");
                    var attribute = continuePage.Attributes["href"].Value;
                    var urlEnd = HtmlEntity.DeEntitize(attribute);
                    var url = $"https://{teaseUri.DnsSafeHost}{urlEnd.Replace("#t","")}"; //https://milovana.com/webteases/showtease.php?id=45103
                    nextPageUri = new Uri(url);
                    var htmlresponse = await _client.GetAsync(nextPageUri).ConfigureAwait(false);
                    var htmlcontent = await htmlresponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                    nextDocument.LoadHtml(htmlcontent);
                    thisPage = nextPage;
                    pageNo++;
                    nextPage = $"Page{pageNo}";
                }

            }

            return teasePages.ToArray();
        }

        private TeasePagesPage GetPagesPage(HtmlDocument document, string thisPage, string nextPage, string mediaDir)
        {
            HtmlDocument pageDocument = new HtmlDocument
            {
                OptionOutputAsXml = true
            };



            TeasePagesPage page = new TeasePagesPage
            {
                id = thisPage
            };
            List<object> items = new List<object>();
            List<ItemsChoiceType2> type = new List<ItemsChoiceType2>();
            var teaseDiv = document.DocumentNode.SelectSingleNode("//*[@id='cm_wide']");

            //Image
            var imageTag = teaseDiv.SelectSingleNode("//*[@class='tease_pic']");
            Uri imageUri = new Uri(imageTag.Attributes["src"].Value);
            string imageFile = $"{imageUri.LocalPath.Split('/').Last()}";
            string savePath = $"{_saveDirectory}{Path.DirectorySeparatorChar}{mediaDir}{imageFile}";
            _mediaDownloader.GetHtmlImage(savePath, imageUri);

            TeasePagesPageImage image = new TeasePagesPageImage()
            {
                id = imageFile
            };
            items.Add(image);
            type.Add(ItemsChoiceType2.Image);


            //Text
            List<XmlElement> elements = new List<XmlElement>();
            string plainText = HtmlEntity.DeEntitize(teaseDiv.SelectSingleNode("//*[@id='tease_content']/p[@class='text']").OuterHtml);
            pageDocument.LoadHtml(plainText);
            string xml = pageDocument.DocumentNode.OuterHtml;
            _doc.LoadXml(xml);
            elements.Add(_doc.DocumentElement);
            TeasePagesPageText text = new TeasePagesPageText()
            {
                Items = elements.ToArray()
            };
            items.Add(text);
            type.Add(ItemsChoiceType2.Text);

            //Button
            var ended = teaseDiv.SelectSingleNode("//div[@class='item']/a[@name='rate']");
            if (ended == null)
            {
                TeasePagesPageButton button = new TeasePagesPageButton()
                {
                    Value = "Continue",
                    target = nextPage
                };
                items.Add(button);
                type.Add(ItemsChoiceType2.Button);
            }

            page.Items = items.ToArray();
            page.ItemsElementName = type.ToArray();
            return page;
        }
    }
}

