﻿namespace MiloLibrary.Helpers.Interfaces
{
    public interface IMiloLibraryConfig
    {
        int Delay { get; set; }
        string SaveDirectory { get; set; }
    }
}