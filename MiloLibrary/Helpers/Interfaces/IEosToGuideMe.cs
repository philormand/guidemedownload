﻿using System;
using System.Threading.Tasks;

namespace MiloLibrary.Helpers.Interfaces
{
    public interface IEosToGuideMe
    {
        Task GetTease(Uri eosUri, string saveDirectory);
    }
}