﻿using MiloLibrary.Json;
using System;
using System.Threading.Tasks;

namespace MiloLibrary.Helpers.Interfaces
{
    public interface IEosParser
    {
        Task<TeaseData> GetTeaseData(Uri teaseUri, string jsonPath);
    }
}