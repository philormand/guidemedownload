﻿using MiloLibrary.Json;
using System;
using System.Threading.Tasks;

namespace MiloLibrary.Helpers.Interfaces
{
    public interface IMediaDownloader
    {
        Task DownloadFiles(TeaseData teaseData, string directory);
        Task DownloadImages(TeaseData teaseData, string directory);
        Task GetHtmlImage(string filePath, Uri imageUri);

    }
}