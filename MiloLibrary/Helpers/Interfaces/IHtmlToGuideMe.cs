﻿using System;
using System.Threading.Tasks;

namespace MiloLibrary.Helpers.Interfaces
{
    public interface IHtmlToGuideMe
    {
        Task GetTease(Uri htmlUri, string saveDirectory);
    }
}