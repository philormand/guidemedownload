﻿using MiloLibrary.Json;
using System.IO;
using System.Linq;

namespace MiloLibrary.Helpers
{
    public static class MiscHelpers
    {
        public static string SanitisePath(string path)
        {
            var excludes = Path.GetInvalidPathChars().Concat(new[] { ':', '*', '?', ' ', '\\', '/', '.' });
            foreach (var item in excludes)
            {
                switch (item)
                {
                    default:
                        path = path.Replace(item, '-');
                        break;
                }
            }
            return StripMulipleMinus(path);
        }

        public static string StripMulipleMinus(string original)
        {
            while (original.IndexOf("--") > 0)
            {
                original = original.Replace("--", "-");
            }
            return original;
        }
    }
}
