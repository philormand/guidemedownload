﻿using HtmlAgilityPack;
using MiloLibrary.Helpers.Interfaces;
using MiloLibrary.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Action = MiloLibrary.Json.Action;

namespace MiloLibrary.Helpers
{
    public class EosParser : IEosParser
    {
        private static readonly string _eosUrl = @"https://milovana.com/webteases/geteosscript.php?id=";
        private readonly HttpClient _client;

        public EosParser(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<TeaseData> GetTeaseData(Uri teaseUri, string jsonPath)
        {
            if (teaseUri == null) throw new ArgumentNullException(nameof(teaseUri));

            string queryString = teaseUri.Query;
            var query = System.Web.HttpUtility.ParseQueryString(queryString);
            string teaseId = query.Get("id");

            var htmlresponse = await _client.GetAsync(teaseUri).ConfigureAwait(false);
            var htmlcontent = await htmlresponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(htmlcontent);
            var docData = document.DocumentNode.SelectSingleNode("//body");

            Uri jsonUri = new Uri($"{_eosUrl}{teaseId}");
            var jsonresponse = await _client.GetAsync(jsonUri).ConfigureAwait(false);
            var jsoncontent = await jsonresponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            JObject teaseJson = (JObject)JsonConvert.DeserializeObject(jsoncontent);

            var teaseData = new TeaseData()
            {
                TeaseUrl = new Uri(teaseUri.OriginalString),
                TeaseId = docData.Attributes["data-tease-id"].Value,
                AuthorName = docData.Attributes["data-author"].Value,
                AuthorId = docData.Attributes["data-author-id"].Value,
                Title = docData.Attributes["data-title"].Value,
                MediaDirectory = MiscHelpers.SanitisePath(docData.Attributes["data-title"].Value),
            };

            string formattedJson = JsonConvert.SerializeObject(JsonConvert.DeserializeObject<JObject>(jsoncontent), Formatting.Indented);
            using (StreamWriter file = new StreamWriter($"{jsonPath}{Path.DirectorySeparatorChar}{teaseData.MediaDirectory}.json"))
            {
                file.WriteLine(formattedJson);
            }

            ProcessPages(teaseData.Pages, teaseJson.GetValue("pages", StringComparison.InvariantCulture).ToObject<JObject>());
            var galleriesNode = teaseJson.GetValue("galleries", StringComparison.InvariantCulture);
            if (galleriesNode != null)
            {
                ProcessGalleries(teaseData.Galleries, galleriesNode.ToObject<JObject>());
            }
            ProcessFiles(teaseData.Files, teaseJson.GetValue("files", StringComparison.InvariantCulture).ToObject<JObject>());

            string processedJson = JsonConvert.SerializeObject(teaseData, ShouldSerializeContractResolver.JsonSettings);
            using (StreamWriter file = new StreamWriter($"{jsonPath}{Path.DirectorySeparatorChar}{teaseData.MediaDirectory}Processed.json"))
            {
                file.WriteLine(processedJson);
            }
            return teaseData;
        }

        public void ProcessPages(List<Page> pages, JObject pagesArray)
        {
            if (pagesArray is null) throw new ArgumentNullException(nameof(pagesArray));
            if (pages is null) throw new ArgumentNullException(nameof(pages));

            foreach (var keyValuePair in pagesArray)
            {
                if (!string.IsNullOrWhiteSpace(keyValuePair.Key))
                {
                    var page = new Page()
                    {
                        Id = keyValuePair.Key
                    };
                    JArray array = JArray.FromObject(keyValuePair.Value);
                    List<EosUI> eosUi = new List<EosUI>();
                    List<Timer> timers = new List<Timer>();
                    foreach (JObject item in array)
                    {
                        if (item.ContainsKey("image"))
                        {
                            var image = item.GetValue("image", StringComparison.InvariantCulture).ToObject<EosUI>();
                            image.Type = "image";
                            eosUi.Add(image);
                        }
                        else if (item.ContainsKey("say"))
                        {
                            var say = item.GetValue("say", StringComparison.InvariantCulture).ToObject<EosUI>();
                            say.Type = "say";
                            eosUi.Add(say);
                        }
                        else if (item.ContainsKey("audio.play"))
                        {
                            page.Audioplay = item.GetValue("audio.play", StringComparison.InvariantCulture).ToObject<AudioPlay>();
                        }
                        else if (item.ContainsKey("choice"))
                        {
                            page.Choice = item.GetValue("choice", StringComparison.InvariantCulture).ToObject<Choice>();
                        }
                        else if (item.ContainsKey("timer"))
                        {
                            timers.Add(item.GetValue("timer", StringComparison.InvariantCulture).ToObject<Timer>());
                        }
                        else if (item.ContainsKey("goto"))
                        {
                            var tgoto = item.GetValue("goto", StringComparison.InvariantCulture).ToObject<EosUI>();
                            tgoto.Type = "goto";
                            eosUi.Add(tgoto);
                        }
                        else if (item.ContainsKey("nyx.page"))
                        {
                            page.NyxPage = item.GetValue("nyx.page", StringComparison.InvariantCulture).ToObject<NyxPage>();
                        }

                    }

                    page.Timers = timers.ToArray();
                    page.EosUis = eosUi.ToArray();

                    pages.Add(page);
                }
            }
        }

        public void ProcessGalleries(List<Gallery> galleries, JObject galleriesObject)
        {
            if (galleriesObject is null) throw new ArgumentNullException(nameof(galleriesObject));
            if (galleries is null) throw new ArgumentNullException(nameof(galleries));

            foreach (var keyValuePair in galleriesObject)
            {
                var gallery = new Gallery()
                {
                    Guid = keyValuePair.Key
                };
                JObject galleryObject = JObject.FromObject(keyValuePair.Value);
                gallery.Name = galleryObject.GetValue("name", StringComparison.InvariantCulture).ToString();
                gallery.Images = galleryObject.GetValue("images", StringComparison.InvariantCulture).ToObject<GalleryImage[]>();

                galleries.Add(gallery);
            }
        }

        public void ProcessFiles(List<EosFile> eosFiles, JObject eosFilesObject)
        {
            if (eosFilesObject is null) throw new ArgumentNullException(nameof(eosFilesObject));
            if (eosFiles is null) throw new ArgumentNullException(nameof(eosFiles));

            foreach (var keyValuePair in eosFilesObject)
            {
                var eosFile = new EosFile()
                {
                    Name = keyValuePair.Key
                };
                JObject galleryObject = JObject.FromObject(keyValuePair.Value);
                eosFile.Id = (int)galleryObject.GetValue("id", StringComparison.InvariantCulture);
                eosFile.Hash = galleryObject.GetValue("hash", StringComparison.InvariantCulture).ToString();
                eosFile.Size = (int)galleryObject.GetValue("size", StringComparison.InvariantCulture);
                eosFile.Type = galleryObject.GetValue("type", StringComparison.InvariantCulture).ToString();
                var height = galleryObject.GetValue("height", StringComparison.InvariantCulture);
                if (height != null)
                {
                    eosFile.Height = (int)height;
                }
                var width = galleryObject.GetValue("width", StringComparison.InvariantCulture);
                if (width != null)
                {
                    eosFile.Width = (int)width;
                }

                eosFiles.Add(eosFile);
            }
        }

    }
}
