﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace MiloLibrary.Helpers
{
    [Serializable]
    public class MediaDownloaderException : Exception
    {
        public string ResourceReferenceProperty { get; set; }

        public MediaDownloaderException()
        {
        }

        public MediaDownloaderException(string message)
            : base(message)
        {
        }

        public MediaDownloaderException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected MediaDownloaderException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            ResourceReferenceProperty = info.GetString("ResourceReferenceProperty");
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            info.AddValue("ResourceReferenceProperty", ResourceReferenceProperty);
            base.GetObjectData(info, context);
        }

    }


}
