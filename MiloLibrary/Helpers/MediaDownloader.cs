﻿using MiloLibrary.Helpers.Interfaces;
using MiloLibrary.Json;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace MiloLibrary.Helpers
{
    public class MediaDownloader : IMediaDownloader
    {
        private readonly HttpClient _httpClient;
        private readonly IMiloLibraryConfig _config;
        private readonly ILogger<MediaDownloader> _logger;

        public MediaDownloader(HttpClient httpClient, IMiloLibraryConfig config, ILogger<MediaDownloader> logger)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _config = config ?? throw new ArgumentNullException(nameof(config));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Task DownloadImages(TeaseData teaseData, string directory)
        {
            if (teaseData == null) throw new ArgumentNullException(nameof(teaseData));

            List<Task> tasks = new List<Task>();
            int delay = 0;

            string mediaDirectory = $"{directory}{Path.DirectorySeparatorChar}{teaseData.MediaDirectory}";
            Directory.CreateDirectory(mediaDirectory);

            foreach (var gallery in teaseData.Galleries)
            {
                string galleryDirectory = $"{mediaDirectory}{Path.DirectorySeparatorChar}{gallery.Guid}";
                Directory.CreateDirectory(galleryDirectory);
                foreach (var item in gallery.Images)
                {
                    string filePath = $"{galleryDirectory}{Path.DirectorySeparatorChar}{item.Id}.jpg";
                    if (!File.Exists(filePath))
                    {
                        delay += _config.Delay;
                        tasks.Add(GetImage(filePath, item, delay));
                    }

                }
            }
            return Task.WhenAll(tasks);
        }

        public async Task GetImage(string filePath, Json.GalleryImage item, int delay)
        {
            await Task.Delay(delay);
            Uri imageUri = new Uri($"https://media.milovana.com/timg/tb_xl/{item.Hash}.jpg?origin=https://eosscript.com");
            try
            {
                using (var image = await _httpClient.GetStreamAsync(imageUri).ConfigureAwait(false))
                {
                    using (var fileStream = File.Create(filePath))
                    {
                        await image.CopyToAsync(fileStream).ConfigureAwait(false);
                        _logger.LogWarning($"Downloaded {filePath}");
                    }
                }
            }
            catch (Exception ex)
            {
                var error = new MediaDownloaderException($"Failed to Download {imageUri} for {filePath}", ex);
                _logger.LogError(error, "", null);
            }

        }


        public Task DownloadFiles(TeaseData teaseData, string directory)
        {
            if (teaseData == null) throw new ArgumentNullException(nameof(teaseData));

            List<Task> tasks = new List<Task>();
            int delay = 0;

            string mediaDirectory = $"{directory}{Path.DirectorySeparatorChar}{teaseData.MediaDirectory}";
            Directory.CreateDirectory(mediaDirectory);

            foreach (var item in teaseData.Files)
            {
                string filePath = $"{mediaDirectory}{Path.DirectorySeparatorChar}{item.Name}";
                if (!File.Exists(filePath))
                {
                    delay += _config.Delay;
                    tasks.Add(GetFile(filePath, item, delay));
                }

            }
            return Task.WhenAll(tasks);
        }

        public async Task GetFile(string filePath, Json.EosFile item, int delay)
        {
            await Task.Delay(delay);
            var fileExtension = GetExtension(item.Type);
            Uri fileUri = new Uri($"https://media.milovana.com/timg/{item.Hash}{fileExtension}?origin=https://eosscript.com");
            try
            {
                using (var image = await _httpClient.GetStreamAsync(fileUri).ConfigureAwait(false))
                {
                    using (var fileStream = File.Create(filePath))
                    {
                        await image.CopyToAsync(fileStream).ConfigureAwait(false);
                        _logger.LogWarning($"Downloaded {filePath}");
                    }
                }
            }
            catch (Exception ex)
            {
                var error = new MediaDownloaderException($"Failed to Download {fileUri} for {filePath}", ex);
                _logger.LogError(error, "", null);
            }
        }

        public async Task GetHtmlImage(string filePath, Uri imageUri)
        {
            try
            {
                if (!File.Exists(filePath))
                { 
                    using (var image = await _httpClient.GetStreamAsync(imageUri).ConfigureAwait(false))
                    {
                        using (var fileStream = File.Create(filePath))
                        {
                            await image.CopyToAsync(fileStream).ConfigureAwait(false);
                            _logger.LogWarning($"Downloaded {filePath}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var error = new MediaDownloaderException($"Failed to Download {imageUri} for {filePath}", ex);
                _logger.LogError(error, "", null);
            }

        }

        private static string GetExtension(string contentType)
        {
            return MimeTypes.MimeTypeMap.GetExtension(contentType);
        }

    }
}
