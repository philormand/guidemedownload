﻿using MiloLibrary.Helpers.Interfaces;
using MiloLibrary.Json;
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Net.Http;
using System.Linq;

namespace MiloLibrary.Helpers
{
    public class EosToGuideMe : IEosToGuideMe
    {

        private readonly XmlDocument _doc = new XmlDocument();
        private readonly IMediaDownloader _mediaDownloader;
        private readonly IEosParser _eosParser;
        private readonly ILogger<EosToGuideMe> _logger;
        private readonly HttpClient _client;

        public EosToGuideMe(IMediaDownloader mediaDownloader, IEosParser eosParser, ILogger<EosToGuideMe> logger, HttpClient client)
        {
            _mediaDownloader = mediaDownloader ?? throw new ArgumentNullException(nameof(mediaDownloader));
            _eosParser = eosParser ?? throw new ArgumentNullException(nameof(eosParser));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }
        public async Task GetTease(Uri eosUri, string saveDirectory)
        {
            var htmlresponse = await _client.GetAsync(eosUri).ConfigureAwait(false);
            var htmlcontent = await htmlresponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(htmlcontent);
            var docData = document.DocumentNode.SelectSingleNode("//body");
            if (docData.Attributes["class"].Value == "eosTopBody")
            {
                TeaseData teaseData = await _eosParser.GetTeaseData(eosUri, saveDirectory).ConfigureAwait(false);
                Tease tease = ConvertTeaseDataToXml(teaseData);
                string fileName = $"{saveDirectory}{Path.DirectorySeparatorChar}{teaseData.MediaDirectory}.xml";

                _logger.LogWarning($"EosTease{teaseData.Title} Processed");
                List<Task> tasks = new List<Task>
                {
                    _mediaDownloader.DownloadImages(teaseData, saveDirectory),
                    _mediaDownloader.DownloadFiles(teaseData, saveDirectory),
                    Task.Run(() => SaveXml(tease, fileName))
                };
                await Task.WhenAll(tasks).ConfigureAwait(false);
            }
        }

        public Tease ConvertTeaseDataToXml(TeaseData teaseData)
        {
            if (teaseData == null) throw new ArgumentNullException(nameof(teaseData));

            Tease tease = new Tease
            {
                id = teaseData.TeaseId,
                scriptVersion = @"v0.1"
            };

            TeaseAuthor author = GetTeaseAuthor(teaseData);
            TeaseSettings settings = GetTeaseSettings();
            TeasePages teasePages = GetTeasePages(teaseData);


            tease.Items = new object[]
                {
                    author,
                    teaseData.MediaDirectory,
                    settings,
                    teaseData.Title,
                    teaseData.TeaseUrl.OriginalString,
                    teasePages
                };
            tease.ItemsElementName = new ItemsChoiceType3[]
                {
                    ItemsChoiceType3.Author,
                    ItemsChoiceType3.MediaDirectory,
                    ItemsChoiceType3.Settings,
                    ItemsChoiceType3.Title,
                    ItemsChoiceType3.Url,
                    ItemsChoiceType3.Pages
                };
            return tease;
        }

        public void SaveXml(Tease tease, string fullFilename)
        {
            System.IO.File.WriteAllText(fullFilename, tease.Serialize());
            _logger.LogWarning($"Xml Written {fullFilename}");
        }

        public TeaseAuthor GetTeaseAuthor(TeaseData teaseData)
        {
            TeaseAuthor author = new TeaseAuthor() { id = teaseData.AuthorId };
            author.Items = new string[] { teaseData.AuthorName };
            author.ItemsElementName = new ItemsChoiceType[] { ItemsChoiceType.Name };
            return author;
        }

        public TeaseSettings GetTeaseSettings()
        {
            TeaseSettings settings = new TeaseSettings
            {
                Items = new bool[] { true },
                ItemsElementName = new ItemsChoiceType1[] { ItemsChoiceType1.AutoSetPageWhenSeen }
            };
            return settings;
        }

        public TeasePages GetTeasePages(TeaseData teaseData)
        {
            TeasePages teasePages = new TeasePages
            {
                Items = GetTeasePagesPages(teaseData)
            };
            return teasePages;
        }

        public TeasePagesPage[] GetTeasePagesPages(TeaseData teaseData)
        {
            List<TeasePagesPage> teasePages = new List<TeasePagesPage>();
            foreach (var page in teaseData.Pages)
            {
                teasePages.Add(GetPagesPage(page));
            }
            return teasePages.ToArray();
        }

        public TeasePagesPage GetPagesPage(Json.Page jpage)
        {

            if (jpage.NyxPage != null)
            {
                return GetNyxPage(jpage);
            }
            else
            {
                return GetEosPage(jpage);
            }


        }

        private TeasePagesPage GetEosPage(Json.Page jpage)
        {

            HtmlDocument document = new HtmlDocument
            {
                OptionOutputAsXml = true
            };

            TeasePagesPage page = new TeasePagesPage
            {
                id = jpage.Id
            };


            List<object> items = new List<object>();
            List<ItemsChoiceType2> type = new List<ItemsChoiceType2>();
            if (jpage.EosUis.GetLength(0) > 0)
            {
                foreach (var eosUI in jpage.EosUis)
                {
                    if (eosUI.Type == "image")
                    {
                        TeasePagesPageEosUI pageEosUI = new TeasePagesPageEosUI();
                        var locator = eosUI.Locator.Split(':');
                        pageEosUI.id = $"{locator[1]}.jpg";
                        pageEosUI.type = "image";
                        items.Add(pageEosUI);
                        type.Add(ItemsChoiceType2.EosUI);
                    }
                    if (eosUI.Type == "say")
                    {
                        TeasePagesPageEosUI pageEosUI = new TeasePagesPageEosUI();
                        List<XmlElement> elements = new List<XmlElement>();
                        string plainText = HtmlEntity.DeEntitize(eosUI.Label);
                        document.LoadHtml(plainText);
                        string xml = document.DocumentNode.OuterHtml;
                        _doc.LoadXml(xml);
                        elements.Add(_doc.DocumentElement);
                        pageEosUI.Items = elements.ToArray();
                        pageEosUI.type = "text";
                        pageEosUI.mode = eosUI.Mode;
                        items.Add(pageEosUI);
                        type.Add(ItemsChoiceType2.EosUI);
                    }
                    if (eosUI.Type == "goto")
                    {
                        TeasePagesPageEosUI pageEosUI = new TeasePagesPageEosUI();
                        pageEosUI.target = eosUI.Target;
                        pageEosUI.type = "goto";
                        items.Add(pageEosUI);
                        type.Add(ItemsChoiceType2.EosUI);
                    }
                }
            }

            if (jpage.Choice != null)
            {
                int sortOrder = 0;
                foreach (var option in jpage.Choice.Options)
                {
                    string target = "";
                    foreach (var item in option.Commands)
                    {
                        if (item.EosGoto?.Target != null)
                        {
                            target = item.EosGoto.Target;
                        }
                    }
                    TeasePagesPageButton button = new TeasePagesPageButton()
                    {
                        Value = option.Label,
                        target = target
                    };
                    if (option.Color != null)
                    {
                        button.bgColor1 = option.Color;
                        button.bgColor2 = option.Color;
                    }
                    else
                    {
                        button.bgColor1 = "#000000";
                        button.bgColor2 = "#000000";
                    }
                    button.fontColor = "#FFFFFF";
                    sortOrder++;
                    button.sortOrder = sortOrder.ToString();
                    items.Add(button);
                    type.Add(ItemsChoiceType2.Button);
                }
            }

            /*
            if (jpage.EosGoto != null)
            {
                TeasePagesPageButton button = new TeasePagesPageButton()
                {
                    Value = "Continue",
                    target = jpage.EosGoto.Target
                };
                button.bgColor1 = "#000000";
                button.bgColor2 = "#000000";
                button.fontColor = "#FFFFFF";
                items.Add(button);
                type.Add(ItemsChoiceType2.Button);
            }
            */

            if (jpage.Audioplay != null)
            {
                TeasePagesPageAudio audio = new TeasePagesPageAudio()
                {
                    id = $"{jpage.Audioplay.Locator.Replace(@"file:", "")}"
                };
                if (jpage.Audioplay.Volume != 0)
                {
                    audio.volume = (jpage.Audioplay.Volume * 100).ToString();
                }
                items.Add(audio);
                type.Add(ItemsChoiceType2.Audio);
            }

            if (jpage.Timers.GetLength(0) > 0)
            {
                foreach (var jtimer in jpage.Timers)
                {
                    if (jtimer.Duration != null)
                    {
                        string seconds = jtimer.Duration.Replace("s", "");
                        if (jtimer.Commands != null)
                        {
                            foreach (var command in jtimer.Commands)
                            {

                                if (command.EosGoto != null)
                                {
                                    TeasePagesPageDelay delay = new TeasePagesPageDelay
                                    {
                                        seconds = seconds,
                                        target = command.EosGoto.Target
                                    };
                                    if (Enum.TryParse("Active", out TeasePagesPageDelayStyle style))
                                    {
                                        delay.style = style;
                                    }
                                    else
                                    {
                                        delay.style = TeasePagesPageDelayStyle.secret;
                                    }
                                    items.Add(delay);
                                    type.Add(ItemsChoiceType2.Delay);
                                }

                                if (command.Image != null)
                                {
                                    TeasePagesPageTimer timer = new TeasePagesPageTimer
                                    {
                                        seconds = seconds
                                    };
                                    var locator = command.Image.Locator.Split('/');
                                    timer.imageId = $"{locator[1]}.jpg";
                                    items.Add(timer);
                                    type.Add(ItemsChoiceType2.Timer);
                                }

                                if (command.Say != null)
                                {
                                    TeasePagesPageTimer timer = new TeasePagesPageTimer
                                    {
                                        seconds = seconds
                                    };
                                    string plainText = HtmlEntity.DeEntitize(command.Say.Label);
                                    document.LoadHtml(plainText);
                                    timer.Value = document.DocumentNode.InnerHtml;
                                    items.Add(timer);
                                    type.Add(ItemsChoiceType2.Timer);
                                }
                            }
                        }
                        else
                        {
                            _logger.LogWarning($"Not Processed for {jpage.Id} jpage.Timer.Commands is null");
                        }
                    }
                    else
                    {
                        _logger.LogWarning($"Not Processed for {jpage.Id} jpage.Duration is null");
                    }
                }

            }


            page.Items = items.ToArray();
            page.ItemsElementName = type.ToArray();

            return page;
        }

        private TeasePagesPage GetNyxPage(Json.Page jpage)
        {
            HtmlDocument document = new HtmlDocument
            {
                OptionOutputAsXml = true
            };

            TeasePagesPage page = new TeasePagesPage
            {
                id = jpage.Id
            };


            List<object> items = new List<object>();
            List<ItemsChoiceType2> type = new List<ItemsChoiceType2>();

            if (!string.IsNullOrWhiteSpace(jpage.NyxPage.Text))
            {
                List<XmlElement> elements = new List<XmlElement>();
                string plainText = $"<div>{jpage.NyxPage.Text}</div>";
                document.LoadHtml(plainText);
                string xml = document.DocumentNode.OuterHtml;
                _doc.LoadXml(xml);
                elements.Add(_doc.DocumentElement);
                TeasePagesPageText text = new TeasePagesPageText()
                {
                    Items = elements.ToArray()
                };
                items.Add(text);
                type.Add(ItemsChoiceType2.Text);
            }

            if (jpage.NyxPage.Media != null)
            {
                var locator = jpage.NyxPage.Media.Nyximage.Split(':');
                TeasePagesPageImage image = new TeasePagesPageImage()
                {
                    id = locator[1]
                };
                items.Add(image);
                type.Add(ItemsChoiceType2.Image);
            }

            if (jpage.NyxPage.Action?.NyxButtons != null)
            {
                int sortOrder = 0;
                foreach (var option in jpage.NyxPage.Action.NyxButtons)
                {
                    sortOrder++;
                    items.Add(GetNyxButton(option, sortOrder));
                    type.Add(ItemsChoiceType2.Button);
                }
            }

            if (jpage.NyxPage.Instruc?.NyxButtons != null)
            {
                int sortOrder = 0;
                foreach (var option in jpage.NyxPage.Instruc.NyxButtons)
                {
                    sortOrder++;
                    items.Add(GetNyxButton(option, sortOrder));
                    type.Add(ItemsChoiceType2.Button);
                }
            }

            if (jpage.NyxPage.Hidden?.Audioplay != null)
            {
                items.Add(GetAudio(jpage.NyxPage.Hidden.Audioplay));
                type.Add(ItemsChoiceType2.Audio);
            }

            if (jpage.NyxPage.Hidden?.Set != null)
            {
                page.set = ConvertPagesArray(jpage.NyxPage.Hidden.Set.Pages);
            }

            if (jpage.NyxPage.Hidden?.Unset != null)
            {
                page.unset = ConvertPagesArray(jpage.NyxPage.Hidden.Unset.Pages);
            }

            if (jpage.NyxPage.Action?.AudioPlay != null)
            {
                items.Add(GetAudio(jpage.NyxPage.Action.AudioPlay));
                type.Add(ItemsChoiceType2.Audio);
            }

            if (jpage.NyxPage.Instruc?.nyxtimer != null)
            {
                items.Add(GetDelay(jpage.NyxPage.Instruc.nyxtimer));
                type.Add(ItemsChoiceType2.Delay);
            }

            if (jpage.NyxPage.Instruc?.Set != null)
            {
                page.set = ConvertPagesArray(jpage.NyxPage.Instruc.Set.Pages);
            }

            if (jpage.NyxPage.Instruc?.Unset != null)
            {
                page.unset = ConvertPagesArray(jpage.NyxPage.Instruc.Unset.Pages);
            }

            if (jpage.NyxPage.Action?.NyxTimer != null)
            {
                items.Add(GetDelay(jpage.NyxPage.Action.NyxTimer));
                type.Add(ItemsChoiceType2.Delay);
            }

            if (jpage.NyxPage.Action?.NyxVert != null)
            {
                foreach (var element in jpage.NyxPage.Action.NyxVert.Elements)
                {
                    if (element.Nyxbuttons != null)
                    {
                        int sortOrder = 0;
                        foreach (var option in element.Nyxbuttons)
                        {
                            sortOrder++;
                            items.Add(GetNyxButton(option, sortOrder));
                            type.Add(ItemsChoiceType2.Button);
                        }
                    }
                    if (element.NyxTimer != null)
                    {
                        items.Add(GetDelay(element.NyxTimer));
                        type.Add(ItemsChoiceType2.Delay);
                    }

                }
            }

            page.Items = items.ToArray();
            page.ItemsElementName = type.ToArray();

            return page;
        }

        private TeasePagesPageButton GetNyxButton(NyxButton button, int sortOrder)
        {
            string target = "";
            foreach (var item in button.Commands)
            {
                if (item.EosGoto?.Target != null)
                {
                    target = item.EosGoto.Target;
                }
            }
            TeasePagesPageButton tbutton = new TeasePagesPageButton()
            {
                Value = button.Label,
                target = target
            };
            tbutton.sortOrder = sortOrder.ToString();
            return tbutton;

        }

        private TeasePagesPageDelay GetDelay(NyxTimer timer)
        {
            if (int.TryParse(timer.Duration.Replace("ms", ""), out int seconds))
            {
                seconds = seconds / 1000;
            }
            else
            {
                seconds = 30;
            }

            string target = "";
            if (timer.Commands[0].Range != null)
            {
                target = ConvertRange(timer.Commands[0].Range);
            }
            if (timer.Commands[0].EosGoto != null)
            {
                target = timer.Commands[0].EosGoto.Target;
            }

            TeasePagesPageDelay delay = new TeasePagesPageDelay
            {
                seconds = seconds.ToString(),
                target = target
            };
            if (Enum.TryParse(timer.Style, out TeasePagesPageDelayStyle style))
            {
                delay.style = style;
            }
            else
            {
                delay.style = TeasePagesPageDelayStyle.secret;
            }
            return delay;
        }

        private TeasePagesPageAudio GetAudio(AudioPlay audioPlay)
        {
            TeasePagesPageAudio audio = new TeasePagesPageAudio()
            {
                id = $"{audioPlay.Locator.Replace(@"file:", "")}"
            };
            return audio;
        }

        private string ConvertPagesArray(string[] arrayIn)
        {
            return string.Join(",", arrayIn).Replace('#', ',');
        }

        private string ConvertRange(Range range)
        {
            return $"({range.From}..{range.To})";
        }



    }
}
