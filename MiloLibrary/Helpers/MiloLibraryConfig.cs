﻿using MiloLibrary.Helpers.Interfaces;

namespace MiloLibrary.Helpers
{
    public class MiloLibraryConfig : IMiloLibraryConfig
    {
        public int Delay { get; set; }
        public string SaveDirectory { get; set; }

    }
}
