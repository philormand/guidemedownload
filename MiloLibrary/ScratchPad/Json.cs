﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace MiloLibrary.ScratchPad
{

    public class Rootobject
    {
        public Modules modules { get; set; }
        public Files files { get; set; }
        public Pages pages { get; set; }
    }

    public class Modules
    {
        public Nyx nyx { get; set; }
        public Pcm2 pcm2 { get; set; }
        public Audio audio { get; set; }
    }

    public class Nyx
    {
    }

    public class Pcm2
    {
    }

    public class Audio
    {
    }

    public class Files
    {
        public BlankJpg blankjpg { get; set; }
        public Coldshower1Jpg coldshower1jpg { get; set; }
        public Coldshower2Jpg coldshower2jpg { get; set; }
        public Coldshower3Jpg coldshower3jpg { get; set; }
        public Doggy1Jpg doggy1jpg { get; set; }
        public Doggy3Jpg doggy3jpg { get; set; }
        public Downstairs31Jpg downstairs31jpg { get; set; }
        public Downstairs310Jpg downstairs310jpg { get; set; }
        public Downstairs311Jpg downstairs311jpg { get; set; }
        public Downstairs312Jpg downstairs312jpg { get; set; }
        public Downstairs313Jpg downstairs313jpg { get; set; }
        public Downstairs314Jpg downstairs314jpg { get; set; }
        public Downstairs315Jpg downstairs315jpg { get; set; }
        public Downstairs32Jpg downstairs32jpg { get; set; }
        public Downstairs33Jpg downstairs33jpg { get; set; }
        public Downstairs34Jpg downstairs34jpg { get; set; }
        public Downstairs35Jpg downstairs35jpg { get; set; }
        public Downstairs36Jpg downstairs36jpg { get; set; }
        public Downstairs37Jpg downstairs37jpg { get; set; }
        public Downstairs38Jpg downstairs38jpg { get; set; }
        public Downstairs39Jpg downstairs39jpg { get; set; }
        public Draw1Jpg draw1jpg { get; set; }
        public Draw10Jpg draw10jpg { get; set; }
        public Draw11Jpg draw11jpg { get; set; }
        public Draw12Jpg draw12jpg { get; set; }
        public Draw13Jpg draw13jpg { get; set; }
        public Draw15Jpg draw15jpg { get; set; }
        public Draw2Jpg draw2jpg { get; set; }
        public Draw3Jpg draw3jpg { get; set; }
        public Draw4Jpg draw4jpg { get; set; }
        public Draw5Jpg draw5jpg { get; set; }
        public Draw6Jpg draw6jpg { get; set; }
        public Draw7Jpg draw7jpg { get; set; }
        public Draw8Jpg draw8jpg { get; set; }
        public Draw9Jpg draw9jpg { get; set; }
        public FlatJpg flatjpg { get; set; }
        public FrontdoorJpg frontdoorjpg { get; set; }
        public GoldJpg goldjpg { get; set; }
        public KarenDreamsMasturbates10Jpg karendreamsmasturbates10jpg { get; set; }
        public Karenday4bJpg karenday4bjpg { get; set; }
        public Karenday4dJpg karenday4djpg { get; set; }
        public Karenday4eJpg karenday4ejpg { get; set; }
        public Karenday4fJpg karenday4fjpg { get; set; }
        public Karenday4gJpg karenday4gjpg { get; set; }
        public Karenday4hJpg karenday4hjpg { get; set; }
        public Karenmast1Jpg karenmast1jpg { get; set; }
        public Karenmast2Jpg karenmast2jpg { get; set; }
        public Karenmast3Jpg karenmast3jpg { get; set; }
        public Karenmast5Jpg karenmast5jpg { get; set; }
        public Karennude101Jpg karennude101jpg { get; set; }
        public Karennude10Jpg karennude10jpg { get; set; }
        public Karennude111Jpg karennude111jpg { get; set; }
        public Karennude11Jpg karennude11jpg { get; set; }
        public Karennude121Jpg karennude121jpg { get; set; }
        public Karennude12Jpg karennude12jpg { get; set; }
        public Karennude151Jpg karennude151jpg { get; set; }
        public Karennude15Jpg karennude15jpg { get; set; }
        public Karennude211Jpg karennude211jpg { get; set; }
        public Karennude21Jpg karennude21jpg { get; set; }
        public Karennude221Jpg karennude221jpg { get; set; }
        public Karennude22Jpg karennude22jpg { get; set; }
        public Karennude31Jpg karennude31jpg { get; set; }
        public Karennude3Jpg karennude3jpg { get; set; }
        public Karennude4Jpg karennude4jpg { get; set; }
        public Lawyer1Jpg lawyer1jpg { get; set; }
        public Lawyer2Jpg lawyer2jpg { get; set; }
        public Lawyer3Jpg lawyer3jpg { get; set; }
        public Lawyer4Jpg lawyer4jpg { get; set; }
        public Lawyer5Jpg lawyer5jpg { get; set; }
        public Lawyer6Jpg lawyer6jpg { get; set; }
        public Lawyer7Jpg lawyer7jpg { get; set; }
        public Lawyer8Jpg lawyer8jpg { get; set; }
        public Lawyer9Jpg lawyer9jpg { get; set; }
        public Lawyeroffice1Jpg lawyeroffice1jpg { get; set; }
        public Lawyeroffice2Jpg lawyeroffice2jpg { get; set; }
        public Lawyeroffice3Jpg lawyeroffice3jpg { get; set; }
        public Lawyeroffice4Jpg lawyeroffice4jpg { get; set; }
        public Lawyeroffice7Jpg lawyeroffice7jpg { get; set; }
        public MansionJpg mansionjpg { get; set; }
        public PetiteCollegeStudentKarenDreams010Jpg petitecollegestudentkarendreams010jpg { get; set; }
        public PinkporscheJpg pinkporschejpg { get; set; }
        public RingtoneMp3 ringtonemp3 { get; set; }
        public Shower1Jpg shower1jpg { get; set; }
        public Shower11Jpg shower11jpg { get; set; }
        public Shower15Jpg shower15jpg { get; set; }
        public Shower2Jpg shower2jpg { get; set; }
        public Shower3Jpg shower3jpg { get; set; }
        public Shower4Jpg shower4jpg { get; set; }
        public Shower6Jpg shower6jpg { get; set; }
        public Slideshowa1Jpg slideshowa1jpg { get; set; }
        public Slideshowa10Jpg slideshowa10jpg { get; set; }
        public Slideshowa11Jpg slideshowa11jpg { get; set; }
        public Slideshowa12Jpg slideshowa12jpg { get; set; }
        public Slideshowa13Jpg slideshowa13jpg { get; set; }
        public Slideshowa14Jpg slideshowa14jpg { get; set; }
        public Slideshowa15Jpg slideshowa15jpg { get; set; }
        public Slideshowa16Jpg slideshowa16jpg { get; set; }
        public Slideshowa17Jpg slideshowa17jpg { get; set; }
        public Slideshowa18Jpg slideshowa18jpg { get; set; }
        public Slideshowa19Jpg slideshowa19jpg { get; set; }
        public Slideshowa2Jpg slideshowa2jpg { get; set; }
        public Slideshowa20Jpg slideshowa20jpg { get; set; }
        public Slideshowa21Jpg slideshowa21jpg { get; set; }
        public Slideshowa22Jpg slideshowa22jpg { get; set; }
        public Slideshowa23Jpg slideshowa23jpg { get; set; }
        public Slideshowa24Jpg slideshowa24jpg { get; set; }
        public Slideshowa25Jpg slideshowa25jpg { get; set; }
        public Slideshowa26Jpg slideshowa26jpg { get; set; }
        public Slideshowa27Jpg slideshowa27jpg { get; set; }
        public Slideshowa28Jpg slideshowa28jpg { get; set; }
        public Slideshowa29Jpg slideshowa29jpg { get; set; }
        public Slideshowa3Jpg slideshowa3jpg { get; set; }
        public Slideshowa30Jpg slideshowa30jpg { get; set; }
        public Slideshowa31Jpg slideshowa31jpg { get; set; }
        public Slideshowa32Jpg slideshowa32jpg { get; set; }
        public Slideshowa33Jpg slideshowa33jpg { get; set; }
        public Slideshowa34Jpg slideshowa34jpg { get; set; }
        public Slideshowa35Jpg slideshowa35jpg { get; set; }
        public Slideshowa36Jpg slideshowa36jpg { get; set; }
        public Slideshowa37Jpg slideshowa37jpg { get; set; }
        public Slideshowa38Jpg slideshowa38jpg { get; set; }
        public Slideshowa39Jpg slideshowa39jpg { get; set; }
        public Slideshowa4Jpg slideshowa4jpg { get; set; }
        public Slideshowa40Jpg slideshowa40jpg { get; set; }
        public Slideshowa41Jpg slideshowa41jpg { get; set; }
        public Slideshowa42Jpg slideshowa42jpg { get; set; }
        public Slideshowa43Jpg slideshowa43jpg { get; set; }
        public Slideshowa44Jpg slideshowa44jpg { get; set; }
        public Slideshowa45Jpg slideshowa45jpg { get; set; }
        public Slideshowa46Jpg slideshowa46jpg { get; set; }
        public Slideshowa5Jpg slideshowa5jpg { get; set; }
        public Slideshowa6Jpg slideshowa6jpg { get; set; }
        public Slideshowa7Jpg slideshowa7jpg { get; set; }
        public Slideshowa8Jpg slideshowa8jpg { get; set; }
        public Slideshowa9Jpg slideshowa9jpg { get; set; }
        public Sofa1Jpg sofa1jpg { get; set; }
        public Sofa10Jpg sofa10jpg { get; set; }
        public Sofa11Jpg sofa11jpg { get; set; }
        public Sofa12Jpg sofa12jpg { get; set; }
        public Sofa13Jpg sofa13jpg { get; set; }
        public Sofa2Jpg sofa2jpg { get; set; }
        public Sofa3Jpg sofa3jpg { get; set; }
        public Sofa4Jpg sofa4jpg { get; set; }
        public Sofa5Jpg sofa5jpg { get; set; }
        public Sofa6Jpg sofa6jpg { get; set; }
        public Sofa7Jpg sofa7jpg { get; set; }
        public Sofa8Jpg sofa8jpg { get; set; }
        public Sofa9Jpg sofa9jpg { get; set; }
        public T6aJpg t6ajpg { get; set; }
        public T6bJpg t6bjpg { get; set; }
        public T6cJpg t6cjpg { get; set; }
        public T6dJpg t6djpg { get; set; }
        public T6eJpg t6ejpg { get; set; }
        public T6fJpg t6fjpg { get; set; }
        public T6gJpg t6gjpg { get; set; }
        public T6hJpg t6hjpg { get; set; }
        public T6jJpg t6jjpg { get; set; }
        public T6kJpg t6kjpg { get; set; }
        public T6lJpg t6ljpg { get; set; }
        public T6mJpg t6mjpg { get; set; }
        public T6nJpg t6njpg { get; set; }
        public T6oJpg t6ojpg { get; set; }
        public T6pJpg t6pjpg { get; set; }
        public T6rJpg t6rjpg { get; set; }
        public ToiletbowlJpg toiletbowljpg { get; set; }
    }

    public class BlankJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Coldshower1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Coldshower2Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Coldshower3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Doggy1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Doggy3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs31Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs310Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs311Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs312Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs313Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs314Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs315Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs32Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs33Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs34Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs35Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs36Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs37Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs38Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Downstairs39Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw10Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw11Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw12Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw13Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw15Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw2Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw4Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw5Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw6Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw7Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw8Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Draw9Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class FlatJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class FrontdoorJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class GoldJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class KarenDreamsMasturbates10Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenday4bJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenday4dJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenday4eJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenday4fJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenday4gJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenday4hJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenmast1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenmast2Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenmast3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karenmast5Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude101Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude10Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude111Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude11Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude121Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude12Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude151Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude15Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude211Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude21Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude221Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude22Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude31Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Karennude4Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer2Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer4Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer5Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer6Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer7Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer8Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyer9Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyeroffice1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyeroffice2Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyeroffice3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyeroffice4Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Lawyeroffice7Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class MansionJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class PetiteCollegeStudentKarenDreams010Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class PinkporscheJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class RingtoneMp3
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
    }

    public class Shower1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Shower11Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Shower15Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Shower2Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Shower3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Shower4Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Shower6Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa10Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa11Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa12Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa13Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa14Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa15Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa16Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa17Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa18Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa19Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa2Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa20Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa21Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa22Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa23Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa24Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa25Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa26Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa27Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa28Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa29Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa30Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa31Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa32Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa33Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa34Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa35Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa36Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa37Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa38Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa39Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa4Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa40Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa41Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa42Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa43Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa44Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa45Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa46Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa5Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa6Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa7Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa8Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Slideshowa9Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa1Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa10Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa11Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa12Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa13Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa2Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa3Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa4Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa5Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa6Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa7Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa8Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Sofa9Jpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6aJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6bJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6cJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6dJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6eJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6fJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6gJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6hJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6jJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6kJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6lJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6mJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6nJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6oJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6pJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class T6rJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class ToiletbowlJpg
    {
        public int id { get; set; }
        public string hash { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Pages
    {
        public Start[] start { get; set; }
        public _1[] _1 { get; set; }
        public _10[] _10 { get; set; }
        public _11[] _11 { get; set; }
        public _12[] _12 { get; set; }
        public _13[] _13 { get; set; }
        public _14[] _14 { get; set; }
        public _15[] _15 { get; set; }
        public _16[] _16 { get; set; }
        public _17[] _17 { get; set; }
        public _18[] _18 { get; set; }
        public _19[] _19 { get; set; }
        public _2[] _2 { get; set; }
        public _20[] _20 { get; set; }
        public _21[] _21 { get; set; }
        public _3[] _3 { get; set; }
        public _4[] _4 { get; set; }
        public _5[] _5 { get; set; }
        public _6[] _6 { get; set; }
        public _7[] _7 { get; set; }
        public _8[] _8 { get; set; }
        public _9[] _9 { get; set; }
        public D4A1[] D4A1 { get; set; }
        public D4A10[] D4A10 { get; set; }
        public D4A11[] D4A11 { get; set; }
        public D4A12[] D4A12 { get; set; }
        public D4A13[] D4A13 { get; set; }
        public D4A14[] D4A14 { get; set; }
        public D4A15[] D4A15 { get; set; }
        public D4A16[] D4A16 { get; set; }
        public D4A17[] D4A17 { get; set; }
        public D4A18[] D4A18 { get; set; }
        public D4A19[] D4A19 { get; set; }
        public D4A2[] D4A2 { get; set; }
        public D4A20[] D4A20 { get; set; }
        public D4A21[] D4A21 { get; set; }
        public D4A22[] D4A22 { get; set; }
        public D4A3[] D4A3 { get; set; }
        public D4A4[] D4A4 { get; set; }
        public D4A5[] D4A5 { get; set; }
        public D4A6[] D4A6 { get; set; }
        public D4A7[] D4A7 { get; set; }
        public D4A8[] D4A8 { get; set; }
        public D4A9[] D4A9 { get; set; }
        public D4end[] D4End { get; set; }
        public D4enda[] D4EndA { get; set; }
        public D4endb[] D4EndB { get; set; }
        public D4endc[] D4EndC { get; set; }
        public D4endd[] D4EndD { get; set; }
        public D4ende[] D4EndE { get; set; }
        public D4fail[] D4Fail { get; set; }
        public D4fail2[] D4Fail2 { get; set; }
        public D4fail3[] D4Fail3 { get; set; }
        public D4fail4[] D4Fail4 { get; set; }
        public D4failt1[] D4FailT1 { get; set; }
        public D4failt10[] D4FailT10 { get; set; }
        public D4failt2[] D4FailT2 { get; set; }
        public D4failt3[] D4FailT3 { get; set; }
        public D4failt4[] D4FailT4 { get; set; }
        public D4failt5[] D4FailT5 { get; set; }
        public D4failt6[] D4FailT6 { get; set; }
        public D4failt7[] D4FailT7 { get; set; }
        public D4failt8[] D4FailT8 { get; set; }
        public D4failt9[] D4FailT9 { get; set; }
        public D4T10B1[] D4T10B1 { get; set; }
        public D4T10B2[] D4T10B2 { get; set; }
        public D4T10B3[] D4T10B3 { get; set; }
        public D4T10B4[] D4T10B4 { get; set; }
        public D4t10bearly[] D4T10BEarly { get; set; }
        public D4t10early[] D4T10Early { get; set; }
        public D4T10P1[] D4T10P1 { get; set; }
        public D4T10P2[] D4T10P2 { get; set; }
        public D4T10P3[] D4T10P3 { get; set; }
        public D4T10P4[] D4T10P4 { get; set; }
        public D4T10SA[] D4T10SA { get; set; }
        public D4t10together[] D4T10Together { get; set; }
        public D4t5came[] D4T5Came { get; set; }
        public D4T9A[] D4T9A { get; set; }
        public D4T9A2[] D4T9A2 { get; set; }
        public D4T9B2[] D4T9B2 { get; set; }
        public D4T9C[] D4T9C { get; set; }
        public D4T9C2[] D4T9C2 { get; set; }
        public D4T9C3[] D4T9C3 { get; set; }
        public D4T9D[] D4T9D { get; set; }
        public D4T9E[] D4T9E { get; set; }
        public D4T9E2[] D4T9E2 { get; set; }
        public Day4[] Day4 { get; set; }
        public Day4sp[] Day4SP { get; set; }
        public Day5[] Day5 { get; set; }
        public Day5fail[] Day5Fail { get; set; }
        public Day5fail2[] Day5Fail2 { get; set; }
        public Day5l1[] Day5L1 { get; set; }
        public Day5l10[] Day5L10 { get; set; }
        public Day5l11[] Day5L11 { get; set; }
        public Day5l12[] Day5L12 { get; set; }
        public Day5l13n[] Day5L13N { get; set; }
        public Day5l13y[] Day5L13Y { get; set; }
        public Day5l14n[] Day5L14N { get; set; }
        public Day5l14y[] Day5L14Y { get; set; }
        public Day5l15[] Day5L15 { get; set; }
        public Day5l16[] Day5L16 { get; set; }
        public Day5l17[] Day5L17 { get; set; }
        public Day5l18[] Day5L18 { get; set; }
        public Day5l19[] Day5L19 { get; set; }
        public Day5l2[] Day5L2 { get; set; }
        public Day5l21[] Day5L21 { get; set; }
        public Day5l22[] Day5L22 { get; set; }
        public Day5l23[] Day5L23 { get; set; }
        public Day5l24[] Day5L24 { get; set; }
        public Day5l25[] Day5L25 { get; set; }
        public Day5l26[] Day5L26 { get; set; }
        public Day5l27[] Day5L27 { get; set; }
        public Day5l28[] Day5L28 { get; set; }
        public Day5l29[] Day5L29 { get; set; }
        public Day5l3[] Day5L3 { get; set; }
        public Day5l30[] Day5L30 { get; set; }
        public Day5l31[] Day5L31 { get; set; }
        public Day5l32[] Day5L32 { get; set; }
        public Day5l4[] Day5L4 { get; set; }
        public Day5l5[] Day5L5 { get; set; }
        public Day5l6[] Day5L6 { get; set; }
        public Day5l7n[] Day5L7N { get; set; }
        public Day5l7y[] Day5L7Y { get; set; }
        public Day5l8[] Day5L8 { get; set; }
        public Day5l9[] Day5L9 { get; set; }
        public T10slut[] T10Slut { get; set; }
        public T10toy[] T10Toy { get; set; }
        public T1S01[] T1S01 { get; set; }
        public T1S01D[] T1S01D { get; set; }
        public T1S02[] T1S02 { get; set; }
        public T1S02D[] T1S02D { get; set; }
        public T1S03[] T1S03 { get; set; }
        public T1S03D[] T1S03D { get; set; }
        public T1S04[] T1S04 { get; set; }
        public T1S04D[] T1S04D { get; set; }
        public T1S05[] T1S05 { get; set; }
        public T1S10[] T1S10 { get; set; }
        public T1S10D[] T1S10D { get; set; }
        public T1S11[] T1S11 { get; set; }
        public T1S11D[] T1S11D { get; set; }
        public T1S12[] T1S12 { get; set; }
        public T1S12D[] T1S12D { get; set; }
        public T1S13[] T1S13 { get; set; }
        public T1S13D[] T1S13D { get; set; }
        public T1S14[] T1S14 { get; set; }
        public T1S14D[] T1S14D { get; set; }
        public T1S15[] T1S15 { get; set; }
        public T1S20[] T1S20 { get; set; }
        public T1S20D[] T1S20D { get; set; }
        public T1S21[] T1S21 { get; set; }
        public T1S21D[] T1S21D { get; set; }
        public T1S22[] T1S22 { get; set; }
        public T1S22D[] T1S22D { get; set; }
        public T1S23[] T1S23 { get; set; }
        public T1S23D[] T1S23D { get; set; }
        public T1S24[] T1S24 { get; set; }
        public T1S24D[] T1S24D { get; set; }
        public T1S25[] T1S25 { get; set; }
        public T1S30[] T1S30 { get; set; }
        public T1S30D[] T1S30D { get; set; }
        public T1S31[] T1S31 { get; set; }
        public T1S31D[] T1S31D { get; set; }
        public T1S32[] T1S32 { get; set; }
        public T1S32D[] T1S32D { get; set; }
        public T1S33[] T1S33 { get; set; }
        public T1S33D[] T1S33D { get; set; }
        public T1S34[] T1S34 { get; set; }
        public T1S34D[] T1S34D { get; set; }
        public T1S35[] T1S35 { get; set; }
        public T1S40[] T1S40 { get; set; }
        public T1S40D[] T1S40D { get; set; }
        public T1S41[] T1S41 { get; set; }
        public T1S41D[] T1S41D { get; set; }
        public T1S42[] T1S42 { get; set; }
        public T1S42D[] T1S42D { get; set; }
        public T1S43[] T1S43 { get; set; }
        public T1S43D[] T1S43D { get; set; }
        public T1S44[] T1S44 { get; set; }
        public T1S44D[] T1S44D { get; set; }
        public T1S45[] T1S45 { get; set; }
        public T1S50[] T1S50 { get; set; }
        public T1S51[] T1S51 { get; set; }
        public T1S52[] T1S52 { get; set; }
        public T1S53[] T1S53 { get; set; }
        public T1S54[] T1S54 { get; set; }
        public T2slut[] T2slut { get; set; }
        public T2SS[] T2SS { get; set; }
        public T2SS1[] T2SS1 { get; set; }
        public T2SS2[] T2SS2 { get; set; }
        public T2SS3[] T2SS3 { get; set; }
        public T2SS4[] T2SS4 { get; set; }
        public T2SS5[] T2SS5 { get; set; }
        public T2ssfail[] T2SSFail { get; set; }
        public T2sswin[] T2SSWin { get; set; }
        public T2toy[] T2toy { get; set; }
        public T2WR[] T2WR { get; set; }
        public T2WR1[] T2WR1 { get; set; }
        public T2WR2[] T2WR2 { get; set; }
        public T2WR3[] T2WR3 { get; set; }
        public T2WR4[] T2WR4 { get; set; }
        public T2WR5[] T2WR5 { get; set; }
        public T2wrfail[] T2WRFail { get; set; }
        public T2wrwin[] T2WRWin { get; set; }
        public T3ES[] T3ES { get; set; }
        public T3ES1[] T3ES1 { get; set; }
        public T3ES2[] T3ES2 { get; set; }
        public T3ES3[] T3ES3 { get; set; }
        public T3esfail[] T3ESFail { get; set; }
        public T3eswin[] T3ESWin { get; set; }
        public T3SD[] T3SD { get; set; }
        public T3SD1[] T3SD1 { get; set; }
        public T3SD2[] T3SD2 { get; set; }
        public T3SD3[] T3SD3 { get; set; }
        public T3SD4[] T3SD4 { get; set; }
        public T3SD5[] T3SD5 { get; set; }
        public T3SD6[] T3SD6 { get; set; }
        public T3sdfail[] T3SDFail { get; set; }
        public T3sdwin[] T3SDWin { get; set; }
        public T3slut[] T3slut { get; set; }
        public T3toy[] T3toy { get; set; }
        public T4E1[] T4E1 { get; set; }
        public T4E2[] T4E2 { get; set; }
        public T4E3[] T4E3 { get; set; }
        public T4E4[] T4E4 { get; set; }
        public T4E5[] T4E5 { get; set; }
        public T4lose[] T4Lose { get; set; }
        public T4slut[] T4slut { get; set; }
        public T4start[] T4start { get; set; }
        public T4toy[] T4toy { get; set; }
        public T4win[] T4Win { get; set; }
        public T5E1[] T5E1 { get; set; }
        public T5E10[] T5E10 { get; set; }
        public T5E2[] T5E2 { get; set; }
        public T5E3[] T5E3 { get; set; }
        public T5E4[] T5E4 { get; set; }
        public T5E5[] T5E5 { get; set; }
        public T5E6[] T5E6 { get; set; }
        public T5E7[] T5E7 { get; set; }
        public T5E8[] T5E8 { get; set; }
        public T5E9[] T5E9 { get; set; }
        public T5lose[] T5Lose { get; set; }
        public T5slut[] T5slut { get; set; }
        public T5toy[] T5toy { get; set; }
        public T5win[] T5Win { get; set; }
        public T6N1[] T6N1 { get; set; }
        public T6N10[] T6N10 { get; set; }
        public T6N11[] T6N11 { get; set; }
        public T6N12[] T6N12 { get; set; }
        public T6N13[] T6N13 { get; set; }
        public T6N14[] T6N14 { get; set; }
        public T6N15[] T6N15 { get; set; }
        public T6N16[] T6N16 { get; set; }
        public T6N17[] T6N17 { get; set; }
        public T6N18[] T6N18 { get; set; }
        public T6N19[] T6N19 { get; set; }
        public T6N2[] T6N2 { get; set; }
        public T6N3[] T6N3 { get; set; }
        public T6N4[] T6N4 { get; set; }
        public T6N5[] T6N5 { get; set; }
        public T6N6[] T6N6 { get; set; }
        public T6N7[] T6N7 { get; set; }
        public T6N8[] T6N8 { get; set; }
        public T6N9[] T6N9 { get; set; }
        public T6slut[] T6slut { get; set; }
        public T6toy[] T6toy { get; set; }
        public T7A1[] T7A1 { get; set; }
        public T7A2[] T7A2 { get; set; }
        public T7A3[] T7A3 { get; set; }
        public T7A4[] T7A4 { get; set; }
        public T7B1[] T7B1 { get; set; }
        public T7B2[] T7B2 { get; set; }
        public T7B3[] T7B3 { get; set; }
        public T7B4[] T7B4 { get; set; }
        public T7C1[] T7C1 { get; set; }
        public T7C2[] T7C2 { get; set; }
        public T7C3[] T7C3 { get; set; }
        public T7C4[] T7C4 { get; set; }
        public T7D1[] T7D1 { get; set; }
        public T7D2[] T7D2 { get; set; }
        public T7D3[] T7D3 { get; set; }
        public T7D4[] T7D4 { get; set; }
        public T7E1[] T7E1 { get; set; }
        public T7E2[] T7E2 { get; set; }
        public T7finish[] T7Finish { get; set; }
        public T8C1[] T8C1 { get; set; }
        public T8C2[] T8C2 { get; set; }
        public T8C3[] T8C3 { get; set; }
        public T8C4[] T8C4 { get; set; }
        public T8C5[] T8C5 { get; set; }
        public T8C6[] T8C6 { get; set; }
        public T8C7[] T8C7 { get; set; }
        public T8P2[] T8P2 { get; set; }
        public T8P3[] T8P3 { get; set; }
        public T8P4[] T8P4 { get; set; }
        public T8P4B[] T8P4B { get; set; }
        public T8P4C[] T8P4C { get; set; }
        public T8P4D[] T8P4D { get; set; }
        public T8P4E[] T8P4E { get; set; }
        public T8P4F[] T8P4F { get; set; }
        public T8P4G[] T8P4G { get; set; }
        public T8P5[] T8P5 { get; set; }
        public T8p5a[] T8P5a { get; set; }
        public T8p5b[] T8P5b { get; set; }
        public T9slut[] T9slut { get; set; }
        public _22[] _22 { get; set; }
        public _23[] _23 { get; set; }
        public _24[] _24 { get; set; }
        public _25[] _25 { get; set; }
        public _26[] _26 { get; set; }
        public _27[] _27 { get; set; }
        public _28[] _28 { get; set; }
        public _29[] _29 { get; set; }
        public _30[] _30 { get; set; }
        public _31[] _31 { get; set; }
        public _32[] _32 { get; set; }
        public _33[] _33 { get; set; }
        public _34[] _34 { get; set; }
        public _35[] _35 { get; set; }
        public _36[] _36 { get; set; }
        public _37[] _37 { get; set; }
        public _38[] _38 { get; set; }
        public Child[] _ { get; set; }
    }

    public class Start
    {
        public NyxPage nyxpage { get; set; }
    }

    public class NyxPage
    {
        public Media media { get; set; }
        public string text { get; set; }
        public Action action { get; set; }
    }

    public class Media
    {
        public string nyximage { get; set; }
    }

    public class Action
    {
        public NyxVert nyxvert { get; set; }
    }

    public class NyxVert
    {
        public Element[] elements { get; set; }
    }

    public class Element
    {
        public NyxButtons[] nyxbuttons { get; set; }
    }

    public class NyxButtons
    {
        public string label { get; set; }
        public Command[] commands { get; set; }
    }

    public class Command
    {
        public Goto _goto { get; set; }
    }

    public class Goto
    {
        public string target { get; set; }
    }

    public class _1
    {
        public NyxPage1 nyxpage { get; set; }
    }

    public class NyxPage1
    {
        public Media1 media { get; set; }
        public string text { get; set; }
        public Action1 action { get; set; }
    }

    public class Media1
    {
        public string nyximage { get; set; }
    }

    public class Action1
    {
        public NyxTimer nyxtimer { get; set; }
    }

    public class NyxTimer
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command1[] commands { get; set; }
    }

    public class Command1
    {
        public Goto1 _goto { get; set; }
    }

    public class Goto1
    {
        public string target { get; set; }
    }

    public class _10
    {
        public NyxPage2 nyxpage { get; set; }
    }

    public class NyxPage2
    {
        public Media2 media { get; set; }
        public string text { get; set; }
        public Action2 action { get; set; }
    }

    public class Media2
    {
        public string nyximage { get; set; }
    }

    public class Action2
    {
        public NyxVert1 nyxvert { get; set; }
    }

    public class NyxVert1
    {
        public Element1[] elements { get; set; }
    }

    public class Element1
    {
        public NyxButtons1[] nyxbuttons { get; set; }
    }

    public class NyxButtons1
    {
        public string label { get; set; }
        public Command2[] commands { get; set; }
    }

    public class Command2
    {
        public Goto2 _goto { get; set; }
    }

    public class Goto2
    {
        public string target { get; set; }
    }

    public class _11
    {
        public NyxPage3 nyxpage { get; set; }
    }

    public class NyxPage3
    {
        public Media3 media { get; set; }
        public string text { get; set; }
        public Action3 action { get; set; }
    }

    public class Media3
    {
        public string nyximage { get; set; }
    }

    public class Action3
    {
        public NyxVert2 nyxvert { get; set; }
    }

    public class NyxVert2
    {
        public Element2[] elements { get; set; }
    }

    public class Element2
    {
        public NyxButtons2[] nyxbuttons { get; set; }
    }

    public class NyxButtons2
    {
        public string label { get; set; }
        public Command3[] commands { get; set; }
    }

    public class Command3
    {
        public Goto3 _goto { get; set; }
    }

    public class Goto3
    {
        public string target { get; set; }
    }

    public class _12
    {
        public NyxPage4 nyxpage { get; set; }
    }

    public class NyxPage4
    {
        public Media4 media { get; set; }
        public string text { get; set; }
        public Action4 action { get; set; }
        public Hidden hidden { get; set; }
        public Instruc instruc { get; set; }
    }

    public class Media4
    {
        public string nyximage { get; set; }
    }

    public class Action4
    {
        public NyxVert3 nyxvert { get; set; }
    }

    public class NyxVert3
    {
        public Element3[] elements { get; set; }
    }

    public class Element3
    {
        public NyxButtons3[] nyxbuttons { get; set; }
    }

    public class NyxButtons3
    {
        public string label { get; set; }
        public Command4[] commands { get; set; }
    }

    public class Command4
    {
        public Goto4 _goto { get; set; }
    }

    public class Goto4
    {
        public string target { get; set; }
    }

    public class Hidden
    {
        public Pcm2Unset pcm2unset { get; set; }
    }

    public class Pcm2Unset
    {
        public string[] pages { get; set; }
    }

    public class Instruc
    {
        public Pcm2Set pcm2set { get; set; }
    }

    public class Pcm2Set
    {
        public string[] pages { get; set; }
    }

    public class _13
    {
        public NyxPage5 nyxpage { get; set; }
    }

    public class NyxPage5
    {
        public Media5 media { get; set; }
        public string text { get; set; }
        public Action5 action { get; set; }
        public Hidden1 hidden { get; set; }
        public Instruc1 instruc { get; set; }
    }

    public class Media5
    {
        public string nyximage { get; set; }
    }

    public class Action5
    {
        public NyxVert4 nyxvert { get; set; }
    }

    public class NyxVert4
    {
        public Element4[] elements { get; set; }
    }

    public class Element4
    {
        public NyxButtons4[] nyxbuttons { get; set; }
    }

    public class NyxButtons4
    {
        public string label { get; set; }
        public Command5[] commands { get; set; }
    }

    public class Command5
    {
        public Goto5 _goto { get; set; }
    }

    public class Goto5
    {
        public string target { get; set; }
    }

    public class Hidden1
    {
        public Pcm2Unset1 pcm2unset { get; set; }
    }

    public class Pcm2Unset1
    {
        public string[] pages { get; set; }
    }

    public class Instruc1
    {
        public Pcm2Set1 pcm2set { get; set; }
    }

    public class Pcm2Set1
    {
        public string[] pages { get; set; }
    }

    public class _14
    {
        public NyxPage6 nyxpage { get; set; }
    }

    public class NyxPage6
    {
        public Media6 media { get; set; }
        public string text { get; set; }
        public Action6 action { get; set; }
        public Hidden2 hidden { get; set; }
        public Instruc2 instruc { get; set; }
    }

    public class Media6
    {
        public string nyximage { get; set; }
    }

    public class Action6
    {
        public NyxVert5 nyxvert { get; set; }
    }

    public class NyxVert5
    {
        public Element5[] elements { get; set; }
    }

    public class Element5
    {
        public NyxButtons5[] nyxbuttons { get; set; }
    }

    public class NyxButtons5
    {
        public string label { get; set; }
        public Command6[] commands { get; set; }
    }

    public class Command6
    {
        public Goto6 _goto { get; set; }
    }

    public class Goto6
    {
        public string target { get; set; }
    }

    public class Hidden2
    {
        public Pcm2Unset2 pcm2unset { get; set; }
    }

    public class Pcm2Unset2
    {
        public string[] pages { get; set; }
    }

    public class Instruc2
    {
        public Pcm2Set2 pcm2set { get; set; }
    }

    public class Pcm2Set2
    {
        public string[] pages { get; set; }
    }

    public class _15
    {
        public NyxPage7 nyxpage { get; set; }
    }

    public class NyxPage7
    {
        public Media7 media { get; set; }
        public string text { get; set; }
        public Action7 action { get; set; }
        public Hidden3 hidden { get; set; }
        public Instruc3 instruc { get; set; }
    }

    public class Media7
    {
        public string nyximage { get; set; }
    }

    public class Action7
    {
        public NyxVert6 nyxvert { get; set; }
    }

    public class NyxVert6
    {
        public Element6[] elements { get; set; }
    }

    public class Element6
    {
        public NyxButtons6[] nyxbuttons { get; set; }
    }

    public class NyxButtons6
    {
        public string label { get; set; }
        public Command7[] commands { get; set; }
    }

    public class Command7
    {
        public Goto7 _goto { get; set; }
    }

    public class Goto7
    {
        public string target { get; set; }
    }

    public class Hidden3
    {
        public Pcm2Unset3 pcm2unset { get; set; }
    }

    public class Pcm2Unset3
    {
        public string[] pages { get; set; }
    }

    public class Instruc3
    {
        public Pcm2Set3 pcm2set { get; set; }
    }

    public class Pcm2Set3
    {
        public string[] pages { get; set; }
    }

    public class _16
    {
        public NyxPage8 nyxpage { get; set; }
    }

    public class NyxPage8
    {
        public Media8 media { get; set; }
        public string text { get; set; }
        public Action8 action { get; set; }
        public Hidden4 hidden { get; set; }
        public Instruc4 instruc { get; set; }
    }

    public class Media8
    {
        public string nyximage { get; set; }
    }

    public class Action8
    {
        public NyxVert7 nyxvert { get; set; }
    }

    public class NyxVert7
    {
        public Element7[] elements { get; set; }
    }

    public class Element7
    {
        public NyxButtons7[] nyxbuttons { get; set; }
    }

    public class NyxButtons7
    {
        public string label { get; set; }
        public Command8[] commands { get; set; }
    }

    public class Command8
    {
        public Goto8 _goto { get; set; }
    }

    public class Goto8
    {
        public string target { get; set; }
    }

    public class Hidden4
    {
        public Pcm2Unset4 pcm2unset { get; set; }
    }

    public class Pcm2Unset4
    {
        public string[] pages { get; set; }
    }

    public class Instruc4
    {
        public Pcm2Set4 pcm2set { get; set; }
    }

    public class Pcm2Set4
    {
        public string[] pages { get; set; }
    }

    public class _17
    {
        public NyxPage9 nyxpage { get; set; }
    }

    public class NyxPage9
    {
        public Media9 media { get; set; }
        public string text { get; set; }
        public Action9 action { get; set; }
        public Hidden5 hidden { get; set; }
        public Instruc5 instruc { get; set; }
    }

    public class Media9
    {
        public string nyximage { get; set; }
    }

    public class Action9
    {
        public NyxVert8 nyxvert { get; set; }
    }

    public class NyxVert8
    {
        public Element8[] elements { get; set; }
    }

    public class Element8
    {
        public NyxButtons8[] nyxbuttons { get; set; }
    }

    public class NyxButtons8
    {
        public string label { get; set; }
        public Command9[] commands { get; set; }
    }

    public class Command9
    {
        public Goto9 _goto { get; set; }
    }

    public class Goto9
    {
        public string target { get; set; }
    }

    public class Hidden5
    {
        public Pcm2Unset5 pcm2unset { get; set; }
    }

    public class Pcm2Unset5
    {
        public string[] pages { get; set; }
    }

    public class Instruc5
    {
        public Pcm2Set5 pcm2set { get; set; }
    }

    public class Pcm2Set5
    {
        public string[] pages { get; set; }
    }

    public class _18
    {
        public NyxPage10 nyxpage { get; set; }
    }

    public class NyxPage10
    {
        public Media10 media { get; set; }
        public string text { get; set; }
        public Action10 action { get; set; }
        public Hidden6 hidden { get; set; }
        public Instruc6 instruc { get; set; }
    }

    public class Media10
    {
        public string nyximage { get; set; }
    }

    public class Action10
    {
        public NyxVert9 nyxvert { get; set; }
    }

    public class NyxVert9
    {
        public Element9[] elements { get; set; }
    }

    public class Element9
    {
        public NyxButtons9[] nyxbuttons { get; set; }
    }

    public class NyxButtons9
    {
        public string label { get; set; }
        public Command10[] commands { get; set; }
    }

    public class Command10
    {
        public Goto10 _goto { get; set; }
    }

    public class Goto10
    {
        public string target { get; set; }
    }

    public class Hidden6
    {
        public Pcm2Unset6 pcm2unset { get; set; }
    }

    public class Pcm2Unset6
    {
        public string[] pages { get; set; }
    }

    public class Instruc6
    {
        public Pcm2Set6 pcm2set { get; set; }
    }

    public class Pcm2Set6
    {
        public string[] pages { get; set; }
    }

    public class _19
    {
        public NyxPage11 nyxpage { get; set; }
    }

    public class NyxPage11
    {
        public Media11 media { get; set; }
        public string text { get; set; }
        public Action11 action { get; set; }
        public Hidden7 hidden { get; set; }
        public Instruc7 instruc { get; set; }
    }

    public class Media11
    {
        public string nyximage { get; set; }
    }

    public class Action11
    {
        public NyxVert10 nyxvert { get; set; }
    }

    public class NyxVert10
    {
        public Element10[] elements { get; set; }
    }

    public class Element10
    {
        public NyxButtons10[] nyxbuttons { get; set; }
    }

    public class NyxButtons10
    {
        public string label { get; set; }
        public Command11[] commands { get; set; }
    }

    public class Command11
    {
        public Goto11 _goto { get; set; }
    }

    public class Goto11
    {
        public string target { get; set; }
    }

    public class Hidden7
    {
        public Pcm2Unset7 pcm2unset { get; set; }
    }

    public class Pcm2Unset7
    {
        public string[] pages { get; set; }
    }

    public class Instruc7
    {
        public Pcm2Set7 pcm2set { get; set; }
    }

    public class Pcm2Set7
    {
        public string[] pages { get; set; }
    }

    public class _2
    {
        public NyxPage12 nyxpage { get; set; }
    }

    public class NyxPage12
    {
        public Media12 media { get; set; }
        public string text { get; set; }
        public Action12 action { get; set; }
    }

    public class Media12
    {
        public string nyximage { get; set; }
    }

    public class Action12
    {
        public NyxTimer1 nyxtimer { get; set; }
    }

    public class NyxTimer1
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command12[] commands { get; set; }
    }

    public class Command12
    {
        public Goto12 _goto { get; set; }
    }

    public class Goto12
    {
        public string target { get; set; }
    }

    public class _20
    {
        public NyxPage13 nyxpage { get; set; }
    }

    public class NyxPage13
    {
        public Media13 media { get; set; }
        public string text { get; set; }
        public Action13 action { get; set; }
    }

    public class Media13
    {
        public string nyximage { get; set; }
    }

    public class Action13
    {
        public NyxTimer2 nyxtimer { get; set; }
    }

    public class NyxTimer2
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command13[] commands { get; set; }
    }

    public class Command13
    {
        public Goto13 _goto { get; set; }
    }

    public class Goto13
    {
        public string target { get; set; }
    }

    public class _21
    {
        public NyxPage14 nyxpage { get; set; }
    }

    public class NyxPage14
    {
        public Media14 media { get; set; }
        public string text { get; set; }
        public Action14 action { get; set; }
    }

    public class Media14
    {
        public string nyximage { get; set; }
    }

    public class Action14
    {
        public NyxTimer3 nyxtimer { get; set; }
    }

    public class NyxTimer3
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command14[] commands { get; set; }
    }

    public class Command14
    {
        public Goto14 _goto { get; set; }
    }

    public class Goto14
    {
        public string target { get; set; }
    }

    public class _3
    {
        public NyxPage15 nyxpage { get; set; }
    }

    public class NyxPage15
    {
        public Media15 media { get; set; }
        public string text { get; set; }
        public Action15 action { get; set; }
    }

    public class Media15
    {
        public string nyximage { get; set; }
    }

    public class Action15
    {
        public NyxTimer4 nyxtimer { get; set; }
    }

    public class NyxTimer4
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command15[] commands { get; set; }
    }

    public class Command15
    {
        public Goto15 _goto { get; set; }
    }

    public class Goto15
    {
        public string target { get; set; }
    }

    public class _4
    {
        public NyxPage16 nyxpage { get; set; }
    }

    public class NyxPage16
    {
        public Media16 media { get; set; }
        public string text { get; set; }
        public Action16 action { get; set; }
    }

    public class Media16
    {
        public string nyximage { get; set; }
    }

    public class Action16
    {
        public NyxTimer5 nyxtimer { get; set; }
    }

    public class NyxTimer5
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command16[] commands { get; set; }
    }

    public class Command16
    {
        public Goto16 _goto { get; set; }
    }

    public class Goto16
    {
        public string target { get; set; }
    }

    public class _5
    {
        public NyxPage17 nyxpage { get; set; }
    }

    public class NyxPage17
    {
        public Media17 media { get; set; }
        public string text { get; set; }
        public Action17 action { get; set; }
    }

    public class Media17
    {
        public string nyximage { get; set; }
    }

    public class Action17
    {
        public NyxVert11 nyxvert { get; set; }
    }

    public class NyxVert11
    {
        public Element11[] elements { get; set; }
    }

    public class Element11
    {
        public NyxButtons11[] nyxbuttons { get; set; }
    }

    public class NyxButtons11
    {
        public string label { get; set; }
        public Command17[] commands { get; set; }
    }

    public class Command17
    {
        public Goto17 _goto { get; set; }
    }

    public class Goto17
    {
        public string target { get; set; }
    }

    public class _6
    {
        public NyxPage18 nyxpage { get; set; }
    }

    public class NyxPage18
    {
        public Media18 media { get; set; }
        public string text { get; set; }
        public Action18 action { get; set; }
    }

    public class Media18
    {
        public string nyximage { get; set; }
    }

    public class Action18
    {
        public NyxVert12 nyxvert { get; set; }
    }

    public class NyxVert12
    {
        public Element12[] elements { get; set; }
    }

    public class Element12
    {
        public NyxButtons12[] nyxbuttons { get; set; }
    }

    public class NyxButtons12
    {
        public string label { get; set; }
        public Command18[] commands { get; set; }
    }

    public class Command18
    {
        public Goto18 _goto { get; set; }
    }

    public class Goto18
    {
        public string target { get; set; }
    }

    public class _7
    {
        public NyxPage19 nyxpage { get; set; }
    }

    public class NyxPage19
    {
        public Media19 media { get; set; }
        public string text { get; set; }
        public Action19 action { get; set; }
    }

    public class Media19
    {
        public string nyximage { get; set; }
    }

    public class Action19
    {
        public NyxVert13 nyxvert { get; set; }
    }

    public class NyxVert13
    {
        public Element13[] elements { get; set; }
    }

    public class Element13
    {
        public NyxButtons13[] nyxbuttons { get; set; }
    }

    public class NyxButtons13
    {
        public string label { get; set; }
        public Command19[] commands { get; set; }
    }

    public class Command19
    {
        public Goto19 _goto { get; set; }
    }

    public class Goto19
    {
        public string target { get; set; }
    }

    public class _8
    {
        public NyxPage20 nyxpage { get; set; }
    }

    public class NyxPage20
    {
        public Media20 media { get; set; }
        public string text { get; set; }
        public Action20 action { get; set; }
    }

    public class Media20
    {
        public string nyximage { get; set; }
    }

    public class Action20
    {
        public NyxVert14 nyxvert { get; set; }
    }

    public class NyxVert14
    {
        public Element14[] elements { get; set; }
    }

    public class Element14
    {
        public NyxButtons14[] nyxbuttons { get; set; }
    }

    public class NyxButtons14
    {
        public string label { get; set; }
        public Command20[] commands { get; set; }
    }

    public class Command20
    {
        public Goto20 _goto { get; set; }
    }

    public class Goto20
    {
        public string target { get; set; }
    }

    public class _9
    {
        public NyxPage21 nyxpage { get; set; }
    }

    public class NyxPage21
    {
        public Media21 media { get; set; }
        public string text { get; set; }
        public Action21 action { get; set; }
    }

    public class Media21
    {
        public string nyximage { get; set; }
    }

    public class Action21
    {
        public NyxVert15 nyxvert { get; set; }
    }

    public class NyxVert15
    {
        public Element15[] elements { get; set; }
    }

    public class Element15
    {
        public NyxButtons15[] nyxbuttons { get; set; }
    }

    public class NyxButtons15
    {
        public string label { get; set; }
        public Command21[] commands { get; set; }
    }

    public class Command21
    {
        public Goto21 _goto { get; set; }
    }

    public class Goto21
    {
        public string target { get; set; }
    }

    public class D4A1
    {
        public NyxPage22 nyxpage { get; set; }
    }

    public class NyxPage22
    {
        public Media22 media { get; set; }
        public string text { get; set; }
        public Action22 action { get; set; }
    }

    public class Media22
    {
        public string nyximage { get; set; }
    }

    public class Action22
    {
        public NyxVert16 nyxvert { get; set; }
    }

    public class NyxVert16
    {
        public Element16[] elements { get; set; }
    }

    public class Element16
    {
        public NyxButtons16[] nyxbuttons { get; set; }
    }

    public class NyxButtons16
    {
        public string label { get; set; }
        public Command22[] commands { get; set; }
    }

    public class Command22
    {
        public Goto22 _goto { get; set; }
    }

    public class Goto22
    {
        public string target { get; set; }
    }

    public class D4A10
    {
        public NyxPage23 nyxpage { get; set; }
    }

    public class NyxPage23
    {
        public Media23 media { get; set; }
        public string text { get; set; }
        public Action23 action { get; set; }
    }

    public class Media23
    {
        public string nyximage { get; set; }
    }

    public class Action23
    {
        public NyxVert17 nyxvert { get; set; }
    }

    public class NyxVert17
    {
        public Element17[] elements { get; set; }
    }

    public class Element17
    {
        public NyxButtons17[] nyxbuttons { get; set; }
    }

    public class NyxButtons17
    {
        public string label { get; set; }
        public Command23[] commands { get; set; }
    }

    public class Command23
    {
        public Goto23 _goto { get; set; }
    }

    public class Goto23
    {
        public string target { get; set; }
    }

    public class D4A11
    {
        public NyxPage24 nyxpage { get; set; }
    }

    public class NyxPage24
    {
        public Media24 media { get; set; }
        public string text { get; set; }
        public Action24 action { get; set; }
    }

    public class Media24
    {
        public string nyximage { get; set; }
    }

    public class Action24
    {
        public NyxVert18 nyxvert { get; set; }
    }

    public class NyxVert18
    {
        public Element18[] elements { get; set; }
    }

    public class Element18
    {
        public NyxButtons18[] nyxbuttons { get; set; }
    }

    public class NyxButtons18
    {
        public string label { get; set; }
        public Command24[] commands { get; set; }
    }

    public class Command24
    {
        public Goto24 _goto { get; set; }
    }

    public class Goto24
    {
        public string target { get; set; }
    }

    public class D4A12
    {
        public NyxPage25 nyxpage { get; set; }
    }

    public class NyxPage25
    {
        public Media25 media { get; set; }
        public string text { get; set; }
        public Action25 action { get; set; }
    }

    public class Media25
    {
        public string nyximage { get; set; }
    }

    public class Action25
    {
        public NyxVert19 nyxvert { get; set; }
    }

    public class NyxVert19
    {
        public Element19[] elements { get; set; }
    }

    public class Element19
    {
        public NyxButtons19[] nyxbuttons { get; set; }
    }

    public class NyxButtons19
    {
        public string label { get; set; }
        public Command25[] commands { get; set; }
    }

    public class Command25
    {
        public Goto25 _goto { get; set; }
    }

    public class Goto25
    {
        public string target { get; set; }
    }

    public class D4A13
    {
        public NyxPage26 nyxpage { get; set; }
    }

    public class NyxPage26
    {
        public Media26 media { get; set; }
        public string text { get; set; }
        public Action26 action { get; set; }
    }

    public class Media26
    {
        public string nyximage { get; set; }
    }

    public class Action26
    {
        public NyxVert20 nyxvert { get; set; }
    }

    public class NyxVert20
    {
        public Element20[] elements { get; set; }
    }

    public class Element20
    {
        public NyxButtons20[] nyxbuttons { get; set; }
    }

    public class NyxButtons20
    {
        public string label { get; set; }
        public Command26[] commands { get; set; }
    }

    public class Command26
    {
        public Goto26 _goto { get; set; }
    }

    public class Goto26
    {
        public string target { get; set; }
    }

    public class D4A14
    {
        public NyxPage27 nyxpage { get; set; }
    }

    public class NyxPage27
    {
        public Media27 media { get; set; }
        public string text { get; set; }
        public Action27 action { get; set; }
    }

    public class Media27
    {
        public string nyximage { get; set; }
    }

    public class Action27
    {
        public NyxVert21 nyxvert { get; set; }
    }

    public class NyxVert21
    {
        public Element21[] elements { get; set; }
    }

    public class Element21
    {
        public NyxButtons21[] nyxbuttons { get; set; }
    }

    public class NyxButtons21
    {
        public string label { get; set; }
        public Command27[] commands { get; set; }
    }

    public class Command27
    {
        public Goto27 _goto { get; set; }
    }

    public class Goto27
    {
        public string target { get; set; }
    }

    public class D4A15
    {
        public NyxPage28 nyxpage { get; set; }
    }

    public class NyxPage28
    {
        public Media28 media { get; set; }
        public string text { get; set; }
        public Action28 action { get; set; }
    }

    public class Media28
    {
        public string nyximage { get; set; }
    }

    public class Action28
    {
        public NyxVert22 nyxvert { get; set; }
    }

    public class NyxVert22
    {
        public Element22[] elements { get; set; }
    }

    public class Element22
    {
        public NyxButtons22[] nyxbuttons { get; set; }
    }

    public class NyxButtons22
    {
        public string label { get; set; }
        public Command28[] commands { get; set; }
    }

    public class Command28
    {
        public Goto28 _goto { get; set; }
    }

    public class Goto28
    {
        public string target { get; set; }
    }

    public class D4A16
    {
        public NyxPage29 nyxpage { get; set; }
    }

    public class NyxPage29
    {
        public Media29 media { get; set; }
        public string text { get; set; }
        public Action29 action { get; set; }
    }

    public class Media29
    {
        public string nyximage { get; set; }
    }

    public class Action29
    {
        public NyxTimer6 nyxtimer { get; set; }
    }

    public class NyxTimer6
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command29[] commands { get; set; }
    }

    public class Command29
    {
        public Goto29 _goto { get; set; }
    }

    public class Goto29
    {
        public string target { get; set; }
    }

    public class D4A17
    {
        public NyxPage30 nyxpage { get; set; }
    }

    public class NyxPage30
    {
        public Media30 media { get; set; }
        public string text { get; set; }
        public Action30 action { get; set; }
    }

    public class Media30
    {
        public string nyximage { get; set; }
    }

    public class Action30
    {
        public NyxTimer7 nyxtimer { get; set; }
    }

    public class NyxTimer7
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command30[] commands { get; set; }
    }

    public class Command30
    {
        public Goto30 _goto { get; set; }
    }

    public class Goto30
    {
        public string target { get; set; }
    }

    public class D4A18
    {
        public NyxPage31 nyxpage { get; set; }
    }

    public class NyxPage31
    {
        public Media31 media { get; set; }
        public string text { get; set; }
        public Action31 action { get; set; }
    }

    public class Media31
    {
        public string nyximage { get; set; }
    }

    public class Action31
    {
        public NyxTimer8 nyxtimer { get; set; }
    }

    public class NyxTimer8
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command31[] commands { get; set; }
    }

    public class Command31
    {
        public Goto31 _goto { get; set; }
    }

    public class Goto31
    {
        public string target { get; set; }
    }

    public class D4A19
    {
        public NyxPage32 nyxpage { get; set; }
    }

    public class NyxPage32
    {
        public Media32 media { get; set; }
        public string text { get; set; }
        public Action32 action { get; set; }
    }

    public class Media32
    {
        public string nyximage { get; set; }
    }

    public class Action32
    {
        public NyxTimer9 nyxtimer { get; set; }
    }

    public class NyxTimer9
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command32[] commands { get; set; }
    }

    public class Command32
    {
        public Goto32 _goto { get; set; }
    }

    public class Goto32
    {
        public string target { get; set; }
    }

    public class D4A2
    {
        public NyxPage33 nyxpage { get; set; }
    }

    public class NyxPage33
    {
        public Media33 media { get; set; }
        public string text { get; set; }
        public Action33 action { get; set; }
    }

    public class Media33
    {
        public string nyximage { get; set; }
    }

    public class Action33
    {
        public NyxVert23 nyxvert { get; set; }
    }

    public class NyxVert23
    {
        public Element23[] elements { get; set; }
    }

    public class Element23
    {
        public NyxButtons23[] nyxbuttons { get; set; }
    }

    public class NyxButtons23
    {
        public string label { get; set; }
        public Command33[] commands { get; set; }
    }

    public class Command33
    {
        public Goto33 _goto { get; set; }
    }

    public class Goto33
    {
        public string target { get; set; }
    }

    public class D4A20
    {
        public NyxPage34 nyxpage { get; set; }
    }

    public class NyxPage34
    {
        public Media34 media { get; set; }
        public string text { get; set; }
        public Action34 action { get; set; }
    }

    public class Media34
    {
        public string nyximage { get; set; }
    }

    public class Action34
    {
        public NyxTimer10 nyxtimer { get; set; }
    }

    public class NyxTimer10
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command34[] commands { get; set; }
    }

    public class Command34
    {
        public Goto34 _goto { get; set; }
    }

    public class Goto34
    {
        public string target { get; set; }
    }

    public class D4A21
    {
        public NyxPage35 nyxpage { get; set; }
    }

    public class NyxPage35
    {
        public Media35 media { get; set; }
        public string text { get; set; }
        public Action35 action { get; set; }
    }

    public class Media35
    {
        public string nyximage { get; set; }
    }

    public class Action35
    {
        public NyxTimer11 nyxtimer { get; set; }
    }

    public class NyxTimer11
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command35[] commands { get; set; }
    }

    public class Command35
    {
        public Goto35 _goto { get; set; }
    }

    public class Goto35
    {
        public string target { get; set; }
    }

    public class D4A22
    {
        public NyxPage36 nyxpage { get; set; }
    }

    public class NyxPage36
    {
        public Media36 media { get; set; }
        public string text { get; set; }
        public Action36 action { get; set; }
        public Instruc8 instruc { get; set; }
    }

    public class Media36
    {
        public string nyximage { get; set; }
    }

    public class Action36
    {
        public NyxVert24 nyxvert { get; set; }
    }

    public class NyxVert24
    {
        public Element24[] elements { get; set; }
    }

    public class Element24
    {
        public NyxButtons24[] nyxbuttons { get; set; }
    }

    public class NyxButtons24
    {
        public string label { get; set; }
        public Command36[] commands { get; set; }
    }

    public class Command36
    {
        public Goto36 _goto { get; set; }
    }

    public class Goto36
    {
        public string target { get; set; }
    }

    public class Instruc8
    {
        public NyxTimer12 nyxtimer { get; set; }
    }

    public class NyxTimer12
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command37[] commands { get; set; }
    }

    public class Command37
    {
        public Goto37 _goto { get; set; }
    }

    public class Goto37
    {
        public string target { get; set; }
    }

    public class D4A3
    {
        public NyxPage37 nyxpage { get; set; }
    }

    public class NyxPage37
    {
        public Media37 media { get; set; }
        public string text { get; set; }
        public Action37 action { get; set; }
    }

    public class Media37
    {
        public string nyximage { get; set; }
    }

    public class Action37
    {
        public NyxVert25 nyxvert { get; set; }
    }

    public class NyxVert25
    {
        public Element25[] elements { get; set; }
    }

    public class Element25
    {
        public NyxButtons25[] nyxbuttons { get; set; }
    }

    public class NyxButtons25
    {
        public string label { get; set; }
        public Command38[] commands { get; set; }
    }

    public class Command38
    {
        public Goto38 _goto { get; set; }
    }

    public class Goto38
    {
        public string target { get; set; }
    }

    public class D4A4
    {
        public NyxPage38 nyxpage { get; set; }
    }

    public class NyxPage38
    {
        public Media38 media { get; set; }
        public string text { get; set; }
        public Action38 action { get; set; }
    }

    public class Media38
    {
        public string nyximage { get; set; }
    }

    public class Action38
    {
        public NyxVert26 nyxvert { get; set; }
    }

    public class NyxVert26
    {
        public Element26[] elements { get; set; }
    }

    public class Element26
    {
        public NyxButtons26[] nyxbuttons { get; set; }
    }

    public class NyxButtons26
    {
        public string label { get; set; }
        public Command39[] commands { get; set; }
    }

    public class Command39
    {
        public Goto39 _goto { get; set; }
    }

    public class Goto39
    {
        public string target { get; set; }
    }

    public class D4A5
    {
        public NyxPage39 nyxpage { get; set; }
    }

    public class NyxPage39
    {
        public Media39 media { get; set; }
        public string text { get; set; }
        public Action39 action { get; set; }
    }

    public class Media39
    {
        public string nyximage { get; set; }
    }

    public class Action39
    {
        public NyxVert27 nyxvert { get; set; }
    }

    public class NyxVert27
    {
        public Element27[] elements { get; set; }
    }

    public class Element27
    {
        public NyxButtons27[] nyxbuttons { get; set; }
    }

    public class NyxButtons27
    {
        public string label { get; set; }
        public Command40[] commands { get; set; }
    }

    public class Command40
    {
        public Goto40 _goto { get; set; }
    }

    public class Goto40
    {
        public string target { get; set; }
    }

    public class D4A6
    {
        public NyxPage40 nyxpage { get; set; }
    }

    public class NyxPage40
    {
        public Media40 media { get; set; }
        public string text { get; set; }
        public Action40 action { get; set; }
    }

    public class Media40
    {
        public string nyximage { get; set; }
    }

    public class Action40
    {
        public NyxVert28 nyxvert { get; set; }
    }

    public class NyxVert28
    {
        public Element28[] elements { get; set; }
    }

    public class Element28
    {
        public NyxButtons28[] nyxbuttons { get; set; }
    }

    public class NyxButtons28
    {
        public string label { get; set; }
        public Command41[] commands { get; set; }
    }

    public class Command41
    {
        public Goto41 _goto { get; set; }
    }

    public class Goto41
    {
        public string target { get; set; }
    }

    public class D4A7
    {
        public NyxPage41 nyxpage { get; set; }
    }

    public class NyxPage41
    {
        public Media41 media { get; set; }
        public string text { get; set; }
        public Action41 action { get; set; }
    }

    public class Media41
    {
        public string nyximage { get; set; }
    }

    public class Action41
    {
        public NyxVert29 nyxvert { get; set; }
    }

    public class NyxVert29
    {
        public Element29[] elements { get; set; }
    }

    public class Element29
    {
        public NyxButtons29[] nyxbuttons { get; set; }
    }

    public class NyxButtons29
    {
        public string label { get; set; }
        public Command42[] commands { get; set; }
    }

    public class Command42
    {
        public Goto42 _goto { get; set; }
    }

    public class Goto42
    {
        public string target { get; set; }
    }

    public class D4A8
    {
        public NyxPage42 nyxpage { get; set; }
    }

    public class NyxPage42
    {
        public Media42 media { get; set; }
        public string text { get; set; }
        public Action42 action { get; set; }
    }

    public class Media42
    {
        public string nyximage { get; set; }
    }

    public class Action42
    {
        public NyxVert30 nyxvert { get; set; }
    }

    public class NyxVert30
    {
        public Element30[] elements { get; set; }
    }

    public class Element30
    {
        public NyxButtons30[] nyxbuttons { get; set; }
    }

    public class NyxButtons30
    {
        public string label { get; set; }
        public Command43[] commands { get; set; }
    }

    public class Command43
    {
        public Goto43 _goto { get; set; }
    }

    public class Goto43
    {
        public string target { get; set; }
    }

    public class D4A9
    {
        public NyxPage43 nyxpage { get; set; }
    }

    public class NyxPage43
    {
        public Media43 media { get; set; }
        public string text { get; set; }
        public Action43 action { get; set; }
    }

    public class Media43
    {
        public string nyximage { get; set; }
    }

    public class Action43
    {
        public NyxVert31 nyxvert { get; set; }
    }

    public class NyxVert31
    {
        public Element31[] elements { get; set; }
    }

    public class Element31
    {
        public NyxButtons31[] nyxbuttons { get; set; }
    }

    public class NyxButtons31
    {
        public string label { get; set; }
        public Command44[] commands { get; set; }
    }

    public class Command44
    {
        public Goto44 _goto { get; set; }
    }

    public class Goto44
    {
        public string target { get; set; }
    }

    public class D4end
    {
        public NyxPage44 nyxpage { get; set; }
    }

    public class NyxPage44
    {
        public Media44 media { get; set; }
        public string text { get; set; }
        public Action44 action { get; set; }
    }

    public class Media44
    {
        public string nyximage { get; set; }
    }

    public class Action44
    {
        public NyxVert32 nyxvert { get; set; }
    }

    public class NyxVert32
    {
        public Element32[] elements { get; set; }
    }

    public class Element32
    {
        public NyxButtons32[] nyxbuttons { get; set; }
    }

    public class NyxButtons32
    {
        public string label { get; set; }
        public Command45[] commands { get; set; }
    }

    public class Command45
    {
        public Goto45 _goto { get; set; }
    }

    public class Goto45
    {
        public string target { get; set; }
    }

    public class D4enda
    {
        public NyxPage45 nyxpage { get; set; }
    }

    public class NyxPage45
    {
        public Media45 media { get; set; }
        public string text { get; set; }
        public Action45 action { get; set; }
    }

    public class Media45
    {
        public string nyximage { get; set; }
    }

    public class Action45
    {
        public NyxVert33 nyxvert { get; set; }
    }

    public class NyxVert33
    {
        public Element33[] elements { get; set; }
    }

    public class Element33
    {
        public NyxButtons33[] nyxbuttons { get; set; }
    }

    public class NyxButtons33
    {
        public string label { get; set; }
        public Command46[] commands { get; set; }
    }

    public class Command46
    {
        public Goto46 _goto { get; set; }
    }

    public class Goto46
    {
        public string target { get; set; }
    }

    public class D4endb
    {
        public NyxPage46 nyxpage { get; set; }
    }

    public class NyxPage46
    {
        public Media46 media { get; set; }
        public string text { get; set; }
        public Action46 action { get; set; }
    }

    public class Media46
    {
        public string nyximage { get; set; }
    }

    public class Action46
    {
        public NyxVert34 nyxvert { get; set; }
    }

    public class NyxVert34
    {
        public Element34[] elements { get; set; }
    }

    public class Element34
    {
        public NyxButtons34[] nyxbuttons { get; set; }
    }

    public class NyxButtons34
    {
        public string label { get; set; }
        public Command47[] commands { get; set; }
    }

    public class Command47
    {
        public Goto47 _goto { get; set; }
    }

    public class Goto47
    {
        public string target { get; set; }
    }

    public class D4endc
    {
        public NyxPage47 nyxpage { get; set; }
    }

    public class NyxPage47
    {
        public Media47 media { get; set; }
        public string text { get; set; }
        public Action47 action { get; set; }
    }

    public class Media47
    {
        public string nyximage { get; set; }
    }

    public class Action47
    {
        public NyxVert35 nyxvert { get; set; }
    }

    public class NyxVert35
    {
        public Element35[] elements { get; set; }
    }

    public class Element35
    {
        public NyxButtons35[] nyxbuttons { get; set; }
    }

    public class NyxButtons35
    {
        public string label { get; set; }
        public Command48[] commands { get; set; }
    }

    public class Command48
    {
        public Goto48 _goto { get; set; }
    }

    public class Goto48
    {
        public string target { get; set; }
    }

    public class D4endd
    {
        public NyxPage48 nyxpage { get; set; }
    }

    public class NyxPage48
    {
        public Media48 media { get; set; }
        public string text { get; set; }
        public Action48 action { get; set; }
    }

    public class Media48
    {
        public string nyximage { get; set; }
    }

    public class Action48
    {
        public NyxVert36 nyxvert { get; set; }
    }

    public class NyxVert36
    {
        public Element36[] elements { get; set; }
    }

    public class Element36
    {
        public NyxButtons36[] nyxbuttons { get; set; }
    }

    public class NyxButtons36
    {
        public string label { get; set; }
        public Command49[] commands { get; set; }
    }

    public class Command49
    {
        public Goto49 _goto { get; set; }
    }

    public class Goto49
    {
        public string target { get; set; }
    }

    public class D4ende
    {
        public NyxPage49 nyxpage { get; set; }
    }

    public class NyxPage49
    {
        public Media49 media { get; set; }
        public string text { get; set; }
    }

    public class Media49
    {
        public string nyximage { get; set; }
    }

    public class D4fail
    {
        public NyxPage50 nyxpage { get; set; }
    }

    public class NyxPage50
    {
        public Media50 media { get; set; }
        public string text { get; set; }
        public Action49 action { get; set; }
    }

    public class Media50
    {
        public string nyximage { get; set; }
    }

    public class Action49
    {
        public NyxVert37 nyxvert { get; set; }
    }

    public class NyxVert37
    {
        public Element37[] elements { get; set; }
    }

    public class Element37
    {
        public NyxButtons37[] nyxbuttons { get; set; }
    }

    public class NyxButtons37
    {
        public string label { get; set; }
        public Command50[] commands { get; set; }
    }

    public class Command50
    {
        public Goto50 _goto { get; set; }
    }

    public class Goto50
    {
        public string target { get; set; }
    }

    public class D4fail2
    {
        public NyxPage51 nyxpage { get; set; }
    }

    public class NyxPage51
    {
        public Media51 media { get; set; }
        public string text { get; set; }
        public Action50 action { get; set; }
    }

    public class Media51
    {
        public string nyximage { get; set; }
    }

    public class Action50
    {
        public NyxVert38 nyxvert { get; set; }
    }

    public class NyxVert38
    {
        public Element38[] elements { get; set; }
    }

    public class Element38
    {
        public NyxButtons38[] nyxbuttons { get; set; }
    }

    public class NyxButtons38
    {
        public string label { get; set; }
        public Command51[] commands { get; set; }
    }

    public class Command51
    {
        public Goto51 _goto { get; set; }
    }

    public class Goto51
    {
        public string target { get; set; }
    }

    public class D4fail3
    {
        public NyxPage52 nyxpage { get; set; }
    }

    public class NyxPage52
    {
        public Media52 media { get; set; }
        public string text { get; set; }
        public Action51 action { get; set; }
    }

    public class Media52
    {
        public string nyximage { get; set; }
    }

    public class Action51
    {
        public NyxVert39 nyxvert { get; set; }
    }

    public class NyxVert39
    {
        public Element39[] elements { get; set; }
    }

    public class Element39
    {
        public NyxButtons39[] nyxbuttons { get; set; }
    }

    public class NyxButtons39
    {
        public string label { get; set; }
        public Command52[] commands { get; set; }
    }

    public class Command52
    {
        public Goto52 _goto { get; set; }
    }

    public class Goto52
    {
        public string target { get; set; }
    }

    public class D4fail4
    {
        public NyxPage53 nyxpage { get; set; }
    }

    public class NyxPage53
    {
        public Media53 media { get; set; }
        public string text { get; set; }
    }

    public class Media53
    {
        public string nyximage { get; set; }
    }

    public class D4failt1
    {
        public NyxPage54 nyxpage { get; set; }
    }

    public class NyxPage54
    {
        public Media54 media { get; set; }
        public string text { get; set; }
        public Action52 action { get; set; }
    }

    public class Media54
    {
        public string nyximage { get; set; }
    }

    public class Action52
    {
        public NyxVert40 nyxvert { get; set; }
    }

    public class NyxVert40
    {
        public Element40[] elements { get; set; }
    }

    public class Element40
    {
        public NyxButtons40[] nyxbuttons { get; set; }
    }

    public class NyxButtons40
    {
        public string label { get; set; }
        public Command53[] commands { get; set; }
    }

    public class Command53
    {
        public Goto53 _goto { get; set; }
    }

    public class Goto53
    {
        public string target { get; set; }
    }

    public class D4failt10
    {
        public NyxPage55 nyxpage { get; set; }
    }

    public class NyxPage55
    {
        public Media55 media { get; set; }
        public string text { get; set; }
        public Action53 action { get; set; }
    }

    public class Media55
    {
        public string nyximage { get; set; }
    }

    public class Action53
    {
        public NyxVert41 nyxvert { get; set; }
    }

    public class NyxVert41
    {
        public Element41[] elements { get; set; }
    }

    public class Element41
    {
        public NyxButtons41[] nyxbuttons { get; set; }
    }

    public class NyxButtons41
    {
        public string label { get; set; }
        public Command54[] commands { get; set; }
    }

    public class Command54
    {
        public Goto54 _goto { get; set; }
    }

    public class Goto54
    {
        public string target { get; set; }
    }

    public class D4failt2
    {
        public NyxPage56 nyxpage { get; set; }
    }

    public class NyxPage56
    {
        public Media56 media { get; set; }
        public string text { get; set; }
        public Action54 action { get; set; }
    }

    public class Media56
    {
        public string nyximage { get; set; }
    }

    public class Action54
    {
        public NyxVert42 nyxvert { get; set; }
    }

    public class NyxVert42
    {
        public Element42[] elements { get; set; }
    }

    public class Element42
    {
        public NyxButtons42[] nyxbuttons { get; set; }
    }

    public class NyxButtons42
    {
        public string label { get; set; }
        public Command55[] commands { get; set; }
    }

    public class Command55
    {
        public Goto55 _goto { get; set; }
    }

    public class Goto55
    {
        public string target { get; set; }
    }

    public class D4failt3
    {
        public NyxPage57 nyxpage { get; set; }
    }

    public class NyxPage57
    {
        public Media57 media { get; set; }
        public string text { get; set; }
        public Action55 action { get; set; }
    }

    public class Media57
    {
        public string nyximage { get; set; }
    }

    public class Action55
    {
        public NyxVert43 nyxvert { get; set; }
    }

    public class NyxVert43
    {
        public Element43[] elements { get; set; }
    }

    public class Element43
    {
        public NyxButtons43[] nyxbuttons { get; set; }
    }

    public class NyxButtons43
    {
        public string label { get; set; }
        public Command56[] commands { get; set; }
    }

    public class Command56
    {
        public Goto56 _goto { get; set; }
    }

    public class Goto56
    {
        public string target { get; set; }
    }

    public class D4failt4
    {
        public NyxPage58 nyxpage { get; set; }
    }

    public class NyxPage58
    {
        public Media58 media { get; set; }
        public string text { get; set; }
        public Action56 action { get; set; }
    }

    public class Media58
    {
        public string nyximage { get; set; }
    }

    public class Action56
    {
        public NyxVert44 nyxvert { get; set; }
    }

    public class NyxVert44
    {
        public Element44[] elements { get; set; }
    }

    public class Element44
    {
        public NyxButtons44[] nyxbuttons { get; set; }
    }

    public class NyxButtons44
    {
        public string label { get; set; }
        public Command57[] commands { get; set; }
    }

    public class Command57
    {
        public Goto57 _goto { get; set; }
    }

    public class Goto57
    {
        public string target { get; set; }
    }

    public class D4failt5
    {
        public NyxPage59 nyxpage { get; set; }
    }

    public class NyxPage59
    {
        public Media59 media { get; set; }
        public string text { get; set; }
        public Action57 action { get; set; }
    }

    public class Media59
    {
        public string nyximage { get; set; }
    }

    public class Action57
    {
        public NyxVert45 nyxvert { get; set; }
    }

    public class NyxVert45
    {
        public Element45[] elements { get; set; }
    }

    public class Element45
    {
        public NyxButtons45[] nyxbuttons { get; set; }
    }

    public class NyxButtons45
    {
        public string label { get; set; }
        public Command58[] commands { get; set; }
    }

    public class Command58
    {
        public Goto58 _goto { get; set; }
    }

    public class Goto58
    {
        public string target { get; set; }
    }

    public class D4failt6
    {
        public NyxPage60 nyxpage { get; set; }
    }

    public class NyxPage60
    {
        public Media60 media { get; set; }
        public string text { get; set; }
        public Action58 action { get; set; }
    }

    public class Media60
    {
        public string nyximage { get; set; }
    }

    public class Action58
    {
        public NyxVert46 nyxvert { get; set; }
    }

    public class NyxVert46
    {
        public Element46[] elements { get; set; }
    }

    public class Element46
    {
        public NyxButtons46[] nyxbuttons { get; set; }
    }

    public class NyxButtons46
    {
        public string label { get; set; }
        public Command59[] commands { get; set; }
    }

    public class Command59
    {
        public Goto59 _goto { get; set; }
    }

    public class Goto59
    {
        public string target { get; set; }
    }

    public class D4failt7
    {
        public NyxPage61 nyxpage { get; set; }
    }

    public class NyxPage61
    {
        public Media61 media { get; set; }
        public string text { get; set; }
        public Action59 action { get; set; }
    }

    public class Media61
    {
        public string nyximage { get; set; }
    }

    public class Action59
    {
        public NyxVert47 nyxvert { get; set; }
    }

    public class NyxVert47
    {
        public Element47[] elements { get; set; }
    }

    public class Element47
    {
        public NyxButtons47[] nyxbuttons { get; set; }
    }

    public class NyxButtons47
    {
        public string label { get; set; }
        public Command60[] commands { get; set; }
    }

    public class Command60
    {
        public Goto60 _goto { get; set; }
    }

    public class Goto60
    {
        public string target { get; set; }
    }

    public class D4failt8
    {
        public NyxPage62 nyxpage { get; set; }
    }

    public class NyxPage62
    {
        public Media62 media { get; set; }
        public string text { get; set; }
        public Action60 action { get; set; }
    }

    public class Media62
    {
        public string nyximage { get; set; }
    }

    public class Action60
    {
        public NyxVert48 nyxvert { get; set; }
    }

    public class NyxVert48
    {
        public Element48[] elements { get; set; }
    }

    public class Element48
    {
        public NyxButtons48[] nyxbuttons { get; set; }
    }

    public class NyxButtons48
    {
        public string label { get; set; }
        public Command61[] commands { get; set; }
    }

    public class Command61
    {
        public Goto61 _goto { get; set; }
    }

    public class Goto61
    {
        public string target { get; set; }
    }

    public class D4failt9
    {
        public NyxPage63 nyxpage { get; set; }
    }

    public class NyxPage63
    {
        public Media63 media { get; set; }
        public string text { get; set; }
        public Action61 action { get; set; }
    }

    public class Media63
    {
        public string nyximage { get; set; }
    }

    public class Action61
    {
        public NyxVert49 nyxvert { get; set; }
    }

    public class NyxVert49
    {
        public Element49[] elements { get; set; }
    }

    public class Element49
    {
        public NyxButtons49[] nyxbuttons { get; set; }
    }

    public class NyxButtons49
    {
        public string label { get; set; }
        public Command62[] commands { get; set; }
    }

    public class Command62
    {
        public Goto62 _goto { get; set; }
    }

    public class Goto62
    {
        public string target { get; set; }
    }

    public class D4T10B1
    {
        public NyxPage64 nyxpage { get; set; }
    }

    public class NyxPage64
    {
        public Media64 media { get; set; }
        public string text { get; set; }
        public Action62 action { get; set; }
    }

    public class Media64
    {
        public string nyximage { get; set; }
    }

    public class Action62
    {
        public NyxVert50 nyxvert { get; set; }
    }

    public class NyxVert50
    {
        public Element50[] elements { get; set; }
    }

    public class Element50
    {
        public NyxButtons50[] nyxbuttons { get; set; }
    }

    public class NyxButtons50
    {
        public string label { get; set; }
        public Command63[] commands { get; set; }
    }

    public class Command63
    {
        public Goto63 _goto { get; set; }
    }

    public class Goto63
    {
        public string target { get; set; }
    }

    public class D4T10B2
    {
        public NyxPage65 nyxpage { get; set; }
    }

    public class NyxPage65
    {
        public Media65 media { get; set; }
        public string text { get; set; }
        public Action63 action { get; set; }
        public Instruc9 instruc { get; set; }
    }

    public class Media65
    {
        public string nyximage { get; set; }
    }

    public class Action63
    {
        public NyxVert51 nyxvert { get; set; }
    }

    public class NyxVert51
    {
        public Element51[] elements { get; set; }
    }

    public class Element51
    {
        public NyxButtons51[] nyxbuttons { get; set; }
    }

    public class NyxButtons51
    {
        public string label { get; set; }
        public Command64[] commands { get; set; }
    }

    public class Command64
    {
        public Goto64 _goto { get; set; }
    }

    public class Goto64
    {
        public string target { get; set; }
    }

    public class Instruc9
    {
        public NyxTimer13 nyxtimer { get; set; }
    }

    public class NyxTimer13
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command65[] commands { get; set; }
    }

    public class Command65
    {
        public Goto65 _goto { get; set; }
    }

    public class Goto65
    {
        public string target { get; set; }
    }

    public class D4T10B3
    {
        public NyxPage66 nyxpage { get; set; }
    }

    public class NyxPage66
    {
        public Media66 media { get; set; }
        public string text { get; set; }
        public Action64 action { get; set; }
        public Instruc10 instruc { get; set; }
    }

    public class Media66
    {
        public string nyximage { get; set; }
    }

    public class Action64
    {
        public NyxVert52 nyxvert { get; set; }
    }

    public class NyxVert52
    {
        public Element52[] elements { get; set; }
    }

    public class Element52
    {
        public NyxButtons52[] nyxbuttons { get; set; }
    }

    public class NyxButtons52
    {
        public string label { get; set; }
        public Command66[] commands { get; set; }
    }

    public class Command66
    {
        public Goto66 _goto { get; set; }
    }

    public class Goto66
    {
        public string target { get; set; }
    }

    public class Instruc10
    {
        public NyxTimer14 nyxtimer { get; set; }
    }

    public class NyxTimer14
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command67[] commands { get; set; }
    }

    public class Command67
    {
        public Goto67 _goto { get; set; }
    }

    public class Goto67
    {
        public string target { get; set; }
    }

    public class D4T10B4
    {
        public NyxPage67 nyxpage { get; set; }
    }

    public class NyxPage67
    {
        public Media67 media { get; set; }
        public string text { get; set; }
        public Action65 action { get; set; }
    }

    public class Media67
    {
        public string nyximage { get; set; }
    }

    public class Action65
    {
        public NyxVert53 nyxvert { get; set; }
    }

    public class NyxVert53
    {
        public Element53[] elements { get; set; }
    }

    public class Element53
    {
        public NyxButtons53[] nyxbuttons { get; set; }
    }

    public class NyxButtons53
    {
        public string label { get; set; }
        public Command68[] commands { get; set; }
    }

    public class Command68
    {
        public Goto68 _goto { get; set; }
    }

    public class Goto68
    {
        public string target { get; set; }
    }

    public class D4t10bearly
    {
        public NyxPage68 nyxpage { get; set; }
    }

    public class NyxPage68
    {
        public Media68 media { get; set; }
        public string text { get; set; }
        public Action66 action { get; set; }
    }

    public class Media68
    {
        public string nyximage { get; set; }
    }

    public class Action66
    {
        public NyxVert54 nyxvert { get; set; }
    }

    public class NyxVert54
    {
        public Element54[] elements { get; set; }
    }

    public class Element54
    {
        public NyxButtons54[] nyxbuttons { get; set; }
    }

    public class NyxButtons54
    {
        public string label { get; set; }
        public Command69[] commands { get; set; }
    }

    public class Command69
    {
        public Goto69 _goto { get; set; }
    }

    public class Goto69
    {
        public string target { get; set; }
    }

    public class D4t10early
    {
        public NyxPage69 nyxpage { get; set; }
    }

    public class NyxPage69
    {
        public Media69 media { get; set; }
        public string text { get; set; }
        public Action67 action { get; set; }
    }

    public class Media69
    {
        public string nyximage { get; set; }
    }

    public class Action67
    {
        public NyxVert55 nyxvert { get; set; }
    }

    public class NyxVert55
    {
        public Element55[] elements { get; set; }
    }

    public class Element55
    {
        public NyxButtons55[] nyxbuttons { get; set; }
    }

    public class NyxButtons55
    {
        public string label { get; set; }
        public Command70[] commands { get; set; }
    }

    public class Command70
    {
        public Goto70 _goto { get; set; }
    }

    public class Goto70
    {
        public string target { get; set; }
    }

    public class D4T10P1
    {
        public NyxPage70 nyxpage { get; set; }
    }

    public class NyxPage70
    {
        public Media70 media { get; set; }
        public string text { get; set; }
        public Action68 action { get; set; }
    }

    public class Media70
    {
        public string nyximage { get; set; }
    }

    public class Action68
    {
        public NyxVert56 nyxvert { get; set; }
    }

    public class NyxVert56
    {
        public Element56[] elements { get; set; }
    }

    public class Element56
    {
        public NyxButtons56[] nyxbuttons { get; set; }
    }

    public class NyxButtons56
    {
        public string label { get; set; }
        public Command71[] commands { get; set; }
    }

    public class Command71
    {
        public Goto71 _goto { get; set; }
    }

    public class Goto71
    {
        public string target { get; set; }
    }

    public class D4T10P2
    {
        public NyxPage71 nyxpage { get; set; }
    }

    public class NyxPage71
    {
        public Media71 media { get; set; }
        public string text { get; set; }
        public Action69 action { get; set; }
        public Instruc11 instruc { get; set; }
    }

    public class Media71
    {
        public string nyximage { get; set; }
    }

    public class Action69
    {
        public NyxVert57 nyxvert { get; set; }
    }

    public class NyxVert57
    {
        public Element57[] elements { get; set; }
    }

    public class Element57
    {
        public NyxButtons57[] nyxbuttons { get; set; }
    }

    public class NyxButtons57
    {
        public string label { get; set; }
        public Command72[] commands { get; set; }
    }

    public class Command72
    {
        public Goto72 _goto { get; set; }
    }

    public class Goto72
    {
        public string target { get; set; }
    }

    public class Instruc11
    {
        public NyxTimer15 nyxtimer { get; set; }
    }

    public class NyxTimer15
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command73[] commands { get; set; }
    }

    public class Command73
    {
        public Goto73 _goto { get; set; }
    }

    public class Goto73
    {
        public string target { get; set; }
    }

    public class D4T10P3
    {
        public NyxPage72 nyxpage { get; set; }
    }

    public class NyxPage72
    {
        public Media72 media { get; set; }
        public string text { get; set; }
        public Action70 action { get; set; }
        public Instruc12 instruc { get; set; }
    }

    public class Media72
    {
        public string nyximage { get; set; }
    }

    public class Action70
    {
        public NyxVert58 nyxvert { get; set; }
    }

    public class NyxVert58
    {
        public Element58[] elements { get; set; }
    }

    public class Element58
    {
        public NyxButtons58[] nyxbuttons { get; set; }
    }

    public class NyxButtons58
    {
        public string label { get; set; }
        public Command74[] commands { get; set; }
    }

    public class Command74
    {
        public Goto74 _goto { get; set; }
    }

    public class Goto74
    {
        public string target { get; set; }
    }

    public class Instruc12
    {
        public NyxTimer16 nyxtimer { get; set; }
    }

    public class NyxTimer16
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command75[] commands { get; set; }
    }

    public class Command75
    {
        public Goto75 _goto { get; set; }
    }

    public class Goto75
    {
        public string target { get; set; }
    }

    public class D4T10P4
    {
        public NyxPage73 nyxpage { get; set; }
    }

    public class NyxPage73
    {
        public Media73 media { get; set; }
        public string text { get; set; }
        public Action71 action { get; set; }
    }

    public class Media73
    {
        public string nyximage { get; set; }
    }

    public class Action71
    {
        public NyxVert59 nyxvert { get; set; }
    }

    public class NyxVert59
    {
        public Element59[] elements { get; set; }
    }

    public class Element59
    {
        public NyxButtons59[] nyxbuttons { get; set; }
    }

    public class NyxButtons59
    {
        public string label { get; set; }
        public Command76[] commands { get; set; }
    }

    public class Command76
    {
        public Goto76 _goto { get; set; }
    }

    public class Goto76
    {
        public string target { get; set; }
    }

    public class D4T10SA
    {
        public NyxPage74 nyxpage { get; set; }
    }

    public class NyxPage74
    {
        public Media74 media { get; set; }
        public string text { get; set; }
        public Action72 action { get; set; }
    }

    public class Media74
    {
        public string nyximage { get; set; }
    }

    public class Action72
    {
        public NyxVert60 nyxvert { get; set; }
    }

    public class NyxVert60
    {
        public Element60[] elements { get; set; }
    }

    public class Element60
    {
        public NyxButtons60[] nyxbuttons { get; set; }
    }

    public class NyxButtons60
    {
        public string label { get; set; }
        public Command77[] commands { get; set; }
    }

    public class Command77
    {
        public Goto77 _goto { get; set; }
    }

    public class Goto77
    {
        public string target { get; set; }
    }

    public class D4t10together
    {
        public NyxPage75 nyxpage { get; set; }
    }

    public class NyxPage75
    {
        public Media75 media { get; set; }
        public string text { get; set; }
        public Action73 action { get; set; }
    }

    public class Media75
    {
        public string nyximage { get; set; }
    }

    public class Action73
    {
        public NyxVert61 nyxvert { get; set; }
    }

    public class NyxVert61
    {
        public Element61[] elements { get; set; }
    }

    public class Element61
    {
        public NyxButtons61[] nyxbuttons { get; set; }
    }

    public class NyxButtons61
    {
        public string label { get; set; }
        public Command78[] commands { get; set; }
    }

    public class Command78
    {
        public Goto78 _goto { get; set; }
    }

    public class Goto78
    {
        public string target { get; set; }
    }

    public class D4t5came
    {
        public NyxPage76 nyxpage { get; set; }
    }

    public class NyxPage76
    {
        public Media76 media { get; set; }
        public string text { get; set; }
        public Action74 action { get; set; }
    }

    public class Media76
    {
        public string nyximage { get; set; }
    }

    public class Action74
    {
        public NyxVert62 nyxvert { get; set; }
    }

    public class NyxVert62
    {
        public Element62[] elements { get; set; }
    }

    public class Element62
    {
        public NyxButtons62[] nyxbuttons { get; set; }
    }

    public class NyxButtons62
    {
        public string label { get; set; }
        public Command79[] commands { get; set; }
    }

    public class Command79
    {
        public Goto79 _goto { get; set; }
    }

    public class Goto79
    {
        public string target { get; set; }
    }

    public class D4T9A
    {
        public NyxPage77 nyxpage { get; set; }
    }

    public class NyxPage77
    {
        public Media77 media { get; set; }
        public string text { get; set; }
        public Action75 action { get; set; }
        public Instruc13 instruc { get; set; }
    }

    public class Media77
    {
        public string nyximage { get; set; }
    }

    public class Action75
    {
        public NyxVert63 nyxvert { get; set; }
    }

    public class NyxVert63
    {
        public Element63[] elements { get; set; }
    }

    public class Element63
    {
        public NyxButtons63[] nyxbuttons { get; set; }
    }

    public class NyxButtons63
    {
        public string label { get; set; }
        public Command80[] commands { get; set; }
    }

    public class Command80
    {
        public Goto80 _goto { get; set; }
    }

    public class Goto80
    {
        public string target { get; set; }
    }

    public class Instruc13
    {
        public NyxTimer17 nyxtimer { get; set; }
    }

    public class NyxTimer17
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command81[] commands { get; set; }
    }

    public class Command81
    {
        public Goto81 _goto { get; set; }
    }

    public class Goto81
    {
        public string target { get; set; }
    }

    public class D4T9A2
    {
        public NyxPage78 nyxpage { get; set; }
    }

    public class NyxPage78
    {
        public Media78 media { get; set; }
        public string text { get; set; }
        public Action76 action { get; set; }
        public Instruc14 instruc { get; set; }
    }

    public class Media78
    {
        public string nyximage { get; set; }
    }

    public class Action76
    {
        public NyxVert64 nyxvert { get; set; }
    }

    public class NyxVert64
    {
        public Element64[] elements { get; set; }
    }

    public class Element64
    {
        public NyxButtons64[] nyxbuttons { get; set; }
    }

    public class NyxButtons64
    {
        public string label { get; set; }
        public Command82[] commands { get; set; }
    }

    public class Command82
    {
        public Goto82 _goto { get; set; }
    }

    public class Goto82
    {
        public string target { get; set; }
    }

    public class Instruc14
    {
        public NyxTimer18 nyxtimer { get; set; }
    }

    public class NyxTimer18
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command83[] commands { get; set; }
    }

    public class Command83
    {
        public Goto83 _goto { get; set; }
    }

    public class Goto83
    {
        public string target { get; set; }
    }

    public class D4T9B2
    {
        public NyxPage79 nyxpage { get; set; }
    }

    public class NyxPage79
    {
        public Media79 media { get; set; }
        public string text { get; set; }
        public Action77 action { get; set; }
        public Instruc15 instruc { get; set; }
    }

    public class Media79
    {
        public string nyximage { get; set; }
    }

    public class Action77
    {
        public NyxVert65 nyxvert { get; set; }
    }

    public class NyxVert65
    {
        public Element65[] elements { get; set; }
    }

    public class Element65
    {
        public NyxButtons65[] nyxbuttons { get; set; }
    }

    public class NyxButtons65
    {
        public string label { get; set; }
        public Command84[] commands { get; set; }
    }

    public class Command84
    {
        public Goto84 _goto { get; set; }
    }

    public class Goto84
    {
        public string target { get; set; }
    }

    public class Instruc15
    {
        public NyxTimer19 nyxtimer { get; set; }
    }

    public class NyxTimer19
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command85[] commands { get; set; }
    }

    public class Command85
    {
        public Goto85 _goto { get; set; }
    }

    public class Goto85
    {
        public string target { get; set; }
    }

    public class D4T9C
    {
        public NyxPage80 nyxpage { get; set; }
    }

    public class NyxPage80
    {
        public Media80 media { get; set; }
        public string text { get; set; }
        public Action78 action { get; set; }
        public Instruc16 instruc { get; set; }
    }

    public class Media80
    {
        public string nyximage { get; set; }
    }

    public class Action78
    {
        public NyxVert66 nyxvert { get; set; }
    }

    public class NyxVert66
    {
        public Element66[] elements { get; set; }
    }

    public class Element66
    {
        public NyxButtons66[] nyxbuttons { get; set; }
    }

    public class NyxButtons66
    {
        public string label { get; set; }
        public Command86[] commands { get; set; }
    }

    public class Command86
    {
        public Goto86 _goto { get; set; }
    }

    public class Goto86
    {
        public string target { get; set; }
    }

    public class Instruc16
    {
        public NyxTimer20 nyxtimer { get; set; }
    }

    public class NyxTimer20
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command87[] commands { get; set; }
    }

    public class Command87
    {
        public Goto87 _goto { get; set; }
    }

    public class Goto87
    {
        public string target { get; set; }
    }

    public class D4T9C2
    {
        public NyxPage81 nyxpage { get; set; }
    }

    public class NyxPage81
    {
        public Media81 media { get; set; }
        public string text { get; set; }
        public Action79 action { get; set; }
        public Instruc17 instruc { get; set; }
    }

    public class Media81
    {
        public string nyximage { get; set; }
    }

    public class Action79
    {
        public NyxVert67 nyxvert { get; set; }
    }

    public class NyxVert67
    {
        public Element67[] elements { get; set; }
    }

    public class Element67
    {
        public NyxButtons67[] nyxbuttons { get; set; }
    }

    public class NyxButtons67
    {
        public string label { get; set; }
        public Command88[] commands { get; set; }
    }

    public class Command88
    {
        public Goto88 _goto { get; set; }
    }

    public class Goto88
    {
        public string target { get; set; }
    }

    public class Instruc17
    {
        public NyxTimer21 nyxtimer { get; set; }
    }

    public class NyxTimer21
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command89[] commands { get; set; }
    }

    public class Command89
    {
        public Goto89 _goto { get; set; }
    }

    public class Goto89
    {
        public string target { get; set; }
    }

    public class D4T9C3
    {
        public NyxPage82 nyxpage { get; set; }
    }

    public class NyxPage82
    {
        public Media82 media { get; set; }
        public string text { get; set; }
        public Action80 action { get; set; }
        public Instruc18 instruc { get; set; }
    }

    public class Media82
    {
        public string nyximage { get; set; }
    }

    public class Action80
    {
        public NyxVert68 nyxvert { get; set; }
    }

    public class NyxVert68
    {
        public Element68[] elements { get; set; }
    }

    public class Element68
    {
        public NyxButtons68[] nyxbuttons { get; set; }
    }

    public class NyxButtons68
    {
        public string label { get; set; }
        public Command90[] commands { get; set; }
    }

    public class Command90
    {
        public Goto90 _goto { get; set; }
    }

    public class Goto90
    {
        public string target { get; set; }
    }

    public class Instruc18
    {
        public NyxTimer22 nyxtimer { get; set; }
    }

    public class NyxTimer22
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command91[] commands { get; set; }
    }

    public class Command91
    {
        public Goto91 _goto { get; set; }
    }

    public class Goto91
    {
        public string target { get; set; }
    }

    public class D4T9D
    {
        public NyxPage83 nyxpage { get; set; }
    }

    public class NyxPage83
    {
        public Media83 media { get; set; }
        public string text { get; set; }
        public Action81 action { get; set; }
        public Instruc19 instruc { get; set; }
    }

    public class Media83
    {
        public string nyximage { get; set; }
    }

    public class Action81
    {
        public NyxVert69 nyxvert { get; set; }
    }

    public class NyxVert69
    {
        public Element69[] elements { get; set; }
    }

    public class Element69
    {
        public NyxButtons69[] nyxbuttons { get; set; }
    }

    public class NyxButtons69
    {
        public string label { get; set; }
        public Command92[] commands { get; set; }
    }

    public class Command92
    {
        public Goto92 _goto { get; set; }
    }

    public class Goto92
    {
        public string target { get; set; }
    }

    public class Instruc19
    {
        public NyxTimer23 nyxtimer { get; set; }
    }

    public class NyxTimer23
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command93[] commands { get; set; }
    }

    public class Command93
    {
        public Goto93 _goto { get; set; }
    }

    public class Goto93
    {
        public string target { get; set; }
    }

    public class D4T9E
    {
        public NyxPage84 nyxpage { get; set; }
    }

    public class NyxPage84
    {
        public Media84 media { get; set; }
        public string text { get; set; }
        public Action82 action { get; set; }
        public Instruc20 instruc { get; set; }
    }

    public class Media84
    {
        public string nyximage { get; set; }
    }

    public class Action82
    {
        public NyxVert70 nyxvert { get; set; }
    }

    public class NyxVert70
    {
        public Element70[] elements { get; set; }
    }

    public class Element70
    {
        public NyxButtons70[] nyxbuttons { get; set; }
    }

    public class NyxButtons70
    {
        public string label { get; set; }
        public Command94[] commands { get; set; }
    }

    public class Command94
    {
        public Goto94 _goto { get; set; }
    }

    public class Goto94
    {
        public string target { get; set; }
    }

    public class Instruc20
    {
        public NyxTimer24 nyxtimer { get; set; }
    }

    public class NyxTimer24
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command95[] commands { get; set; }
    }

    public class Command95
    {
        public Goto95 _goto { get; set; }
    }

    public class Goto95
    {
        public string target { get; set; }
    }

    public class D4T9E2
    {
        public NyxPage85 nyxpage { get; set; }
    }

    public class NyxPage85
    {
        public Media85 media { get; set; }
        public string text { get; set; }
        public Action83 action { get; set; }
        public Instruc21 instruc { get; set; }
    }

    public class Media85
    {
        public string nyximage { get; set; }
    }

    public class Action83
    {
        public NyxVert71 nyxvert { get; set; }
    }

    public class NyxVert71
    {
        public Element71[] elements { get; set; }
    }

    public class Element71
    {
        public NyxButtons71[] nyxbuttons { get; set; }
    }

    public class NyxButtons71
    {
        public string label { get; set; }
        public Command96[] commands { get; set; }
    }

    public class Command96
    {
        public Goto96 _goto { get; set; }
    }

    public class Goto96
    {
        public string target { get; set; }
    }

    public class Instruc21
    {
        public NyxTimer25 nyxtimer { get; set; }
    }

    public class NyxTimer25
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command97[] commands { get; set; }
    }

    public class Command97
    {
        public Goto97 _goto { get; set; }
    }

    public class Goto97
    {
        public string target { get; set; }
    }

    public class Day4
    {
        public NyxPage86 nyxpage { get; set; }
    }

    public class NyxPage86
    {
        public Media86 media { get; set; }
        public string text { get; set; }
        public Action84 action { get; set; }
    }

    public class Media86
    {
        public string nyximage { get; set; }
    }

    public class Action84
    {
        public NyxVert72 nyxvert { get; set; }
    }

    public class NyxVert72
    {
        public Element72[] elements { get; set; }
    }

    public class Element72
    {
        public NyxButtons72[] nyxbuttons { get; set; }
    }

    public class NyxButtons72
    {
        public string label { get; set; }
        public Command98[] commands { get; set; }
    }

    public class Command98
    {
        public Goto98 _goto { get; set; }
    }

    public class Goto98
    {
        public string target { get; set; }
    }

    public class Day4sp
    {
        public NyxPage87 nyxpage { get; set; }
    }

    public class NyxPage87
    {
        public Media87 media { get; set; }
        public string text { get; set; }
        public Action85 action { get; set; }
    }

    public class Media87
    {
        public string nyximage { get; set; }
    }

    public class Action85
    {
        public NyxVert73 nyxvert { get; set; }
    }

    public class NyxVert73
    {
        public Element73[] elements { get; set; }
    }

    public class Element73
    {
        public NyxButtons73[] nyxbuttons { get; set; }
    }

    public class NyxButtons73
    {
        public string label { get; set; }
        public Command99[] commands { get; set; }
    }

    public class Command99
    {
        public Goto99 _goto { get; set; }
    }

    public class Goto99
    {
        public string target { get; set; }
    }

    public class Day5
    {
        public NyxPage88 nyxpage { get; set; }
    }

    public class NyxPage88
    {
        public Media88 media { get; set; }
        public string text { get; set; }
        public Action86 action { get; set; }
    }

    public class Media88
    {
        public string nyximage { get; set; }
    }

    public class Action86
    {
        public NyxVert74 nyxvert { get; set; }
    }

    public class NyxVert74
    {
        public Element74[] elements { get; set; }
    }

    public class Element74
    {
        public NyxButtons74[] nyxbuttons { get; set; }
    }

    public class NyxButtons74
    {
        public string label { get; set; }
        public Command100[] commands { get; set; }
    }

    public class Command100
    {
        public Goto100 _goto { get; set; }
    }

    public class Goto100
    {
        public string target { get; set; }
    }

    public class Day5fail
    {
        public NyxPage89 nyxpage { get; set; }
    }

    public class NyxPage89
    {
        public Media89 media { get; set; }
        public string text { get; set; }
        public Action87 action { get; set; }
    }

    public class Media89
    {
        public string nyximage { get; set; }
    }

    public class Action87
    {
        public NyxVert75 nyxvert { get; set; }
    }

    public class NyxVert75
    {
        public Element75[] elements { get; set; }
    }

    public class Element75
    {
        public NyxButtons75[] nyxbuttons { get; set; }
    }

    public class NyxButtons75
    {
        public string label { get; set; }
        public Command101[] commands { get; set; }
    }

    public class Command101
    {
        public Goto101 _goto { get; set; }
    }

    public class Goto101
    {
        public string target { get; set; }
    }

    public class Day5fail2
    {
        public NyxPage90 nyxpage { get; set; }
    }

    public class NyxPage90
    {
        public Media90 media { get; set; }
        public string text { get; set; }
    }

    public class Media90
    {
        public string nyximage { get; set; }
    }

    public class Day5l1
    {
        public NyxPage91 nyxpage { get; set; }
    }

    public class NyxPage91
    {
        public Media91 media { get; set; }
        public string text { get; set; }
        public Action88 action { get; set; }
    }

    public class Media91
    {
        public string nyximage { get; set; }
    }

    public class Action88
    {
        public NyxVert76 nyxvert { get; set; }
    }

    public class NyxVert76
    {
        public Element76[] elements { get; set; }
    }

    public class Element76
    {
        public NyxButtons76[] nyxbuttons { get; set; }
    }

    public class NyxButtons76
    {
        public string label { get; set; }
        public Command102[] commands { get; set; }
    }

    public class Command102
    {
        public Goto102 _goto { get; set; }
    }

    public class Goto102
    {
        public string target { get; set; }
    }

    public class Day5l10
    {
        public NyxPage92 nyxpage { get; set; }
    }

    public class NyxPage92
    {
        public Media92 media { get; set; }
        public string text { get; set; }
        public Action89 action { get; set; }
    }

    public class Media92
    {
        public string nyximage { get; set; }
    }

    public class Action89
    {
        public NyxVert77 nyxvert { get; set; }
    }

    public class NyxVert77
    {
        public Element77[] elements { get; set; }
    }

    public class Element77
    {
        public NyxButtons77[] nyxbuttons { get; set; }
    }

    public class NyxButtons77
    {
        public string label { get; set; }
        public Command103[] commands { get; set; }
    }

    public class Command103
    {
        public Goto103 _goto { get; set; }
    }

    public class Goto103
    {
        public string target { get; set; }
    }

    public class Day5l11
    {
        public NyxPage93 nyxpage { get; set; }
    }

    public class NyxPage93
    {
        public Media93 media { get; set; }
        public string text { get; set; }
        public Action90 action { get; set; }
    }

    public class Media93
    {
        public string nyximage { get; set; }
    }

    public class Action90
    {
        public NyxVert78 nyxvert { get; set; }
    }

    public class NyxVert78
    {
        public Element78[] elements { get; set; }
    }

    public class Element78
    {
        public NyxButtons78[] nyxbuttons { get; set; }
    }

    public class NyxButtons78
    {
        public string label { get; set; }
        public Command104[] commands { get; set; }
    }

    public class Command104
    {
        public Goto104 _goto { get; set; }
    }

    public class Goto104
    {
        public string target { get; set; }
    }

    public class Day5l12
    {
        public NyxPage94 nyxpage { get; set; }
    }

    public class NyxPage94
    {
        public Media94 media { get; set; }
        public string text { get; set; }
        public Action91 action { get; set; }
    }

    public class Media94
    {
        public string nyximage { get; set; }
    }

    public class Action91
    {
        public NyxVert79 nyxvert { get; set; }
    }

    public class NyxVert79
    {
        public Element79[] elements { get; set; }
    }

    public class Element79
    {
        public NyxButtons79[] nyxbuttons { get; set; }
    }

    public class NyxButtons79
    {
        public string label { get; set; }
        public Command105[] commands { get; set; }
    }

    public class Command105
    {
        public Goto105 _goto { get; set; }
    }

    public class Goto105
    {
        public string target { get; set; }
    }

    public class Day5l13n
    {
        public NyxPage95 nyxpage { get; set; }
    }

    public class NyxPage95
    {
        public Media95 media { get; set; }
        public string text { get; set; }
        public Action92 action { get; set; }
    }

    public class Media95
    {
        public string nyximage { get; set; }
    }

    public class Action92
    {
        public NyxVert80 nyxvert { get; set; }
    }

    public class NyxVert80
    {
        public Element80[] elements { get; set; }
    }

    public class Element80
    {
        public NyxButtons80[] nyxbuttons { get; set; }
    }

    public class NyxButtons80
    {
        public string label { get; set; }
        public Command106[] commands { get; set; }
    }

    public class Command106
    {
        public Goto106 _goto { get; set; }
    }

    public class Goto106
    {
        public string target { get; set; }
    }

    public class Day5l13y
    {
        public NyxPage96 nyxpage { get; set; }
    }

    public class NyxPage96
    {
        public Media96 media { get; set; }
        public string text { get; set; }
        public Action93 action { get; set; }
    }

    public class Media96
    {
        public string nyximage { get; set; }
    }

    public class Action93
    {
        public NyxVert81 nyxvert { get; set; }
    }

    public class NyxVert81
    {
        public Element81[] elements { get; set; }
    }

    public class Element81
    {
        public NyxButtons81[] nyxbuttons { get; set; }
    }

    public class NyxButtons81
    {
        public string label { get; set; }
        public Command107[] commands { get; set; }
    }

    public class Command107
    {
        public Goto107 _goto { get; set; }
    }

    public class Goto107
    {
        public string target { get; set; }
    }

    public class Day5l14n
    {
        public NyxPage97 nyxpage { get; set; }
    }

    public class NyxPage97
    {
        public Media97 media { get; set; }
        public string text { get; set; }
        public Action94 action { get; set; }
    }

    public class Media97
    {
        public string nyximage { get; set; }
    }

    public class Action94
    {
        public NyxVert82 nyxvert { get; set; }
    }

    public class NyxVert82
    {
        public Element82[] elements { get; set; }
    }

    public class Element82
    {
        public NyxButtons82[] nyxbuttons { get; set; }
    }

    public class NyxButtons82
    {
        public string label { get; set; }
        public Command108[] commands { get; set; }
    }

    public class Command108
    {
        public Goto108 _goto { get; set; }
    }

    public class Goto108
    {
        public string target { get; set; }
    }

    public class Day5l14y
    {
        public NyxPage98 nyxpage { get; set; }
    }

    public class NyxPage98
    {
        public Media98 media { get; set; }
        public string text { get; set; }
        public Action95 action { get; set; }
    }

    public class Media98
    {
        public string nyximage { get; set; }
    }

    public class Action95
    {
        public NyxVert83 nyxvert { get; set; }
    }

    public class NyxVert83
    {
        public Element83[] elements { get; set; }
    }

    public class Element83
    {
        public NyxButtons83[] nyxbuttons { get; set; }
    }

    public class NyxButtons83
    {
        public string label { get; set; }
        public Command109[] commands { get; set; }
    }

    public class Command109
    {
        public Goto109 _goto { get; set; }
    }

    public class Goto109
    {
        public string target { get; set; }
    }

    public class Day5l15
    {
        public NyxPage99 nyxpage { get; set; }
    }

    public class NyxPage99
    {
        public Media99 media { get; set; }
        public string text { get; set; }
        public Action96 action { get; set; }
    }

    public class Media99
    {
        public string nyximage { get; set; }
    }

    public class Action96
    {
        public NyxVert84 nyxvert { get; set; }
    }

    public class NyxVert84
    {
        public Element84[] elements { get; set; }
    }

    public class Element84
    {
        public NyxButtons84[] nyxbuttons { get; set; }
    }

    public class NyxButtons84
    {
        public string label { get; set; }
        public Command110[] commands { get; set; }
    }

    public class Command110
    {
        public Goto110 _goto { get; set; }
    }

    public class Goto110
    {
        public string target { get; set; }
    }

    public class Day5l16
    {
        public NyxPage100 nyxpage { get; set; }
    }

    public class NyxPage100
    {
        public Media100 media { get; set; }
        public string text { get; set; }
        public Action97 action { get; set; }
    }

    public class Media100
    {
        public string nyximage { get; set; }
    }

    public class Action97
    {
        public NyxVert85 nyxvert { get; set; }
    }

    public class NyxVert85
    {
        public Element85[] elements { get; set; }
    }

    public class Element85
    {
        public NyxButtons85[] nyxbuttons { get; set; }
    }

    public class NyxButtons85
    {
        public string label { get; set; }
        public Command111[] commands { get; set; }
    }

    public class Command111
    {
        public Goto111 _goto { get; set; }
    }

    public class Goto111
    {
        public string target { get; set; }
    }

    public class Day5l17
    {
        public NyxPage101 nyxpage { get; set; }
    }

    public class NyxPage101
    {
        public Media101 media { get; set; }
        public string text { get; set; }
        public Action98 action { get; set; }
    }

    public class Media101
    {
        public string nyximage { get; set; }
    }

    public class Action98
    {
        public NyxVert86 nyxvert { get; set; }
    }

    public class NyxVert86
    {
        public Element86[] elements { get; set; }
    }

    public class Element86
    {
        public NyxButtons86[] nyxbuttons { get; set; }
    }

    public class NyxButtons86
    {
        public string label { get; set; }
        public Command112[] commands { get; set; }
    }

    public class Command112
    {
        public Goto112 _goto { get; set; }
    }

    public class Goto112
    {
        public string target { get; set; }
    }

    public class Day5l18
    {
        public NyxPage102 nyxpage { get; set; }
    }

    public class NyxPage102
    {
        public Media102 media { get; set; }
        public string text { get; set; }
        public Action99 action { get; set; }
    }

    public class Media102
    {
        public string nyximage { get; set; }
    }

    public class Action99
    {
        public NyxVert87 nyxvert { get; set; }
    }

    public class NyxVert87
    {
        public Element87[] elements { get; set; }
    }

    public class Element87
    {
        public NyxButtons87[] nyxbuttons { get; set; }
    }

    public class NyxButtons87
    {
        public string label { get; set; }
        public Command113[] commands { get; set; }
    }

    public class Command113
    {
        public Goto113 _goto { get; set; }
    }

    public class Goto113
    {
        public string target { get; set; }
    }

    public class Day5l19
    {
        public NyxPage103 nyxpage { get; set; }
    }

    public class NyxPage103
    {
        public Media103 media { get; set; }
        public string text { get; set; }
        public Action100 action { get; set; }
    }

    public class Media103
    {
        public string nyximage { get; set; }
    }

    public class Action100
    {
        public NyxVert88 nyxvert { get; set; }
    }

    public class NyxVert88
    {
        public Element88[] elements { get; set; }
    }

    public class Element88
    {
        public NyxButtons88[] nyxbuttons { get; set; }
    }

    public class NyxButtons88
    {
        public string label { get; set; }
        public Command114[] commands { get; set; }
    }

    public class Command114
    {
        public Goto114 _goto { get; set; }
    }

    public class Goto114
    {
        public string target { get; set; }
    }

    public class Day5l2
    {
        public NyxPage104 nyxpage { get; set; }
    }

    public class NyxPage104
    {
        public Media104 media { get; set; }
        public string text { get; set; }
        public Action101 action { get; set; }
    }

    public class Media104
    {
        public string nyximage { get; set; }
    }

    public class Action101
    {
        public NyxVert89 nyxvert { get; set; }
    }

    public class NyxVert89
    {
        public Element89[] elements { get; set; }
    }

    public class Element89
    {
        public NyxButtons89[] nyxbuttons { get; set; }
    }

    public class NyxButtons89
    {
        public string label { get; set; }
        public Command115[] commands { get; set; }
    }

    public class Command115
    {
        public Goto115 _goto { get; set; }
    }

    public class Goto115
    {
        public string target { get; set; }
    }

    public class Day5l21
    {
        public NyxPage105 nyxpage { get; set; }
    }

    public class NyxPage105
    {
        public Media105 media { get; set; }
        public string text { get; set; }
        public Action102 action { get; set; }
    }

    public class Media105
    {
        public string nyximage { get; set; }
    }

    public class Action102
    {
        public NyxVert90 nyxvert { get; set; }
    }

    public class NyxVert90
    {
        public Element90[] elements { get; set; }
    }

    public class Element90
    {
        public NyxButtons90[] nyxbuttons { get; set; }
    }

    public class NyxButtons90
    {
        public string label { get; set; }
        public Command116[] commands { get; set; }
    }

    public class Command116
    {
        public Goto116 _goto { get; set; }
    }

    public class Goto116
    {
        public string target { get; set; }
    }

    public class Day5l22
    {
        public NyxPage106 nyxpage { get; set; }
    }

    public class NyxPage106
    {
        public Media106 media { get; set; }
        public string text { get; set; }
        public Action103 action { get; set; }
    }

    public class Media106
    {
        public string nyximage { get; set; }
    }

    public class Action103
    {
        public NyxTimer26 nyxtimer { get; set; }
    }

    public class NyxTimer26
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command117[] commands { get; set; }
    }

    public class Command117
    {
        public Goto117 _goto { get; set; }
    }

    public class Goto117
    {
        public string target { get; set; }
    }

    public class Day5l23
    {
        public NyxPage107 nyxpage { get; set; }
    }

    public class NyxPage107
    {
        public Media107 media { get; set; }
        public string text { get; set; }
        public Action104 action { get; set; }
    }

    public class Media107
    {
        public string nyximage { get; set; }
    }

    public class Action104
    {
        public NyxVert91 nyxvert { get; set; }
    }

    public class NyxVert91
    {
        public Element91[] elements { get; set; }
    }

    public class Element91
    {
        public NyxButtons91[] nyxbuttons { get; set; }
    }

    public class NyxButtons91
    {
        public string label { get; set; }
        public Command118[] commands { get; set; }
    }

    public class Command118
    {
        public Goto118 _goto { get; set; }
    }

    public class Goto118
    {
        public string target { get; set; }
    }

    public class Day5l24
    {
        public NyxPage108 nyxpage { get; set; }
    }

    public class NyxPage108
    {
        public Media108 media { get; set; }
        public string text { get; set; }
        public Action105 action { get; set; }
    }

    public class Media108
    {
        public string nyximage { get; set; }
    }

    public class Action105
    {
        public NyxTimer27 nyxtimer { get; set; }
    }

    public class NyxTimer27
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command119[] commands { get; set; }
    }

    public class Command119
    {
        public Goto119 _goto { get; set; }
    }

    public class Goto119
    {
        public string target { get; set; }
    }

    public class Day5l25
    {
        public NyxPage109 nyxpage { get; set; }
    }

    public class NyxPage109
    {
        public Media109 media { get; set; }
        public string text { get; set; }
        public Action106 action { get; set; }
    }

    public class Media109
    {
        public string nyximage { get; set; }
    }

    public class Action106
    {
        public NyxVert92 nyxvert { get; set; }
    }

    public class NyxVert92
    {
        public Element92[] elements { get; set; }
    }

    public class Element92
    {
        public NyxButtons92[] nyxbuttons { get; set; }
    }

    public class NyxButtons92
    {
        public string label { get; set; }
        public Command120[] commands { get; set; }
    }

    public class Command120
    {
        public Goto120 _goto { get; set; }
    }

    public class Goto120
    {
        public string target { get; set; }
    }

    public class Day5l26
    {
        public NyxPage110 nyxpage { get; set; }
    }

    public class NyxPage110
    {
        public Media110 media { get; set; }
        public string text { get; set; }
        public Action107 action { get; set; }
    }

    public class Media110
    {
        public string nyximage { get; set; }
    }

    public class Action107
    {
        public NyxTimer28 nyxtimer { get; set; }
    }

    public class NyxTimer28
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command121[] commands { get; set; }
    }

    public class Command121
    {
        public Goto121 _goto { get; set; }
    }

    public class Goto121
    {
        public string target { get; set; }
    }

    public class Day5l27
    {
        public NyxPage111 nyxpage { get; set; }
    }

    public class NyxPage111
    {
        public Media111 media { get; set; }
        public string text { get; set; }
        public Action108 action { get; set; }
    }

    public class Media111
    {
        public string nyximage { get; set; }
    }

    public class Action108
    {
        public NyxVert93 nyxvert { get; set; }
    }

    public class NyxVert93
    {
        public Element93[] elements { get; set; }
    }

    public class Element93
    {
        public NyxButtons93[] nyxbuttons { get; set; }
    }

    public class NyxButtons93
    {
        public string label { get; set; }
        public Command122[] commands { get; set; }
    }

    public class Command122
    {
        public Goto122 _goto { get; set; }
    }

    public class Goto122
    {
        public string target { get; set; }
    }

    public class Day5l28
    {
        public NyxPage112 nyxpage { get; set; }
    }

    public class NyxPage112
    {
        public Media112 media { get; set; }
        public string text { get; set; }
        public Action109 action { get; set; }
    }

    public class Media112
    {
        public string nyximage { get; set; }
    }

    public class Action109
    {
        public NyxTimer29 nyxtimer { get; set; }
    }

    public class NyxTimer29
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command123[] commands { get; set; }
    }

    public class Command123
    {
        public Goto123 _goto { get; set; }
    }

    public class Goto123
    {
        public string target { get; set; }
    }

    public class Day5l29
    {
        public NyxPage113 nyxpage { get; set; }
    }

    public class NyxPage113
    {
        public Media113 media { get; set; }
        public string text { get; set; }
        public Action110 action { get; set; }
        public Hidden8 hidden { get; set; }
    }

    public class Media113
    {
        public string nyximage { get; set; }
    }

    public class Action110
    {
        public NyxTimer30 nyxtimer { get; set; }
    }

    public class NyxTimer30
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command124[] commands { get; set; }
    }

    public class Command124
    {
        public Goto124 _goto { get; set; }
    }

    public class Goto124
    {
        public string target { get; set; }
    }

    public class Hidden8
    {
        public AudioPlay audioplay { get; set; }
    }

    public class AudioPlay
    {
        public string locator { get; set; }
    }

    public class Day5l3
    {
        public NyxPage114 nyxpage { get; set; }
    }

    public class NyxPage114
    {
        public Media114 media { get; set; }
        public string text { get; set; }
        public Action111 action { get; set; }
    }

    public class Media114
    {
        public string nyximage { get; set; }
    }

    public class Action111
    {
        public NyxVert94 nyxvert { get; set; }
    }

    public class NyxVert94
    {
        public Element94[] elements { get; set; }
    }

    public class Element94
    {
        public NyxButtons94[] nyxbuttons { get; set; }
    }

    public class NyxButtons94
    {
        public string label { get; set; }
        public Command125[] commands { get; set; }
    }

    public class Command125
    {
        public Goto125 _goto { get; set; }
    }

    public class Goto125
    {
        public string target { get; set; }
    }

    public class Day5l30
    {
        public NyxPage115 nyxpage { get; set; }
    }

    public class NyxPage115
    {
        public Media115 media { get; set; }
        public string text { get; set; }
        public Action112 action { get; set; }
    }

    public class Media115
    {
        public string nyximage { get; set; }
    }

    public class Action112
    {
        public NyxVert95 nyxvert { get; set; }
    }

    public class NyxVert95
    {
        public Element95[] elements { get; set; }
    }

    public class Element95
    {
        public NyxButtons95[] nyxbuttons { get; set; }
    }

    public class NyxButtons95
    {
        public string label { get; set; }
        public Command126[] commands { get; set; }
    }

    public class Command126
    {
        public Goto126 _goto { get; set; }
    }

    public class Goto126
    {
        public string target { get; set; }
    }

    public class Day5l31
    {
        public NyxPage116 nyxpage { get; set; }
    }

    public class NyxPage116
    {
        public Media116 media { get; set; }
        public string text { get; set; }
        public Action113 action { get; set; }
    }

    public class Media116
    {
        public string nyximage { get; set; }
    }

    public class Action113
    {
        public NyxVert96 nyxvert { get; set; }
    }

    public class NyxVert96
    {
        public Element96[] elements { get; set; }
    }

    public class Element96
    {
        public NyxButtons96[] nyxbuttons { get; set; }
    }

    public class NyxButtons96
    {
        public string label { get; set; }
        public Command127[] commands { get; set; }
    }

    public class Command127
    {
        public Goto127 _goto { get; set; }
    }

    public class Goto127
    {
        public string target { get; set; }
    }

    public class Day5l32
    {
        public NyxPage117 nyxpage { get; set; }
    }

    public class NyxPage117
    {
        public Media117 media { get; set; }
        public string text { get; set; }
    }

    public class Media117
    {
        public string nyximage { get; set; }
    }

    public class Day5l4
    {
        public NyxPage118 nyxpage { get; set; }
    }

    public class NyxPage118
    {
        public Media118 media { get; set; }
        public string text { get; set; }
        public Action114 action { get; set; }
    }

    public class Media118
    {
        public string nyximage { get; set; }
    }

    public class Action114
    {
        public NyxVert97 nyxvert { get; set; }
    }

    public class NyxVert97
    {
        public Element97[] elements { get; set; }
    }

    public class Element97
    {
        public NyxButtons97[] nyxbuttons { get; set; }
    }

    public class NyxButtons97
    {
        public string label { get; set; }
        public Command128[] commands { get; set; }
    }

    public class Command128
    {
        public Goto128 _goto { get; set; }
    }

    public class Goto128
    {
        public string target { get; set; }
    }

    public class Day5l5
    {
        public NyxPage119 nyxpage { get; set; }
    }

    public class NyxPage119
    {
        public Media119 media { get; set; }
        public string text { get; set; }
        public Action115 action { get; set; }
    }

    public class Media119
    {
        public string nyximage { get; set; }
    }

    public class Action115
    {
        public NyxVert98 nyxvert { get; set; }
    }

    public class NyxVert98
    {
        public Element98[] elements { get; set; }
    }

    public class Element98
    {
        public NyxButtons98[] nyxbuttons { get; set; }
    }

    public class NyxButtons98
    {
        public string label { get; set; }
        public Command129[] commands { get; set; }
    }

    public class Command129
    {
        public Goto129 _goto { get; set; }
    }

    public class Goto129
    {
        public string target { get; set; }
    }

    public class Day5l6
    {
        public NyxPage120 nyxpage { get; set; }
    }

    public class NyxPage120
    {
        public Media120 media { get; set; }
        public string text { get; set; }
        public Action116 action { get; set; }
    }

    public class Media120
    {
        public string nyximage { get; set; }
    }

    public class Action116
    {
        public NyxVert99 nyxvert { get; set; }
    }

    public class NyxVert99
    {
        public Element99[] elements { get; set; }
    }

    public class Element99
    {
        public NyxButtons99[] nyxbuttons { get; set; }
    }

    public class NyxButtons99
    {
        public string label { get; set; }
        public Command130[] commands { get; set; }
    }

    public class Command130
    {
        public Goto130 _goto { get; set; }
    }

    public class Goto130
    {
        public string target { get; set; }
    }

    public class Day5l7n
    {
        public NyxPage121 nyxpage { get; set; }
    }

    public class NyxPage121
    {
        public Media121 media { get; set; }
        public string text { get; set; }
        public Action117 action { get; set; }
    }

    public class Media121
    {
        public string nyximage { get; set; }
    }

    public class Action117
    {
        public NyxVert100 nyxvert { get; set; }
    }

    public class NyxVert100
    {
        public Element100[] elements { get; set; }
    }

    public class Element100
    {
        public NyxButtons100[] nyxbuttons { get; set; }
    }

    public class NyxButtons100
    {
        public string label { get; set; }
        public Command131[] commands { get; set; }
    }

    public class Command131
    {
        public Goto131 _goto { get; set; }
    }

    public class Goto131
    {
        public string target { get; set; }
    }

    public class Day5l7y
    {
        public NyxPage122 nyxpage { get; set; }
    }

    public class NyxPage122
    {
        public Media122 media { get; set; }
        public string text { get; set; }
        public Action118 action { get; set; }
    }

    public class Media122
    {
        public string nyximage { get; set; }
    }

    public class Action118
    {
        public NyxVert101 nyxvert { get; set; }
    }

    public class NyxVert101
    {
        public Element101[] elements { get; set; }
    }

    public class Element101
    {
        public NyxButtons101[] nyxbuttons { get; set; }
    }

    public class NyxButtons101
    {
        public string label { get; set; }
        public Command132[] commands { get; set; }
    }

    public class Command132
    {
        public Goto132 _goto { get; set; }
    }

    public class Goto132
    {
        public string target { get; set; }
    }

    public class Day5l8
    {
        public NyxPage123 nyxpage { get; set; }
    }

    public class NyxPage123
    {
        public Media123 media { get; set; }
        public string text { get; set; }
        public Action119 action { get; set; }
    }

    public class Media123
    {
        public string nyximage { get; set; }
    }

    public class Action119
    {
        public NyxVert102 nyxvert { get; set; }
    }

    public class NyxVert102
    {
        public Element102[] elements { get; set; }
    }

    public class Element102
    {
        public NyxButtons102[] nyxbuttons { get; set; }
    }

    public class NyxButtons102
    {
        public string label { get; set; }
        public Command133[] commands { get; set; }
    }

    public class Command133
    {
        public Goto133 _goto { get; set; }
    }

    public class Goto133
    {
        public string target { get; set; }
    }

    public class Day5l9
    {
        public NyxPage124 nyxpage { get; set; }
    }

    public class NyxPage124
    {
        public Media124 media { get; set; }
        public string text { get; set; }
        public Action120 action { get; set; }
    }

    public class Media124
    {
        public string nyximage { get; set; }
    }

    public class Action120
    {
        public NyxVert103 nyxvert { get; set; }
    }

    public class NyxVert103
    {
        public Element103[] elements { get; set; }
    }

    public class Element103
    {
        public NyxButtons103[] nyxbuttons { get; set; }
    }

    public class NyxButtons103
    {
        public string label { get; set; }
        public Command134[] commands { get; set; }
    }

    public class Command134
    {
        public Goto134 _goto { get; set; }
    }

    public class Goto134
    {
        public string target { get; set; }
    }

    public class T10slut
    {
        public NyxPage125 nyxpage { get; set; }
    }

    public class NyxPage125
    {
        public Media125 media { get; set; }
        public string text { get; set; }
        public Action121 action { get; set; }
    }

    public class Media125
    {
        public string nyximage { get; set; }
    }

    public class Action121
    {
        public NyxVert104 nyxvert { get; set; }
    }

    public class NyxVert104
    {
        public Element104[] elements { get; set; }
    }

    public class Element104
    {
        public NyxButtons104[] nyxbuttons { get; set; }
    }

    public class NyxButtons104
    {
        public string label { get; set; }
        public Command135[] commands { get; set; }
    }

    public class Command135
    {
        public Goto135 _goto { get; set; }
    }

    public class Goto135
    {
        public string target { get; set; }
    }

    public class T10toy
    {
        public NyxPage126 nyxpage { get; set; }
    }

    public class NyxPage126
    {
        public Media126 media { get; set; }
        public string text { get; set; }
        public Action122 action { get; set; }
    }

    public class Media126
    {
        public string nyximage { get; set; }
    }

    public class Action122
    {
        public NyxVert105 nyxvert { get; set; }
    }

    public class NyxVert105
    {
        public Element105[] elements { get; set; }
    }

    public class Element105
    {
        public NyxButtons105[] nyxbuttons { get; set; }
    }

    public class NyxButtons105
    {
        public string label { get; set; }
        public Command136[] commands { get; set; }
    }

    public class Command136
    {
        public Goto136 _goto { get; set; }
    }

    public class Goto136
    {
        public string target { get; set; }
    }

    public class T1S01
    {
        public NyxPage127 nyxpage { get; set; }
    }

    public class NyxPage127
    {
        public Media127 media { get; set; }
        public string text { get; set; }
        public Action123 action { get; set; }
    }

    public class Media127
    {
        public string nyximage { get; set; }
    }

    public class Action123
    {
        public NyxTimer31 nyxtimer { get; set; }
    }

    public class NyxTimer31
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command137[] commands { get; set; }
    }

    public class Command137
    {
        public Goto137 _goto { get; set; }
    }

    public class Goto137
    {
        public string target { get; set; }
    }

    public class T1S01D
    {
        public NyxPage128 nyxpage { get; set; }
    }

    public class NyxPage128
    {
        public Media128 media { get; set; }
        public string text { get; set; }
        public Action124 action { get; set; }
        public Instruc22 instruc { get; set; }
    }

    public class Media128
    {
        public string nyximage { get; set; }
    }

    public class Action124
    {
        public NyxVert106 nyxvert { get; set; }
    }

    public class NyxVert106
    {
        public Element106[] elements { get; set; }
    }

    public class Element106
    {
        public NyxButtons106[] nyxbuttons { get; set; }
    }

    public class NyxButtons106
    {
        public string label { get; set; }
        public Command138[] commands { get; set; }
    }

    public class Command138
    {
        public Goto138 _goto { get; set; }
    }

    public class Goto138
    {
        public string target { get; set; }
    }

    public class Instruc22
    {
        public NyxTimer32 nyxtimer { get; set; }
    }

    public class NyxTimer32
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command139[] commands { get; set; }
    }

    public class Command139
    {
        public Goto139 _goto { get; set; }
    }

    public class Goto139
    {
        public string target { get; set; }
    }

    public class T1S02
    {
        public NyxPage129 nyxpage { get; set; }
    }

    public class NyxPage129
    {
        public Media129 media { get; set; }
        public string text { get; set; }
        public Action125 action { get; set; }
    }

    public class Media129
    {
        public string nyximage { get; set; }
    }

    public class Action125
    {
        public NyxTimer33 nyxtimer { get; set; }
    }

    public class NyxTimer33
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command140[] commands { get; set; }
    }

    public class Command140
    {
        public Goto140 _goto { get; set; }
    }

    public class Goto140
    {
        public string target { get; set; }
    }

    public class T1S02D
    {
        public NyxPage130 nyxpage { get; set; }
    }

    public class NyxPage130
    {
        public Media130 media { get; set; }
        public string text { get; set; }
        public Action126 action { get; set; }
        public Instruc23 instruc { get; set; }
    }

    public class Media130
    {
        public string nyximage { get; set; }
    }

    public class Action126
    {
        public NyxVert107 nyxvert { get; set; }
    }

    public class NyxVert107
    {
        public Element107[] elements { get; set; }
    }

    public class Element107
    {
        public NyxButtons107[] nyxbuttons { get; set; }
    }

    public class NyxButtons107
    {
        public string label { get; set; }
        public Command141[] commands { get; set; }
    }

    public class Command141
    {
        public Goto141 _goto { get; set; }
    }

    public class Goto141
    {
        public string target { get; set; }
    }

    public class Instruc23
    {
        public NyxTimer34 nyxtimer { get; set; }
    }

    public class NyxTimer34
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command142[] commands { get; set; }
    }

    public class Command142
    {
        public Goto142 _goto { get; set; }
    }

    public class Goto142
    {
        public string target { get; set; }
    }

    public class T1S03
    {
        public NyxPage131 nyxpage { get; set; }
    }

    public class NyxPage131
    {
        public Media131 media { get; set; }
        public string text { get; set; }
        public Action127 action { get; set; }
    }

    public class Media131
    {
        public string nyximage { get; set; }
    }

    public class Action127
    {
        public NyxTimer35 nyxtimer { get; set; }
    }

    public class NyxTimer35
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command143[] commands { get; set; }
    }

    public class Command143
    {
        public Goto143 _goto { get; set; }
    }

    public class Goto143
    {
        public string target { get; set; }
    }

    public class T1S03D
    {
        public NyxPage132 nyxpage { get; set; }
    }

    public class NyxPage132
    {
        public Media132 media { get; set; }
        public string text { get; set; }
        public Action128 action { get; set; }
        public Instruc24 instruc { get; set; }
    }

    public class Media132
    {
        public string nyximage { get; set; }
    }

    public class Action128
    {
        public NyxVert108 nyxvert { get; set; }
    }

    public class NyxVert108
    {
        public Element108[] elements { get; set; }
    }

    public class Element108
    {
        public NyxButtons108[] nyxbuttons { get; set; }
    }

    public class NyxButtons108
    {
        public string label { get; set; }
        public Command144[] commands { get; set; }
    }

    public class Command144
    {
        public Goto144 _goto { get; set; }
    }

    public class Goto144
    {
        public string target { get; set; }
    }

    public class Instruc24
    {
        public NyxTimer36 nyxtimer { get; set; }
    }

    public class NyxTimer36
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command145[] commands { get; set; }
    }

    public class Command145
    {
        public Goto145 _goto { get; set; }
    }

    public class Goto145
    {
        public string target { get; set; }
    }

    public class T1S04
    {
        public NyxPage133 nyxpage { get; set; }
    }

    public class NyxPage133
    {
        public Media133 media { get; set; }
        public string text { get; set; }
        public Action129 action { get; set; }
    }

    public class Media133
    {
        public string nyximage { get; set; }
    }

    public class Action129
    {
        public NyxTimer37 nyxtimer { get; set; }
    }

    public class NyxTimer37
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command146[] commands { get; set; }
    }

    public class Command146
    {
        public Goto146 _goto { get; set; }
    }

    public class Goto146
    {
        public string target { get; set; }
    }

    public class T1S04D
    {
        public NyxPage134 nyxpage { get; set; }
    }

    public class NyxPage134
    {
        public Media134 media { get; set; }
        public string text { get; set; }
        public Action130 action { get; set; }
        public Instruc25 instruc { get; set; }
    }

    public class Media134
    {
        public string nyximage { get; set; }
    }

    public class Action130
    {
        public NyxVert109 nyxvert { get; set; }
    }

    public class NyxVert109
    {
        public Element109[] elements { get; set; }
    }

    public class Element109
    {
        public NyxButtons109[] nyxbuttons { get; set; }
    }

    public class NyxButtons109
    {
        public string label { get; set; }
        public Command147[] commands { get; set; }
    }

    public class Command147
    {
        public Goto147 _goto { get; set; }
    }

    public class Goto147
    {
        public string target { get; set; }
    }

    public class Instruc25
    {
        public NyxTimer38 nyxtimer { get; set; }
    }

    public class NyxTimer38
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command148[] commands { get; set; }
    }

    public class Command148
    {
        public Goto148 _goto { get; set; }
    }

    public class Goto148
    {
        public string target { get; set; }
    }

    public class T1S05
    {
        public NyxPage135 nyxpage { get; set; }
    }

    public class NyxPage135
    {
        public Media135 media { get; set; }
        public string text { get; set; }
        public Action131 action { get; set; }
    }

    public class Media135
    {
        public string nyximage { get; set; }
    }

    public class Action131
    {
        public NyxVert110 nyxvert { get; set; }
    }

    public class NyxVert110
    {
        public Element110[] elements { get; set; }
    }

    public class Element110
    {
        public NyxButtons110[] nyxbuttons { get; set; }
    }

    public class NyxButtons110
    {
        public string label { get; set; }
        public Command149[] commands { get; set; }
    }

    public class Command149
    {
        public Goto149 _goto { get; set; }
    }

    public class Goto149
    {
        public string target { get; set; }
    }

    public class T1S10
    {
        public NyxPage136 nyxpage { get; set; }
    }

    public class NyxPage136
    {
        public Media136 media { get; set; }
        public string text { get; set; }
        public Action132 action { get; set; }
    }

    public class Media136
    {
        public string nyximage { get; set; }
    }

    public class Action132
    {
        public NyxTimer39 nyxtimer { get; set; }
    }

    public class NyxTimer39
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command150[] commands { get; set; }
    }

    public class Command150
    {
        public Goto150 _goto { get; set; }
    }

    public class Goto150
    {
        public string target { get; set; }
    }

    public class T1S10D
    {
        public NyxPage137 nyxpage { get; set; }
    }

    public class NyxPage137
    {
        public Media137 media { get; set; }
        public string text { get; set; }
        public Action133 action { get; set; }
        public Instruc26 instruc { get; set; }
    }

    public class Media137
    {
        public string nyximage { get; set; }
    }

    public class Action133
    {
        public NyxVert111 nyxvert { get; set; }
    }

    public class NyxVert111
    {
        public Element111[] elements { get; set; }
    }

    public class Element111
    {
        public NyxButtons111[] nyxbuttons { get; set; }
    }

    public class NyxButtons111
    {
        public string label { get; set; }
        public Command151[] commands { get; set; }
    }

    public class Command151
    {
        public Goto151 _goto { get; set; }
    }

    public class Goto151
    {
        public string target { get; set; }
    }

    public class Instruc26
    {
        public NyxTimer40 nyxtimer { get; set; }
    }

    public class NyxTimer40
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command152[] commands { get; set; }
    }

    public class Command152
    {
        public Goto152 _goto { get; set; }
    }

    public class Goto152
    {
        public string target { get; set; }
    }

    public class T1S11
    {
        public NyxPage138 nyxpage { get; set; }
    }

    public class NyxPage138
    {
        public Media138 media { get; set; }
        public string text { get; set; }
        public Action134 action { get; set; }
    }

    public class Media138
    {
        public string nyximage { get; set; }
    }

    public class Action134
    {
        public NyxTimer41 nyxtimer { get; set; }
    }

    public class NyxTimer41
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command153[] commands { get; set; }
    }

    public class Command153
    {
        public Goto153 _goto { get; set; }
    }

    public class Goto153
    {
        public string target { get; set; }
    }

    public class T1S11D
    {
        public NyxPage139 nyxpage { get; set; }
    }

    public class NyxPage139
    {
        public Media139 media { get; set; }
        public string text { get; set; }
        public Action135 action { get; set; }
        public Instruc27 instruc { get; set; }
    }

    public class Media139
    {
        public string nyximage { get; set; }
    }

    public class Action135
    {
        public NyxVert112 nyxvert { get; set; }
    }

    public class NyxVert112
    {
        public Element112[] elements { get; set; }
    }

    public class Element112
    {
        public NyxButtons112[] nyxbuttons { get; set; }
    }

    public class NyxButtons112
    {
        public string label { get; set; }
        public Command154[] commands { get; set; }
    }

    public class Command154
    {
        public Goto154 _goto { get; set; }
    }

    public class Goto154
    {
        public string target { get; set; }
    }

    public class Instruc27
    {
        public NyxTimer42 nyxtimer { get; set; }
    }

    public class NyxTimer42
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command155[] commands { get; set; }
    }

    public class Command155
    {
        public Goto155 _goto { get; set; }
    }

    public class Goto155
    {
        public string target { get; set; }
    }

    public class T1S12
    {
        public NyxPage140 nyxpage { get; set; }
    }

    public class NyxPage140
    {
        public Media140 media { get; set; }
        public string text { get; set; }
        public Action136 action { get; set; }
    }

    public class Media140
    {
        public string nyximage { get; set; }
    }

    public class Action136
    {
        public NyxTimer43 nyxtimer { get; set; }
    }

    public class NyxTimer43
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command156[] commands { get; set; }
    }

    public class Command156
    {
        public Goto156 _goto { get; set; }
    }

    public class Goto156
    {
        public string target { get; set; }
    }

    public class T1S12D
    {
        public NyxPage141 nyxpage { get; set; }
    }

    public class NyxPage141
    {
        public Media141 media { get; set; }
        public string text { get; set; }
        public Action137 action { get; set; }
        public Instruc28 instruc { get; set; }
    }

    public class Media141
    {
        public string nyximage { get; set; }
    }

    public class Action137
    {
        public NyxVert113 nyxvert { get; set; }
    }

    public class NyxVert113
    {
        public Element113[] elements { get; set; }
    }

    public class Element113
    {
        public NyxButtons113[] nyxbuttons { get; set; }
    }

    public class NyxButtons113
    {
        public string label { get; set; }
        public Command157[] commands { get; set; }
    }

    public class Command157
    {
        public Goto157 _goto { get; set; }
    }

    public class Goto157
    {
        public string target { get; set; }
    }

    public class Instruc28
    {
        public NyxTimer44 nyxtimer { get; set; }
    }

    public class NyxTimer44
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command158[] commands { get; set; }
    }

    public class Command158
    {
        public Goto158 _goto { get; set; }
    }

    public class Goto158
    {
        public string target { get; set; }
    }

    public class T1S13
    {
        public NyxPage142 nyxpage { get; set; }
    }

    public class NyxPage142
    {
        public Media142 media { get; set; }
        public string text { get; set; }
        public Action138 action { get; set; }
    }

    public class Media142
    {
        public string nyximage { get; set; }
    }

    public class Action138
    {
        public NyxTimer45 nyxtimer { get; set; }
    }

    public class NyxTimer45
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command159[] commands { get; set; }
    }

    public class Command159
    {
        public Goto159 _goto { get; set; }
    }

    public class Goto159
    {
        public string target { get; set; }
    }

    public class T1S13D
    {
        public NyxPage143 nyxpage { get; set; }
    }

    public class NyxPage143
    {
        public Media143 media { get; set; }
        public string text { get; set; }
        public Action139 action { get; set; }
        public Instruc29 instruc { get; set; }
    }

    public class Media143
    {
        public string nyximage { get; set; }
    }

    public class Action139
    {
        public NyxVert114 nyxvert { get; set; }
    }

    public class NyxVert114
    {
        public Element114[] elements { get; set; }
    }

    public class Element114
    {
        public NyxButtons114[] nyxbuttons { get; set; }
    }

    public class NyxButtons114
    {
        public string label { get; set; }
        public Command160[] commands { get; set; }
    }

    public class Command160
    {
        public Goto160 _goto { get; set; }
    }

    public class Goto160
    {
        public string target { get; set; }
    }

    public class Instruc29
    {
        public NyxTimer46 nyxtimer { get; set; }
    }

    public class NyxTimer46
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command161[] commands { get; set; }
    }

    public class Command161
    {
        public Goto161 _goto { get; set; }
    }

    public class Goto161
    {
        public string target { get; set; }
    }

    public class T1S14
    {
        public NyxPage144 nyxpage { get; set; }
    }

    public class NyxPage144
    {
        public Media144 media { get; set; }
        public string text { get; set; }
        public Action140 action { get; set; }
    }

    public class Media144
    {
        public string nyximage { get; set; }
    }

    public class Action140
    {
        public NyxTimer47 nyxtimer { get; set; }
    }

    public class NyxTimer47
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command162[] commands { get; set; }
    }

    public class Command162
    {
        public Goto162 _goto { get; set; }
    }

    public class Goto162
    {
        public string target { get; set; }
    }

    public class T1S14D
    {
        public NyxPage145 nyxpage { get; set; }
    }

    public class NyxPage145
    {
        public Media145 media { get; set; }
        public string text { get; set; }
        public Action141 action { get; set; }
        public Instruc30 instruc { get; set; }
    }

    public class Media145
    {
        public string nyximage { get; set; }
    }

    public class Action141
    {
        public NyxVert115 nyxvert { get; set; }
    }

    public class NyxVert115
    {
        public Element115[] elements { get; set; }
    }

    public class Element115
    {
        public NyxButtons115[] nyxbuttons { get; set; }
    }

    public class NyxButtons115
    {
        public string label { get; set; }
        public Command163[] commands { get; set; }
    }

    public class Command163
    {
        public Goto163 _goto { get; set; }
    }

    public class Goto163
    {
        public string target { get; set; }
    }

    public class Instruc30
    {
        public NyxTimer48 nyxtimer { get; set; }
    }

    public class NyxTimer48
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command164[] commands { get; set; }
    }

    public class Command164
    {
        public Goto164 _goto { get; set; }
    }

    public class Goto164
    {
        public string target { get; set; }
    }

    public class T1S15
    {
        public NyxPage146 nyxpage { get; set; }
    }

    public class NyxPage146
    {
        public Media146 media { get; set; }
        public string text { get; set; }
        public Action142 action { get; set; }
    }

    public class Media146
    {
        public string nyximage { get; set; }
    }

    public class Action142
    {
        public NyxVert116 nyxvert { get; set; }
    }

    public class NyxVert116
    {
        public Element116[] elements { get; set; }
    }

    public class Element116
    {
        public NyxButtons116[] nyxbuttons { get; set; }
    }

    public class NyxButtons116
    {
        public string label { get; set; }
        public Command165[] commands { get; set; }
    }

    public class Command165
    {
        public Goto165 _goto { get; set; }
    }

    public class Goto165
    {
        public string target { get; set; }
    }

    public class T1S20
    {
        public NyxPage147 nyxpage { get; set; }
    }

    public class NyxPage147
    {
        public Media147 media { get; set; }
        public string text { get; set; }
        public Action143 action { get; set; }
    }

    public class Media147
    {
        public string nyximage { get; set; }
    }

    public class Action143
    {
        public NyxTimer49 nyxtimer { get; set; }
    }

    public class NyxTimer49
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command166[] commands { get; set; }
    }

    public class Command166
    {
        public Goto166 _goto { get; set; }
    }

    public class Goto166
    {
        public string target { get; set; }
    }

    public class T1S20D
    {
        public NyxPage148 nyxpage { get; set; }
    }

    public class NyxPage148
    {
        public Media148 media { get; set; }
        public string text { get; set; }
        public Action144 action { get; set; }
        public Instruc31 instruc { get; set; }
    }

    public class Media148
    {
        public string nyximage { get; set; }
    }

    public class Action144
    {
        public NyxVert117 nyxvert { get; set; }
    }

    public class NyxVert117
    {
        public Element117[] elements { get; set; }
    }

    public class Element117
    {
        public NyxButtons117[] nyxbuttons { get; set; }
    }

    public class NyxButtons117
    {
        public string label { get; set; }
        public Command167[] commands { get; set; }
    }

    public class Command167
    {
        public Goto167 _goto { get; set; }
    }

    public class Goto167
    {
        public string target { get; set; }
    }

    public class Instruc31
    {
        public NyxTimer50 nyxtimer { get; set; }
    }

    public class NyxTimer50
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command168[] commands { get; set; }
    }

    public class Command168
    {
        public Goto168 _goto { get; set; }
    }

    public class Goto168
    {
        public string target { get; set; }
    }

    public class T1S21
    {
        public NyxPage149 nyxpage { get; set; }
    }

    public class NyxPage149
    {
        public Media149 media { get; set; }
        public string text { get; set; }
        public Action145 action { get; set; }
    }

    public class Media149
    {
        public string nyximage { get; set; }
    }

    public class Action145
    {
        public NyxTimer51 nyxtimer { get; set; }
    }

    public class NyxTimer51
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command169[] commands { get; set; }
    }

    public class Command169
    {
        public Goto169 _goto { get; set; }
    }

    public class Goto169
    {
        public string target { get; set; }
    }

    public class T1S21D
    {
        public NyxPage150 nyxpage { get; set; }
    }

    public class NyxPage150
    {
        public Media150 media { get; set; }
        public string text { get; set; }
        public Action146 action { get; set; }
        public Instruc32 instruc { get; set; }
    }

    public class Media150
    {
        public string nyximage { get; set; }
    }

    public class Action146
    {
        public NyxVert118 nyxvert { get; set; }
    }

    public class NyxVert118
    {
        public Element118[] elements { get; set; }
    }

    public class Element118
    {
        public NyxButtons118[] nyxbuttons { get; set; }
    }

    public class NyxButtons118
    {
        public string label { get; set; }
        public Command170[] commands { get; set; }
    }

    public class Command170
    {
        public Goto170 _goto { get; set; }
    }

    public class Goto170
    {
        public string target { get; set; }
    }

    public class Instruc32
    {
        public NyxTimer52 nyxtimer { get; set; }
    }

    public class NyxTimer52
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command171[] commands { get; set; }
    }

    public class Command171
    {
        public Goto171 _goto { get; set; }
    }

    public class Goto171
    {
        public string target { get; set; }
    }

    public class T1S22
    {
        public NyxPage151 nyxpage { get; set; }
    }

    public class NyxPage151
    {
        public Media151 media { get; set; }
        public string text { get; set; }
        public Action147 action { get; set; }
    }

    public class Media151
    {
        public string nyximage { get; set; }
    }

    public class Action147
    {
        public NyxTimer53 nyxtimer { get; set; }
    }

    public class NyxTimer53
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command172[] commands { get; set; }
    }

    public class Command172
    {
        public Goto172 _goto { get; set; }
    }

    public class Goto172
    {
        public string target { get; set; }
    }

    public class T1S22D
    {
        public NyxPage152 nyxpage { get; set; }
    }

    public class NyxPage152
    {
        public Media152 media { get; set; }
        public string text { get; set; }
        public Action148 action { get; set; }
        public Instruc33 instruc { get; set; }
    }

    public class Media152
    {
        public string nyximage { get; set; }
    }

    public class Action148
    {
        public NyxVert119 nyxvert { get; set; }
    }

    public class NyxVert119
    {
        public Element119[] elements { get; set; }
    }

    public class Element119
    {
        public NyxButtons119[] nyxbuttons { get; set; }
    }

    public class NyxButtons119
    {
        public string label { get; set; }
        public Command173[] commands { get; set; }
    }

    public class Command173
    {
        public Goto173 _goto { get; set; }
    }

    public class Goto173
    {
        public string target { get; set; }
    }

    public class Instruc33
    {
        public NyxTimer54 nyxtimer { get; set; }
    }

    public class NyxTimer54
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command174[] commands { get; set; }
    }

    public class Command174
    {
        public Goto174 _goto { get; set; }
    }

    public class Goto174
    {
        public string target { get; set; }
    }

    public class T1S23
    {
        public NyxPage153 nyxpage { get; set; }
    }

    public class NyxPage153
    {
        public Media153 media { get; set; }
        public string text { get; set; }
        public Action149 action { get; set; }
    }

    public class Media153
    {
        public string nyximage { get; set; }
    }

    public class Action149
    {
        public NyxTimer55 nyxtimer { get; set; }
    }

    public class NyxTimer55
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command175[] commands { get; set; }
    }

    public class Command175
    {
        public Goto175 _goto { get; set; }
    }

    public class Goto175
    {
        public string target { get; set; }
    }

    public class T1S23D
    {
        public NyxPage154 nyxpage { get; set; }
    }

    public class NyxPage154
    {
        public Media154 media { get; set; }
        public string text { get; set; }
        public Action150 action { get; set; }
        public Instruc34 instruc { get; set; }
    }

    public class Media154
    {
        public string nyximage { get; set; }
    }

    public class Action150
    {
        public NyxVert120 nyxvert { get; set; }
    }

    public class NyxVert120
    {
        public Element120[] elements { get; set; }
    }

    public class Element120
    {
        public NyxButtons120[] nyxbuttons { get; set; }
    }

    public class NyxButtons120
    {
        public string label { get; set; }
        public Command176[] commands { get; set; }
    }

    public class Command176
    {
        public Goto176 _goto { get; set; }
    }

    public class Goto176
    {
        public string target { get; set; }
    }

    public class Instruc34
    {
        public NyxTimer56 nyxtimer { get; set; }
    }

    public class NyxTimer56
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command177[] commands { get; set; }
    }

    public class Command177
    {
        public Goto177 _goto { get; set; }
    }

    public class Goto177
    {
        public string target { get; set; }
    }

    public class T1S24
    {
        public NyxPage155 nyxpage { get; set; }
    }

    public class NyxPage155
    {
        public Media155 media { get; set; }
        public string text { get; set; }
        public Action151 action { get; set; }
    }

    public class Media155
    {
        public string nyximage { get; set; }
    }

    public class Action151
    {
        public NyxTimer57 nyxtimer { get; set; }
    }

    public class NyxTimer57
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command178[] commands { get; set; }
    }

    public class Command178
    {
        public Goto178 _goto { get; set; }
    }

    public class Goto178
    {
        public string target { get; set; }
    }

    public class T1S24D
    {
        public NyxPage156 nyxpage { get; set; }
    }

    public class NyxPage156
    {
        public Media156 media { get; set; }
        public string text { get; set; }
        public Action152 action { get; set; }
        public Instruc35 instruc { get; set; }
    }

    public class Media156
    {
        public string nyximage { get; set; }
    }

    public class Action152
    {
        public NyxVert121 nyxvert { get; set; }
    }

    public class NyxVert121
    {
        public Element121[] elements { get; set; }
    }

    public class Element121
    {
        public NyxButtons121[] nyxbuttons { get; set; }
    }

    public class NyxButtons121
    {
        public string label { get; set; }
        public Command179[] commands { get; set; }
    }

    public class Command179
    {
        public Goto179 _goto { get; set; }
    }

    public class Goto179
    {
        public string target { get; set; }
    }

    public class Instruc35
    {
        public NyxTimer58 nyxtimer { get; set; }
    }

    public class NyxTimer58
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command180[] commands { get; set; }
    }

    public class Command180
    {
        public Goto180 _goto { get; set; }
    }

    public class Goto180
    {
        public string target { get; set; }
    }

    public class T1S25
    {
        public NyxPage157 nyxpage { get; set; }
    }

    public class NyxPage157
    {
        public Media157 media { get; set; }
        public string text { get; set; }
        public Action153 action { get; set; }
    }

    public class Media157
    {
        public string nyximage { get; set; }
    }

    public class Action153
    {
        public NyxVert122 nyxvert { get; set; }
    }

    public class NyxVert122
    {
        public Element122[] elements { get; set; }
    }

    public class Element122
    {
        public NyxButtons122[] nyxbuttons { get; set; }
    }

    public class NyxButtons122
    {
        public string label { get; set; }
        public Command181[] commands { get; set; }
    }

    public class Command181
    {
        public Goto181 _goto { get; set; }
    }

    public class Goto181
    {
        public string target { get; set; }
    }

    public class T1S30
    {
        public NyxPage158 nyxpage { get; set; }
    }

    public class NyxPage158
    {
        public Media158 media { get; set; }
        public string text { get; set; }
        public Action154 action { get; set; }
    }

    public class Media158
    {
        public string nyximage { get; set; }
    }

    public class Action154
    {
        public NyxTimer59 nyxtimer { get; set; }
    }

    public class NyxTimer59
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command182[] commands { get; set; }
    }

    public class Command182
    {
        public Goto182 _goto { get; set; }
    }

    public class Goto182
    {
        public string target { get; set; }
    }

    public class T1S30D
    {
        public NyxPage159 nyxpage { get; set; }
    }

    public class NyxPage159
    {
        public Media159 media { get; set; }
        public string text { get; set; }
        public Action155 action { get; set; }
        public Instruc36 instruc { get; set; }
    }

    public class Media159
    {
        public string nyximage { get; set; }
    }

    public class Action155
    {
        public NyxVert123 nyxvert { get; set; }
    }

    public class NyxVert123
    {
        public Element123[] elements { get; set; }
    }

    public class Element123
    {
        public NyxButtons123[] nyxbuttons { get; set; }
    }

    public class NyxButtons123
    {
        public string label { get; set; }
        public Command183[] commands { get; set; }
    }

    public class Command183
    {
        public Goto183 _goto { get; set; }
    }

    public class Goto183
    {
        public string target { get; set; }
    }

    public class Instruc36
    {
        public NyxTimer60 nyxtimer { get; set; }
    }

    public class NyxTimer60
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command184[] commands { get; set; }
    }

    public class Command184
    {
        public Goto184 _goto { get; set; }
    }

    public class Goto184
    {
        public string target { get; set; }
    }

    public class T1S31
    {
        public NyxPage160 nyxpage { get; set; }
    }

    public class NyxPage160
    {
        public Media160 media { get; set; }
        public string text { get; set; }
        public Action156 action { get; set; }
    }

    public class Media160
    {
        public string nyximage { get; set; }
    }

    public class Action156
    {
        public NyxTimer61 nyxtimer { get; set; }
    }

    public class NyxTimer61
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command185[] commands { get; set; }
    }

    public class Command185
    {
        public Goto185 _goto { get; set; }
    }

    public class Goto185
    {
        public string target { get; set; }
    }

    public class T1S31D
    {
        public NyxPage161 nyxpage { get; set; }
    }

    public class NyxPage161
    {
        public Media161 media { get; set; }
        public string text { get; set; }
        public Action157 action { get; set; }
        public Instruc37 instruc { get; set; }
    }

    public class Media161
    {
        public string nyximage { get; set; }
    }

    public class Action157
    {
        public NyxVert124 nyxvert { get; set; }
    }

    public class NyxVert124
    {
        public Element124[] elements { get; set; }
    }

    public class Element124
    {
        public NyxButtons124[] nyxbuttons { get; set; }
    }

    public class NyxButtons124
    {
        public string label { get; set; }
        public Command186[] commands { get; set; }
    }

    public class Command186
    {
        public Goto186 _goto { get; set; }
    }

    public class Goto186
    {
        public string target { get; set; }
    }

    public class Instruc37
    {
        public NyxTimer62 nyxtimer { get; set; }
    }

    public class NyxTimer62
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command187[] commands { get; set; }
    }

    public class Command187
    {
        public Goto187 _goto { get; set; }
    }

    public class Goto187
    {
        public string target { get; set; }
    }

    public class T1S32
    {
        public NyxPage162 nyxpage { get; set; }
    }

    public class NyxPage162
    {
        public Media162 media { get; set; }
        public string text { get; set; }
        public Action158 action { get; set; }
    }

    public class Media162
    {
        public string nyximage { get; set; }
    }

    public class Action158
    {
        public NyxTimer63 nyxtimer { get; set; }
    }

    public class NyxTimer63
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command188[] commands { get; set; }
    }

    public class Command188
    {
        public Goto188 _goto { get; set; }
    }

    public class Goto188
    {
        public string target { get; set; }
    }

    public class T1S32D
    {
        public NyxPage163 nyxpage { get; set; }
    }

    public class NyxPage163
    {
        public Media163 media { get; set; }
        public string text { get; set; }
        public Action159 action { get; set; }
        public Instruc38 instruc { get; set; }
    }

    public class Media163
    {
        public string nyximage { get; set; }
    }

    public class Action159
    {
        public NyxVert125 nyxvert { get; set; }
    }

    public class NyxVert125
    {
        public Element125[] elements { get; set; }
    }

    public class Element125
    {
        public NyxButtons125[] nyxbuttons { get; set; }
    }

    public class NyxButtons125
    {
        public string label { get; set; }
        public Command189[] commands { get; set; }
    }

    public class Command189
    {
        public Goto189 _goto { get; set; }
    }

    public class Goto189
    {
        public string target { get; set; }
    }

    public class Instruc38
    {
        public NyxTimer64 nyxtimer { get; set; }
    }

    public class NyxTimer64
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command190[] commands { get; set; }
    }

    public class Command190
    {
        public Goto190 _goto { get; set; }
    }

    public class Goto190
    {
        public string target { get; set; }
    }

    public class T1S33
    {
        public NyxPage164 nyxpage { get; set; }
    }

    public class NyxPage164
    {
        public Media164 media { get; set; }
        public string text { get; set; }
        public Action160 action { get; set; }
    }

    public class Media164
    {
        public string nyximage { get; set; }
    }

    public class Action160
    {
        public NyxTimer65 nyxtimer { get; set; }
    }

    public class NyxTimer65
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command191[] commands { get; set; }
    }

    public class Command191
    {
        public Goto191 _goto { get; set; }
    }

    public class Goto191
    {
        public string target { get; set; }
    }

    public class T1S33D
    {
        public NyxPage165 nyxpage { get; set; }
    }

    public class NyxPage165
    {
        public Media165 media { get; set; }
        public string text { get; set; }
        public Action161 action { get; set; }
        public Instruc39 instruc { get; set; }
    }

    public class Media165
    {
        public string nyximage { get; set; }
    }

    public class Action161
    {
        public NyxVert126 nyxvert { get; set; }
    }

    public class NyxVert126
    {
        public Element126[] elements { get; set; }
    }

    public class Element126
    {
        public NyxButtons126[] nyxbuttons { get; set; }
    }

    public class NyxButtons126
    {
        public string label { get; set; }
        public Command192[] commands { get; set; }
    }

    public class Command192
    {
        public Goto192 _goto { get; set; }
    }

    public class Goto192
    {
        public string target { get; set; }
    }

    public class Instruc39
    {
        public NyxTimer66 nyxtimer { get; set; }
    }

    public class NyxTimer66
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command193[] commands { get; set; }
    }

    public class Command193
    {
        public Goto193 _goto { get; set; }
    }

    public class Goto193
    {
        public string target { get; set; }
    }

    public class T1S34
    {
        public NyxPage166 nyxpage { get; set; }
    }

    public class NyxPage166
    {
        public Media166 media { get; set; }
        public string text { get; set; }
        public Action162 action { get; set; }
    }

    public class Media166
    {
        public string nyximage { get; set; }
    }

    public class Action162
    {
        public NyxTimer67 nyxtimer { get; set; }
    }

    public class NyxTimer67
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command194[] commands { get; set; }
    }

    public class Command194
    {
        public Goto194 _goto { get; set; }
    }

    public class Goto194
    {
        public string target { get; set; }
    }

    public class T1S34D
    {
        public NyxPage167 nyxpage { get; set; }
    }

    public class NyxPage167
    {
        public Media167 media { get; set; }
        public string text { get; set; }
        public Action163 action { get; set; }
        public Instruc40 instruc { get; set; }
    }

    public class Media167
    {
        public string nyximage { get; set; }
    }

    public class Action163
    {
        public NyxVert127 nyxvert { get; set; }
    }

    public class NyxVert127
    {
        public Element127[] elements { get; set; }
    }

    public class Element127
    {
        public NyxButtons127[] nyxbuttons { get; set; }
    }

    public class NyxButtons127
    {
        public string label { get; set; }
        public Command195[] commands { get; set; }
    }

    public class Command195
    {
        public Goto195 _goto { get; set; }
    }

    public class Goto195
    {
        public string target { get; set; }
    }

    public class Instruc40
    {
        public NyxTimer68 nyxtimer { get; set; }
    }

    public class NyxTimer68
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command196[] commands { get; set; }
    }

    public class Command196
    {
        public Goto196 _goto { get; set; }
    }

    public class Goto196
    {
        public string target { get; set; }
    }

    public class T1S35
    {
        public NyxPage168 nyxpage { get; set; }
    }

    public class NyxPage168
    {
        public Media168 media { get; set; }
        public string text { get; set; }
        public Action164 action { get; set; }
    }

    public class Media168
    {
        public string nyximage { get; set; }
    }

    public class Action164
    {
        public NyxVert128 nyxvert { get; set; }
    }

    public class NyxVert128
    {
        public Element128[] elements { get; set; }
    }

    public class Element128
    {
        public NyxButtons128[] nyxbuttons { get; set; }
    }

    public class NyxButtons128
    {
        public string label { get; set; }
        public Command197[] commands { get; set; }
    }

    public class Command197
    {
        public Goto197 _goto { get; set; }
    }

    public class Goto197
    {
        public string target { get; set; }
    }

    public class T1S40
    {
        public NyxPage169 nyxpage { get; set; }
    }

    public class NyxPage169
    {
        public Media169 media { get; set; }
        public string text { get; set; }
        public Action165 action { get; set; }
    }

    public class Media169
    {
        public string nyximage { get; set; }
    }

    public class Action165
    {
        public NyxTimer69 nyxtimer { get; set; }
    }

    public class NyxTimer69
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command198[] commands { get; set; }
    }

    public class Command198
    {
        public Goto198 _goto { get; set; }
    }

    public class Goto198
    {
        public string target { get; set; }
    }

    public class T1S40D
    {
        public NyxPage170 nyxpage { get; set; }
    }

    public class NyxPage170
    {
        public Media170 media { get; set; }
        public string text { get; set; }
        public Action166 action { get; set; }
        public Instruc41 instruc { get; set; }
    }

    public class Media170
    {
        public string nyximage { get; set; }
    }

    public class Action166
    {
        public NyxVert129 nyxvert { get; set; }
    }

    public class NyxVert129
    {
        public Element129[] elements { get; set; }
    }

    public class Element129
    {
        public NyxButtons129[] nyxbuttons { get; set; }
    }

    public class NyxButtons129
    {
        public string label { get; set; }
        public Command199[] commands { get; set; }
    }

    public class Command199
    {
        public Goto199 _goto { get; set; }
    }

    public class Goto199
    {
        public string target { get; set; }
    }

    public class Instruc41
    {
        public NyxTimer70 nyxtimer { get; set; }
    }

    public class NyxTimer70
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command200[] commands { get; set; }
    }

    public class Command200
    {
        public Goto200 _goto { get; set; }
    }

    public class Goto200
    {
        public string target { get; set; }
    }

    public class T1S41
    {
        public NyxPage171 nyxpage { get; set; }
    }

    public class NyxPage171
    {
        public Media171 media { get; set; }
        public string text { get; set; }
        public Action167 action { get; set; }
    }

    public class Media171
    {
        public string nyximage { get; set; }
    }

    public class Action167
    {
        public NyxTimer71 nyxtimer { get; set; }
    }

    public class NyxTimer71
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command201[] commands { get; set; }
    }

    public class Command201
    {
        public Goto201 _goto { get; set; }
    }

    public class Goto201
    {
        public string target { get; set; }
    }

    public class T1S41D
    {
        public NyxPage172 nyxpage { get; set; }
    }

    public class NyxPage172
    {
        public Media172 media { get; set; }
        public string text { get; set; }
        public Action168 action { get; set; }
        public Instruc42 instruc { get; set; }
    }

    public class Media172
    {
        public string nyximage { get; set; }
    }

    public class Action168
    {
        public NyxVert130 nyxvert { get; set; }
    }

    public class NyxVert130
    {
        public Element130[] elements { get; set; }
    }

    public class Element130
    {
        public NyxButtons130[] nyxbuttons { get; set; }
    }

    public class NyxButtons130
    {
        public string label { get; set; }
        public Command202[] commands { get; set; }
    }

    public class Command202
    {
        public Goto202 _goto { get; set; }
    }

    public class Goto202
    {
        public string target { get; set; }
    }

    public class Instruc42
    {
        public NyxTimer72 nyxtimer { get; set; }
    }

    public class NyxTimer72
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command203[] commands { get; set; }
    }

    public class Command203
    {
        public Goto203 _goto { get; set; }
    }

    public class Goto203
    {
        public string target { get; set; }
    }

    public class T1S42
    {
        public NyxPage173 nyxpage { get; set; }
    }

    public class NyxPage173
    {
        public Media173 media { get; set; }
        public string text { get; set; }
        public Action169 action { get; set; }
    }

    public class Media173
    {
        public string nyximage { get; set; }
    }

    public class Action169
    {
        public NyxTimer73 nyxtimer { get; set; }
    }

    public class NyxTimer73
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command204[] commands { get; set; }
    }

    public class Command204
    {
        public Goto204 _goto { get; set; }
    }

    public class Goto204
    {
        public string target { get; set; }
    }

    public class T1S42D
    {
        public NyxPage174 nyxpage { get; set; }
    }

    public class NyxPage174
    {
        public Media174 media { get; set; }
        public string text { get; set; }
        public Action170 action { get; set; }
        public Instruc43 instruc { get; set; }
    }

    public class Media174
    {
        public string nyximage { get; set; }
    }

    public class Action170
    {
        public NyxVert131 nyxvert { get; set; }
    }

    public class NyxVert131
    {
        public Element131[] elements { get; set; }
    }

    public class Element131
    {
        public NyxButtons131[] nyxbuttons { get; set; }
    }

    public class NyxButtons131
    {
        public string label { get; set; }
        public Command205[] commands { get; set; }
    }

    public class Command205
    {
        public Goto205 _goto { get; set; }
    }

    public class Goto205
    {
        public string target { get; set; }
    }

    public class Instruc43
    {
        public NyxTimer74 nyxtimer { get; set; }
    }

    public class NyxTimer74
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command206[] commands { get; set; }
    }

    public class Command206
    {
        public Goto206 _goto { get; set; }
    }

    public class Goto206
    {
        public string target { get; set; }
    }

    public class T1S43
    {
        public NyxPage175 nyxpage { get; set; }
    }

    public class NyxPage175
    {
        public Media175 media { get; set; }
        public string text { get; set; }
        public Action171 action { get; set; }
    }

    public class Media175
    {
        public string nyximage { get; set; }
    }

    public class Action171
    {
        public NyxTimer75 nyxtimer { get; set; }
    }

    public class NyxTimer75
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command207[] commands { get; set; }
    }

    public class Command207
    {
        public Goto207 _goto { get; set; }
    }

    public class Goto207
    {
        public string target { get; set; }
    }

    public class T1S43D
    {
        public NyxPage176 nyxpage { get; set; }
    }

    public class NyxPage176
    {
        public Media176 media { get; set; }
        public string text { get; set; }
        public Action172 action { get; set; }
        public Instruc44 instruc { get; set; }
    }

    public class Media176
    {
        public string nyximage { get; set; }
    }

    public class Action172
    {
        public NyxVert132 nyxvert { get; set; }
    }

    public class NyxVert132
    {
        public Element132[] elements { get; set; }
    }

    public class Element132
    {
        public NyxButtons132[] nyxbuttons { get; set; }
    }

    public class NyxButtons132
    {
        public string label { get; set; }
        public Command208[] commands { get; set; }
    }

    public class Command208
    {
        public Goto208 _goto { get; set; }
    }

    public class Goto208
    {
        public string target { get; set; }
    }

    public class Instruc44
    {
        public NyxTimer76 nyxtimer { get; set; }
    }

    public class NyxTimer76
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command209[] commands { get; set; }
    }

    public class Command209
    {
        public Goto209 _goto { get; set; }
    }

    public class Goto209
    {
        public string target { get; set; }
    }

    public class T1S44
    {
        public NyxPage177 nyxpage { get; set; }
    }

    public class NyxPage177
    {
        public Media177 media { get; set; }
        public string text { get; set; }
        public Action173 action { get; set; }
    }

    public class Media177
    {
        public string nyximage { get; set; }
    }

    public class Action173
    {
        public NyxTimer77 nyxtimer { get; set; }
    }

    public class NyxTimer77
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command210[] commands { get; set; }
    }

    public class Command210
    {
        public Goto210 _goto { get; set; }
    }

    public class Goto210
    {
        public string target { get; set; }
    }

    public class T1S44D
    {
        public NyxPage178 nyxpage { get; set; }
    }

    public class NyxPage178
    {
        public Media178 media { get; set; }
        public string text { get; set; }
        public Action174 action { get; set; }
        public Instruc45 instruc { get; set; }
    }

    public class Media178
    {
        public string nyximage { get; set; }
    }

    public class Action174
    {
        public NyxVert133 nyxvert { get; set; }
    }

    public class NyxVert133
    {
        public Element133[] elements { get; set; }
    }

    public class Element133
    {
        public NyxButtons133[] nyxbuttons { get; set; }
    }

    public class NyxButtons133
    {
        public string label { get; set; }
        public Command211[] commands { get; set; }
    }

    public class Command211
    {
        public Goto211 _goto { get; set; }
    }

    public class Goto211
    {
        public string target { get; set; }
    }

    public class Instruc45
    {
        public NyxTimer78 nyxtimer { get; set; }
    }

    public class NyxTimer78
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command212[] commands { get; set; }
    }

    public class Command212
    {
        public Goto212 _goto { get; set; }
    }

    public class Goto212
    {
        public string target { get; set; }
    }

    public class T1S45
    {
        public NyxPage179 nyxpage { get; set; }
    }

    public class NyxPage179
    {
        public Media179 media { get; set; }
        public string text { get; set; }
        public Action175 action { get; set; }
    }

    public class Media179
    {
        public string nyximage { get; set; }
    }

    public class Action175
    {
        public NyxVert134 nyxvert { get; set; }
    }

    public class NyxVert134
    {
        public Element134[] elements { get; set; }
    }

    public class Element134
    {
        public NyxButtons134[] nyxbuttons { get; set; }
    }

    public class NyxButtons134
    {
        public string label { get; set; }
        public Command213[] commands { get; set; }
    }

    public class Command213
    {
        public Goto213 _goto { get; set; }
    }

    public class Goto213
    {
        public string target { get; set; }
    }

    public class T1S50
    {
        public NyxPage180 nyxpage { get; set; }
    }

    public class NyxPage180
    {
        public Media180 media { get; set; }
        public string text { get; set; }
        public Action176 action { get; set; }
    }

    public class Media180
    {
        public string nyximage { get; set; }
    }

    public class Action176
    {
        public NyxVert135 nyxvert { get; set; }
    }

    public class NyxVert135
    {
        public Element135[] elements { get; set; }
    }

    public class Element135
    {
        public NyxButtons135[] nyxbuttons { get; set; }
    }

    public class NyxButtons135
    {
        public string label { get; set; }
        public Command214[] commands { get; set; }
    }

    public class Command214
    {
        public Goto214 _goto { get; set; }
    }

    public class Goto214
    {
        public string target { get; set; }
    }

    public class T1S51
    {
        public NyxPage181 nyxpage { get; set; }
    }

    public class NyxPage181
    {
        public Media181 media { get; set; }
        public string text { get; set; }
        public Action177 action { get; set; }
    }

    public class Media181
    {
        public string nyximage { get; set; }
    }

    public class Action177
    {
        public NyxVert136 nyxvert { get; set; }
    }

    public class NyxVert136
    {
        public Element136[] elements { get; set; }
    }

    public class Element136
    {
        public NyxButtons136[] nyxbuttons { get; set; }
    }

    public class NyxButtons136
    {
        public string label { get; set; }
        public Command215[] commands { get; set; }
    }

    public class Command215
    {
        public Goto215 _goto { get; set; }
    }

    public class Goto215
    {
        public string target { get; set; }
    }

    public class T1S52
    {
        public NyxPage182 nyxpage { get; set; }
    }

    public class NyxPage182
    {
        public Media182 media { get; set; }
        public string text { get; set; }
        public Action178 action { get; set; }
    }

    public class Media182
    {
        public string nyximage { get; set; }
    }

    public class Action178
    {
        public NyxVert137 nyxvert { get; set; }
    }

    public class NyxVert137
    {
        public Element137[] elements { get; set; }
    }

    public class Element137
    {
        public NyxButtons137[] nyxbuttons { get; set; }
    }

    public class NyxButtons137
    {
        public string label { get; set; }
        public Command216[] commands { get; set; }
    }

    public class Command216
    {
        public Goto216 _goto { get; set; }
    }

    public class Goto216
    {
        public string target { get; set; }
    }

    public class T1S53
    {
        public NyxPage183 nyxpage { get; set; }
    }

    public class NyxPage183
    {
        public Media183 media { get; set; }
        public string text { get; set; }
        public Action179 action { get; set; }
    }

    public class Media183
    {
        public string nyximage { get; set; }
    }

    public class Action179
    {
        public NyxVert138 nyxvert { get; set; }
    }

    public class NyxVert138
    {
        public Element138[] elements { get; set; }
    }

    public class Element138
    {
        public NyxButtons138[] nyxbuttons { get; set; }
    }

    public class NyxButtons138
    {
        public string label { get; set; }
        public Command217[] commands { get; set; }
    }

    public class Command217
    {
        public Goto217 _goto { get; set; }
    }

    public class Goto217
    {
        public string target { get; set; }
    }

    public class T1S54
    {
        public NyxPage184 nyxpage { get; set; }
    }

    public class NyxPage184
    {
        public Media184 media { get; set; }
        public string text { get; set; }
        public Action180 action { get; set; }
    }

    public class Media184
    {
        public string nyximage { get; set; }
    }

    public class Action180
    {
        public NyxVert139 nyxvert { get; set; }
    }

    public class NyxVert139
    {
        public Element139[] elements { get; set; }
    }

    public class Element139
    {
        public NyxButtons139[] nyxbuttons { get; set; }
    }

    public class NyxButtons139
    {
        public string label { get; set; }
        public Command218[] commands { get; set; }
    }

    public class Command218
    {
        public Goto218 _goto { get; set; }
    }

    public class Goto218
    {
        public string target { get; set; }
    }

    public class T2slut
    {
        public NyxPage185 nyxpage { get; set; }
    }

    public class NyxPage185
    {
        public Media185 media { get; set; }
        public string text { get; set; }
        public Action181 action { get; set; }
    }

    public class Media185
    {
        public string nyximage { get; set; }
    }

    public class Action181
    {
        public NyxTimer79 nyxtimer { get; set; }
    }

    public class NyxTimer79
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command219[] commands { get; set; }
    }

    public class Command219
    {
        public Pcm2Range pcm2range { get; set; }
    }

    public class Pcm2Range
    {
        public int to { get; set; }
        public int from { get; set; }
        public string prefix { get; set; }
    }

    public class T2SS
    {
        public NyxPage186 nyxpage { get; set; }
    }

    public class NyxPage186
    {
        public Media186 media { get; set; }
        public string text { get; set; }
        public Action182 action { get; set; }
    }

    public class Media186
    {
        public string nyximage { get; set; }
    }

    public class Action182
    {
        public NyxVert140 nyxvert { get; set; }
    }

    public class NyxVert140
    {
        public Element140[] elements { get; set; }
    }

    public class Element140
    {
        public NyxButtons140[] nyxbuttons { get; set; }
    }

    public class NyxButtons140
    {
        public string label { get; set; }
        public Command220[] commands { get; set; }
    }

    public class Command220
    {
        public Goto219 _goto { get; set; }
    }

    public class Goto219
    {
        public string target { get; set; }
    }

    public class T2SS1
    {
        public NyxPage187 nyxpage { get; set; }
    }

    public class NyxPage187
    {
        public Media187 media { get; set; }
        public string text { get; set; }
        public Action183 action { get; set; }
        public Instruc46 instruc { get; set; }
    }

    public class Media187
    {
        public string nyximage { get; set; }
    }

    public class Action183
    {
        public NyxVert141 nyxvert { get; set; }
    }

    public class NyxVert141
    {
        public Element141[] elements { get; set; }
    }

    public class Element141
    {
        public NyxButtons141[] nyxbuttons { get; set; }
    }

    public class NyxButtons141
    {
        public string label { get; set; }
        public Command221[] commands { get; set; }
    }

    public class Command221
    {
        public Goto220 _goto { get; set; }
    }

    public class Goto220
    {
        public string target { get; set; }
    }

    public class Instruc46
    {
        public NyxTimer80 nyxtimer { get; set; }
    }

    public class NyxTimer80
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command222[] commands { get; set; }
    }

    public class Command222
    {
        public Goto221 _goto { get; set; }
    }

    public class Goto221
    {
        public string target { get; set; }
    }

    public class T2SS2
    {
        public NyxPage188 nyxpage { get; set; }
    }

    public class NyxPage188
    {
        public Media188 media { get; set; }
        public string text { get; set; }
        public Action184 action { get; set; }
        public Instruc47 instruc { get; set; }
    }

    public class Media188
    {
        public string nyximage { get; set; }
    }

    public class Action184
    {
        public NyxVert142 nyxvert { get; set; }
    }

    public class NyxVert142
    {
        public Element142[] elements { get; set; }
    }

    public class Element142
    {
        public NyxButtons142[] nyxbuttons { get; set; }
    }

    public class NyxButtons142
    {
        public string label { get; set; }
        public Command223[] commands { get; set; }
    }

    public class Command223
    {
        public Goto222 _goto { get; set; }
    }

    public class Goto222
    {
        public string target { get; set; }
    }

    public class Instruc47
    {
        public NyxTimer81 nyxtimer { get; set; }
    }

    public class NyxTimer81
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command224[] commands { get; set; }
    }

    public class Command224
    {
        public Goto223 _goto { get; set; }
    }

    public class Goto223
    {
        public string target { get; set; }
    }

    public class T2SS3
    {
        public NyxPage189 nyxpage { get; set; }
    }

    public class NyxPage189
    {
        public Media189 media { get; set; }
        public string text { get; set; }
        public Action185 action { get; set; }
        public Instruc48 instruc { get; set; }
    }

    public class Media189
    {
        public string nyximage { get; set; }
    }

    public class Action185
    {
        public NyxVert143 nyxvert { get; set; }
    }

    public class NyxVert143
    {
        public Element143[] elements { get; set; }
    }

    public class Element143
    {
        public NyxButtons143[] nyxbuttons { get; set; }
    }

    public class NyxButtons143
    {
        public string label { get; set; }
        public Command225[] commands { get; set; }
    }

    public class Command225
    {
        public Goto224 _goto { get; set; }
    }

    public class Goto224
    {
        public string target { get; set; }
    }

    public class Instruc48
    {
        public NyxTimer82 nyxtimer { get; set; }
    }

    public class NyxTimer82
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command226[] commands { get; set; }
    }

    public class Command226
    {
        public Goto225 _goto { get; set; }
    }

    public class Goto225
    {
        public string target { get; set; }
    }

    public class T2SS4
    {
        public NyxPage190 nyxpage { get; set; }
    }

    public class NyxPage190
    {
        public Media190 media { get; set; }
        public string text { get; set; }
        public Action186 action { get; set; }
        public Instruc49 instruc { get; set; }
    }

    public class Media190
    {
        public string nyximage { get; set; }
    }

    public class Action186
    {
        public NyxVert144 nyxvert { get; set; }
    }

    public class NyxVert144
    {
        public Element144[] elements { get; set; }
    }

    public class Element144
    {
        public NyxButtons144[] nyxbuttons { get; set; }
    }

    public class NyxButtons144
    {
        public string label { get; set; }
        public Command227[] commands { get; set; }
    }

    public class Command227
    {
        public Goto226 _goto { get; set; }
    }

    public class Goto226
    {
        public string target { get; set; }
    }

    public class Instruc49
    {
        public NyxTimer83 nyxtimer { get; set; }
    }

    public class NyxTimer83
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command228[] commands { get; set; }
    }

    public class Command228
    {
        public Goto227 _goto { get; set; }
    }

    public class Goto227
    {
        public string target { get; set; }
    }

    public class T2SS5
    {
        public NyxPage191 nyxpage { get; set; }
    }

    public class NyxPage191
    {
        public Media191 media { get; set; }
        public string text { get; set; }
        public Action187 action { get; set; }
        public Instruc50 instruc { get; set; }
    }

    public class Media191
    {
        public string nyximage { get; set; }
    }

    public class Action187
    {
        public NyxVert145 nyxvert { get; set; }
    }

    public class NyxVert145
    {
        public Element145[] elements { get; set; }
    }

    public class Element145
    {
        public NyxButtons145[] nyxbuttons { get; set; }
    }

    public class NyxButtons145
    {
        public string label { get; set; }
        public Command229[] commands { get; set; }
    }

    public class Command229
    {
        public Goto228 _goto { get; set; }
    }

    public class Goto228
    {
        public string target { get; set; }
    }

    public class Instruc50
    {
        public NyxTimer84 nyxtimer { get; set; }
    }

    public class NyxTimer84
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command230[] commands { get; set; }
    }

    public class Command230
    {
        public Goto229 _goto { get; set; }
    }

    public class Goto229
    {
        public string target { get; set; }
    }

    public class T2ssfail
    {
        public NyxPage192 nyxpage { get; set; }
    }

    public class NyxPage192
    {
        public Media192 media { get; set; }
        public string text { get; set; }
        public Action188 action { get; set; }
    }

    public class Media192
    {
        public string nyximage { get; set; }
    }

    public class Action188
    {
        public NyxVert146 nyxvert { get; set; }
    }

    public class NyxVert146
    {
        public Element146[] elements { get; set; }
    }

    public class Element146
    {
        public NyxButtons146[] nyxbuttons { get; set; }
    }

    public class NyxButtons146
    {
        public string label { get; set; }
        public Command231[] commands { get; set; }
    }

    public class Command231
    {
        public Goto230 _goto { get; set; }
    }

    public class Goto230
    {
        public string target { get; set; }
    }

    public class T2sswin
    {
        public NyxPage193 nyxpage { get; set; }
    }

    public class NyxPage193
    {
        public Media193 media { get; set; }
        public string text { get; set; }
        public Action189 action { get; set; }
    }

    public class Media193
    {
        public string nyximage { get; set; }
    }

    public class Action189
    {
        public NyxVert147 nyxvert { get; set; }
    }

    public class NyxVert147
    {
        public Element147[] elements { get; set; }
    }

    public class Element147
    {
        public NyxButtons147[] nyxbuttons { get; set; }
    }

    public class NyxButtons147
    {
        public string label { get; set; }
        public Command232[] commands { get; set; }
    }

    public class Command232
    {
        public Goto231 _goto { get; set; }
    }

    public class Goto231
    {
        public string target { get; set; }
    }

    public class T2toy
    {
        public NyxPage194 nyxpage { get; set; }
    }

    public class NyxPage194
    {
        public Media194 media { get; set; }
        public string text { get; set; }
        public Action190 action { get; set; }
    }

    public class Media194
    {
        public string nyximage { get; set; }
    }

    public class Action190
    {
        public NyxVert148 nyxvert { get; set; }
    }

    public class NyxVert148
    {
        public Element148[] elements { get; set; }
    }

    public class Element148
    {
        public NyxButtons148[] nyxbuttons { get; set; }
    }

    public class NyxButtons148
    {
        public string label { get; set; }
        public Command233[] commands { get; set; }
    }

    public class Command233
    {
        public Goto232 _goto { get; set; }
    }

    public class Goto232
    {
        public string target { get; set; }
    }

    public class T2WR
    {
        public NyxPage195 nyxpage { get; set; }
    }

    public class NyxPage195
    {
        public Media195 media { get; set; }
        public string text { get; set; }
        public Action191 action { get; set; }
    }

    public class Media195
    {
        public string nyximage { get; set; }
    }

    public class Action191
    {
        public NyxVert149 nyxvert { get; set; }
    }

    public class NyxVert149
    {
        public Element149[] elements { get; set; }
    }

    public class Element149
    {
        public NyxButtons149[] nyxbuttons { get; set; }
    }

    public class NyxButtons149
    {
        public string label { get; set; }
        public Command234[] commands { get; set; }
    }

    public class Command234
    {
        public Goto233 _goto { get; set; }
    }

    public class Goto233
    {
        public string target { get; set; }
    }

    public class T2WR1
    {
        public NyxPage196 nyxpage { get; set; }
    }

    public class NyxPage196
    {
        public Media196 media { get; set; }
        public string text { get; set; }
        public Action192 action { get; set; }
        public Instruc51 instruc { get; set; }
    }

    public class Media196
    {
        public string nyximage { get; set; }
    }

    public class Action192
    {
        public NyxVert150 nyxvert { get; set; }
    }

    public class NyxVert150
    {
        public Element150[] elements { get; set; }
    }

    public class Element150
    {
        public NyxButtons150[] nyxbuttons { get; set; }
    }

    public class NyxButtons150
    {
        public string label { get; set; }
        public Command235[] commands { get; set; }
    }

    public class Command235
    {
        public Goto234 _goto { get; set; }
    }

    public class Goto234
    {
        public string target { get; set; }
    }

    public class Instruc51
    {
        public NyxTimer85 nyxtimer { get; set; }
    }

    public class NyxTimer85
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command236[] commands { get; set; }
    }

    public class Command236
    {
        public Goto235 _goto { get; set; }
    }

    public class Goto235
    {
        public string target { get; set; }
    }

    public class T2WR2
    {
        public NyxPage197 nyxpage { get; set; }
    }

    public class NyxPage197
    {
        public Media197 media { get; set; }
        public string text { get; set; }
        public Action193 action { get; set; }
        public Instruc52 instruc { get; set; }
    }

    public class Media197
    {
        public string nyximage { get; set; }
    }

    public class Action193
    {
        public NyxVert151 nyxvert { get; set; }
    }

    public class NyxVert151
    {
        public Element151[] elements { get; set; }
    }

    public class Element151
    {
        public NyxButtons151[] nyxbuttons { get; set; }
    }

    public class NyxButtons151
    {
        public string label { get; set; }
        public Command237[] commands { get; set; }
    }

    public class Command237
    {
        public Goto236 _goto { get; set; }
    }

    public class Goto236
    {
        public string target { get; set; }
    }

    public class Instruc52
    {
        public NyxTimer86 nyxtimer { get; set; }
    }

    public class NyxTimer86
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command238[] commands { get; set; }
    }

    public class Command238
    {
        public Goto237 _goto { get; set; }
    }

    public class Goto237
    {
        public string target { get; set; }
    }

    public class T2WR3
    {
        public NyxPage198 nyxpage { get; set; }
    }

    public class NyxPage198
    {
        public Media198 media { get; set; }
        public string text { get; set; }
        public Action194 action { get; set; }
        public Instruc53 instruc { get; set; }
    }

    public class Media198
    {
        public string nyximage { get; set; }
    }

    public class Action194
    {
        public NyxVert152 nyxvert { get; set; }
    }

    public class NyxVert152
    {
        public Element152[] elements { get; set; }
    }

    public class Element152
    {
        public NyxButtons152[] nyxbuttons { get; set; }
    }

    public class NyxButtons152
    {
        public string label { get; set; }
        public Command239[] commands { get; set; }
    }

    public class Command239
    {
        public Goto238 _goto { get; set; }
    }

    public class Goto238
    {
        public string target { get; set; }
    }

    public class Instruc53
    {
        public NyxTimer87 nyxtimer { get; set; }
    }

    public class NyxTimer87
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command240[] commands { get; set; }
    }

    public class Command240
    {
        public Goto239 _goto { get; set; }
    }

    public class Goto239
    {
        public string target { get; set; }
    }

    public class T2WR4
    {
        public NyxPage199 nyxpage { get; set; }
    }

    public class NyxPage199
    {
        public Media199 media { get; set; }
        public string text { get; set; }
        public Action195 action { get; set; }
        public Instruc54 instruc { get; set; }
    }

    public class Media199
    {
        public string nyximage { get; set; }
    }

    public class Action195
    {
        public NyxVert153 nyxvert { get; set; }
    }

    public class NyxVert153
    {
        public Element153[] elements { get; set; }
    }

    public class Element153
    {
        public NyxButtons153[] nyxbuttons { get; set; }
    }

    public class NyxButtons153
    {
        public string label { get; set; }
        public Command241[] commands { get; set; }
    }

    public class Command241
    {
        public Goto240 _goto { get; set; }
    }

    public class Goto240
    {
        public string target { get; set; }
    }

    public class Instruc54
    {
        public NyxTimer88 nyxtimer { get; set; }
    }

    public class NyxTimer88
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command242[] commands { get; set; }
    }

    public class Command242
    {
        public Goto241 _goto { get; set; }
    }

    public class Goto241
    {
        public string target { get; set; }
    }

    public class T2WR5
    {
        public NyxPage200 nyxpage { get; set; }
    }

    public class NyxPage200
    {
        public Media200 media { get; set; }
        public string text { get; set; }
        public Action196 action { get; set; }
        public Instruc55 instruc { get; set; }
    }

    public class Media200
    {
        public string nyximage { get; set; }
    }

    public class Action196
    {
        public NyxVert154 nyxvert { get; set; }
    }

    public class NyxVert154
    {
        public Element154[] elements { get; set; }
    }

    public class Element154
    {
        public NyxButtons154[] nyxbuttons { get; set; }
    }

    public class NyxButtons154
    {
        public string label { get; set; }
        public Command243[] commands { get; set; }
    }

    public class Command243
    {
        public Goto242 _goto { get; set; }
    }

    public class Goto242
    {
        public string target { get; set; }
    }

    public class Instruc55
    {
        public NyxTimer89 nyxtimer { get; set; }
    }

    public class NyxTimer89
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command244[] commands { get; set; }
    }

    public class Command244
    {
        public Goto243 _goto { get; set; }
    }

    public class Goto243
    {
        public string target { get; set; }
    }

    public class T2wrfail
    {
        public NyxPage201 nyxpage { get; set; }
    }

    public class NyxPage201
    {
        public Media201 media { get; set; }
        public string text { get; set; }
        public Action197 action { get; set; }
    }

    public class Media201
    {
        public string nyximage { get; set; }
    }

    public class Action197
    {
        public NyxVert155 nyxvert { get; set; }
    }

    public class NyxVert155
    {
        public Element155[] elements { get; set; }
    }

    public class Element155
    {
        public NyxButtons155[] nyxbuttons { get; set; }
    }

    public class NyxButtons155
    {
        public string label { get; set; }
        public Command245[] commands { get; set; }
    }

    public class Command245
    {
        public Goto244 _goto { get; set; }
    }

    public class Goto244
    {
        public string target { get; set; }
    }

    public class T2wrwin
    {
        public NyxPage202 nyxpage { get; set; }
    }

    public class NyxPage202
    {
        public Media202 media { get; set; }
        public string text { get; set; }
        public Action198 action { get; set; }
    }

    public class Media202
    {
        public string nyximage { get; set; }
    }

    public class Action198
    {
        public NyxVert156 nyxvert { get; set; }
    }

    public class NyxVert156
    {
        public Element156[] elements { get; set; }
    }

    public class Element156
    {
        public NyxButtons156[] nyxbuttons { get; set; }
    }

    public class NyxButtons156
    {
        public string label { get; set; }
        public Command246[] commands { get; set; }
    }

    public class Command246
    {
        public Goto245 _goto { get; set; }
    }

    public class Goto245
    {
        public string target { get; set; }
    }

    public class T3ES
    {
        public NyxPage203 nyxpage { get; set; }
    }

    public class NyxPage203
    {
        public Media203 media { get; set; }
        public string text { get; set; }
        public Action199 action { get; set; }
    }

    public class Media203
    {
        public string nyximage { get; set; }
    }

    public class Action199
    {
        public NyxVert157 nyxvert { get; set; }
    }

    public class NyxVert157
    {
        public Element157[] elements { get; set; }
    }

    public class Element157
    {
        public NyxButtons157[] nyxbuttons { get; set; }
    }

    public class NyxButtons157
    {
        public string label { get; set; }
        public Command247[] commands { get; set; }
    }

    public class Command247
    {
        public Goto246 _goto { get; set; }
    }

    public class Goto246
    {
        public string target { get; set; }
    }

    public class T3ES1
    {
        public NyxPage204 nyxpage { get; set; }
    }

    public class NyxPage204
    {
        public Media204 media { get; set; }
        public string text { get; set; }
        public Action200 action { get; set; }
        public Instruc56 instruc { get; set; }
    }

    public class Media204
    {
        public string nyximage { get; set; }
    }

    public class Action200
    {
        public NyxVert158 nyxvert { get; set; }
    }

    public class NyxVert158
    {
        public Element158[] elements { get; set; }
    }

    public class Element158
    {
        public NyxButtons158[] nyxbuttons { get; set; }
    }

    public class NyxButtons158
    {
        public string label { get; set; }
        public Command248[] commands { get; set; }
    }

    public class Command248
    {
        public Goto247 _goto { get; set; }
    }

    public class Goto247
    {
        public string target { get; set; }
    }

    public class Instruc56
    {
        public NyxTimer90 nyxtimer { get; set; }
    }

    public class NyxTimer90
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command249[] commands { get; set; }
    }

    public class Command249
    {
        public Goto248 _goto { get; set; }
    }

    public class Goto248
    {
        public string target { get; set; }
    }

    public class T3ES2
    {
        public NyxPage205 nyxpage { get; set; }
    }

    public class NyxPage205
    {
        public Media205 media { get; set; }
        public string text { get; set; }
        public Action201 action { get; set; }
        public Instruc57 instruc { get; set; }
    }

    public class Media205
    {
        public string nyximage { get; set; }
    }

    public class Action201
    {
        public NyxVert159 nyxvert { get; set; }
    }

    public class NyxVert159
    {
        public Element159[] elements { get; set; }
    }

    public class Element159
    {
        public NyxButtons159[] nyxbuttons { get; set; }
    }

    public class NyxButtons159
    {
        public string label { get; set; }
        public Command250[] commands { get; set; }
    }

    public class Command250
    {
        public Goto249 _goto { get; set; }
    }

    public class Goto249
    {
        public string target { get; set; }
    }

    public class Instruc57
    {
        public NyxTimer91 nyxtimer { get; set; }
    }

    public class NyxTimer91
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command251[] commands { get; set; }
    }

    public class Command251
    {
        public Goto250 _goto { get; set; }
    }

    public class Goto250
    {
        public string target { get; set; }
    }

    public class T3ES3
    {
        public NyxPage206 nyxpage { get; set; }
    }

    public class NyxPage206
    {
        public Media206 media { get; set; }
        public string text { get; set; }
        public Action202 action { get; set; }
    }

    public class Media206
    {
        public string nyximage { get; set; }
    }

    public class Action202
    {
        public NyxVert160 nyxvert { get; set; }
    }

    public class NyxVert160
    {
        public Element160[] elements { get; set; }
    }

    public class Element160
    {
        public NyxButtons160[] nyxbuttons { get; set; }
    }

    public class NyxButtons160
    {
        public string label { get; set; }
        public Command252[] commands { get; set; }
    }

    public class Command252
    {
        public Goto251 _goto { get; set; }
    }

    public class Goto251
    {
        public string target { get; set; }
    }

    public class T3esfail
    {
        public NyxPage207 nyxpage { get; set; }
    }

    public class NyxPage207
    {
        public Media207 media { get; set; }
        public string text { get; set; }
        public Action203 action { get; set; }
    }

    public class Media207
    {
        public string nyximage { get; set; }
    }

    public class Action203
    {
        public NyxVert161 nyxvert { get; set; }
    }

    public class NyxVert161
    {
        public Element161[] elements { get; set; }
    }

    public class Element161
    {
        public NyxButtons161[] nyxbuttons { get; set; }
    }

    public class NyxButtons161
    {
        public string label { get; set; }
        public Command253[] commands { get; set; }
    }

    public class Command253
    {
        public Goto252 _goto { get; set; }
    }

    public class Goto252
    {
        public string target { get; set; }
    }

    public class T3eswin
    {
        public NyxPage208 nyxpage { get; set; }
    }

    public class NyxPage208
    {
        public Media208 media { get; set; }
        public string text { get; set; }
        public Action204 action { get; set; }
    }

    public class Media208
    {
        public string nyximage { get; set; }
    }

    public class Action204
    {
        public NyxVert162 nyxvert { get; set; }
    }

    public class NyxVert162
    {
        public Element162[] elements { get; set; }
    }

    public class Element162
    {
        public NyxButtons162[] nyxbuttons { get; set; }
    }

    public class NyxButtons162
    {
        public string label { get; set; }
        public Command254[] commands { get; set; }
    }

    public class Command254
    {
        public Goto253 _goto { get; set; }
    }

    public class Goto253
    {
        public string target { get; set; }
    }

    public class T3SD
    {
        public NyxPage209 nyxpage { get; set; }
    }

    public class NyxPage209
    {
        public Media209 media { get; set; }
        public string text { get; set; }
        public Action205 action { get; set; }
    }

    public class Media209
    {
        public string nyximage { get; set; }
    }

    public class Action205
    {
        public NyxVert163 nyxvert { get; set; }
    }

    public class NyxVert163
    {
        public Element163[] elements { get; set; }
    }

    public class Element163
    {
        public NyxButtons163[] nyxbuttons { get; set; }
    }

    public class NyxButtons163
    {
        public string label { get; set; }
        public Command255[] commands { get; set; }
    }

    public class Command255
    {
        public Goto254 _goto { get; set; }
    }

    public class Goto254
    {
        public string target { get; set; }
    }

    public class T3SD1
    {
        public NyxPage210 nyxpage { get; set; }
    }

    public class NyxPage210
    {
        public Media210 media { get; set; }
        public string text { get; set; }
        public Action206 action { get; set; }
    }

    public class Media210
    {
        public string nyximage { get; set; }
    }

    public class Action206
    {
        public NyxVert164 nyxvert { get; set; }
    }

    public class NyxVert164
    {
        public Element164[] elements { get; set; }
    }

    public class Element164
    {
        public NyxButtons164[] nyxbuttons { get; set; }
    }

    public class NyxButtons164
    {
        public string label { get; set; }
        public Command256[] commands { get; set; }
    }

    public class Command256
    {
        public Goto255 _goto { get; set; }
    }

    public class Goto255
    {
        public string target { get; set; }
    }

    public class T3SD2
    {
        public NyxPage211 nyxpage { get; set; }
    }

    public class NyxPage211
    {
        public Media211 media { get; set; }
        public string text { get; set; }
        public Action207 action { get; set; }
    }

    public class Media211
    {
        public string nyximage { get; set; }
    }

    public class Action207
    {
        public NyxVert165 nyxvert { get; set; }
    }

    public class NyxVert165
    {
        public Element165[] elements { get; set; }
    }

    public class Element165
    {
        public NyxButtons165[] nyxbuttons { get; set; }
    }

    public class NyxButtons165
    {
        public string label { get; set; }
        public Command257[] commands { get; set; }
    }

    public class Command257
    {
        public Goto256 _goto { get; set; }
    }

    public class Goto256
    {
        public string target { get; set; }
    }

    public class T3SD3
    {
        public NyxPage212 nyxpage { get; set; }
    }

    public class NyxPage212
    {
        public Media212 media { get; set; }
        public string text { get; set; }
        public Action208 action { get; set; }
    }

    public class Media212
    {
        public string nyximage { get; set; }
    }

    public class Action208
    {
        public NyxVert166 nyxvert { get; set; }
    }

    public class NyxVert166
    {
        public Element166[] elements { get; set; }
    }

    public class Element166
    {
        public NyxButtons166[] nyxbuttons { get; set; }
    }

    public class NyxButtons166
    {
        public string label { get; set; }
        public Command258[] commands { get; set; }
    }

    public class Command258
    {
        public Goto257 _goto { get; set; }
    }

    public class Goto257
    {
        public string target { get; set; }
    }

    public class T3SD4
    {
        public NyxPage213 nyxpage { get; set; }
    }

    public class NyxPage213
    {
        public Media213 media { get; set; }
        public string text { get; set; }
        public Action209 action { get; set; }
        public Instruc58 instruc { get; set; }
    }

    public class Media213
    {
        public string nyximage { get; set; }
    }

    public class Action209
    {
        public NyxVert167 nyxvert { get; set; }
    }

    public class NyxVert167
    {
        public Element167[] elements { get; set; }
    }

    public class Element167
    {
        public NyxButtons167[] nyxbuttons { get; set; }
    }

    public class NyxButtons167
    {
        public string label { get; set; }
        public Command259[] commands { get; set; }
    }

    public class Command259
    {
        public Goto258 _goto { get; set; }
    }

    public class Goto258
    {
        public string target { get; set; }
    }

    public class Instruc58
    {
        public NyxTimer92 nyxtimer { get; set; }
    }

    public class NyxTimer92
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command260[] commands { get; set; }
    }

    public class Command260
    {
        public Goto259 _goto { get; set; }
    }

    public class Goto259
    {
        public string target { get; set; }
    }

    public class T3SD5
    {
        public NyxPage214 nyxpage { get; set; }
    }

    public class NyxPage214
    {
        public Media214 media { get; set; }
        public string text { get; set; }
        public Action210 action { get; set; }
        public Instruc59 instruc { get; set; }
    }

    public class Media214
    {
        public string nyximage { get; set; }
    }

    public class Action210
    {
        public NyxVert168 nyxvert { get; set; }
    }

    public class NyxVert168
    {
        public Element168[] elements { get; set; }
    }

    public class Element168
    {
        public NyxButtons168[] nyxbuttons { get; set; }
    }

    public class NyxButtons168
    {
        public string label { get; set; }
        public Command261[] commands { get; set; }
    }

    public class Command261
    {
        public Goto260 _goto { get; set; }
    }

    public class Goto260
    {
        public string target { get; set; }
    }

    public class Instruc59
    {
        public NyxTimer93 nyxtimer { get; set; }
    }

    public class NyxTimer93
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command262[] commands { get; set; }
    }

    public class Command262
    {
        public Goto261 _goto { get; set; }
    }

    public class Goto261
    {
        public string target { get; set; }
    }

    public class T3SD6
    {
        public NyxPage215 nyxpage { get; set; }
    }

    public class NyxPage215
    {
        public Media215 media { get; set; }
        public string text { get; set; }
        public Action211 action { get; set; }
    }

    public class Media215
    {
        public string nyximage { get; set; }
    }

    public class Action211
    {
        public NyxVert169 nyxvert { get; set; }
    }

    public class NyxVert169
    {
        public Element169[] elements { get; set; }
    }

    public class Element169
    {
        public NyxButtons169[] nyxbuttons { get; set; }
    }

    public class NyxButtons169
    {
        public string label { get; set; }
        public Command263[] commands { get; set; }
    }

    public class Command263
    {
        public Goto262 _goto { get; set; }
    }

    public class Goto262
    {
        public string target { get; set; }
    }

    public class T3sdfail
    {
        public NyxPage216 nyxpage { get; set; }
    }

    public class NyxPage216
    {
        public Media216 media { get; set; }
        public string text { get; set; }
        public Action212 action { get; set; }
    }

    public class Media216
    {
        public string nyximage { get; set; }
    }

    public class Action212
    {
        public NyxVert170 nyxvert { get; set; }
    }

    public class NyxVert170
    {
        public Element170[] elements { get; set; }
    }

    public class Element170
    {
        public NyxButtons170[] nyxbuttons { get; set; }
    }

    public class NyxButtons170
    {
        public string label { get; set; }
        public Command264[] commands { get; set; }
    }

    public class Command264
    {
        public Goto263 _goto { get; set; }
    }

    public class Goto263
    {
        public string target { get; set; }
    }

    public class T3sdwin
    {
        public NyxPage217 nyxpage { get; set; }
    }

    public class NyxPage217
    {
        public Media217 media { get; set; }
        public string text { get; set; }
        public Action213 action { get; set; }
    }

    public class Media217
    {
        public string nyximage { get; set; }
    }

    public class Action213
    {
        public NyxVert171 nyxvert { get; set; }
    }

    public class NyxVert171
    {
        public Element171[] elements { get; set; }
    }

    public class Element171
    {
        public NyxButtons171[] nyxbuttons { get; set; }
    }

    public class NyxButtons171
    {
        public string label { get; set; }
        public Command265[] commands { get; set; }
    }

    public class Command265
    {
        public Goto264 _goto { get; set; }
    }

    public class Goto264
    {
        public string target { get; set; }
    }

    public class T3slut
    {
        public NyxPage218 nyxpage { get; set; }
    }

    public class NyxPage218
    {
        public Media218 media { get; set; }
        public string text { get; set; }
        public Action214 action { get; set; }
    }

    public class Media218
    {
        public string nyximage { get; set; }
    }

    public class Action214
    {
        public NyxTimer94 nyxtimer { get; set; }
    }

    public class NyxTimer94
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command266[] commands { get; set; }
    }

    public class Command266
    {
        public Pcm2Range1 pcm2range { get; set; }
    }

    public class Pcm2Range1
    {
        public int to { get; set; }
        public int from { get; set; }
        public string prefix { get; set; }
    }

    public class T3toy
    {
        public NyxPage219 nyxpage { get; set; }
    }

    public class NyxPage219
    {
        public Media219 media { get; set; }
        public string text { get; set; }
        public Action215 action { get; set; }
    }

    public class Media219
    {
        public string nyximage { get; set; }
    }

    public class Action215
    {
        public NyxVert172 nyxvert { get; set; }
    }

    public class NyxVert172
    {
        public Element172[] elements { get; set; }
    }

    public class Element172
    {
        public NyxButtons172[] nyxbuttons { get; set; }
    }

    public class NyxButtons172
    {
        public string label { get; set; }
        public Command267[] commands { get; set; }
    }

    public class Command267
    {
        public Goto265 _goto { get; set; }
    }

    public class Goto265
    {
        public string target { get; set; }
    }

    public class T4E1
    {
        public NyxPage220 nyxpage { get; set; }
    }

    public class NyxPage220
    {
        public Media220 media { get; set; }
        public string text { get; set; }
        public Action216 action { get; set; }
    }

    public class Media220
    {
        public string nyximage { get; set; }
    }

    public class Action216
    {
        public NyxTimer95 nyxtimer { get; set; }
    }

    public class NyxTimer95
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command268[] commands { get; set; }
    }

    public class Command268
    {
        public Goto266 _goto { get; set; }
    }

    public class Goto266
    {
        public string target { get; set; }
    }

    public class T4E2
    {
        public NyxPage221 nyxpage { get; set; }
    }

    public class NyxPage221
    {
        public Media221 media { get; set; }
        public string text { get; set; }
        public Action217 action { get; set; }
    }

    public class Media221
    {
        public string nyximage { get; set; }
    }

    public class Action217
    {
        public NyxTimer96 nyxtimer { get; set; }
    }

    public class NyxTimer96
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command269[] commands { get; set; }
    }

    public class Command269
    {
        public Goto267 _goto { get; set; }
    }

    public class Goto267
    {
        public string target { get; set; }
    }

    public class T4E3
    {
        public NyxPage222 nyxpage { get; set; }
    }

    public class NyxPage222
    {
        public Media222 media { get; set; }
        public string text { get; set; }
        public Action218 action { get; set; }
    }

    public class Media222
    {
        public string nyximage { get; set; }
    }

    public class Action218
    {
        public NyxTimer97 nyxtimer { get; set; }
    }

    public class NyxTimer97
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command270[] commands { get; set; }
    }

    public class Command270
    {
        public Goto268 _goto { get; set; }
    }

    public class Goto268
    {
        public string target { get; set; }
    }

    public class T4E4
    {
        public NyxPage223 nyxpage { get; set; }
    }

    public class NyxPage223
    {
        public Media223 media { get; set; }
        public string text { get; set; }
        public Action219 action { get; set; }
    }

    public class Media223
    {
        public string nyximage { get; set; }
    }

    public class Action219
    {
        public NyxTimer98 nyxtimer { get; set; }
    }

    public class NyxTimer98
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command271[] commands { get; set; }
    }

    public class Command271
    {
        public Goto269 _goto { get; set; }
    }

    public class Goto269
    {
        public string target { get; set; }
    }

    public class T4E5
    {
        public NyxPage224 nyxpage { get; set; }
    }

    public class NyxPage224
    {
        public Media224 media { get; set; }
        public string text { get; set; }
        public Action220 action { get; set; }
    }

    public class Media224
    {
        public string nyximage { get; set; }
    }

    public class Action220
    {
        public NyxTimer99 nyxtimer { get; set; }
    }

    public class NyxTimer99
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command272[] commands { get; set; }
    }

    public class Command272
    {
        public Pcm2Range2 pcm2range { get; set; }
    }

    public class Pcm2Range2
    {
        public int to { get; set; }
        public int from { get; set; }
        public string prefix { get; set; }
    }

    public class T4lose
    {
        public NyxPage225 nyxpage { get; set; }
    }

    public class NyxPage225
    {
        public Media225 media { get; set; }
        public string text { get; set; }
        public Action221 action { get; set; }
    }

    public class Media225
    {
        public string nyximage { get; set; }
    }

    public class Action221
    {
        public NyxVert173 nyxvert { get; set; }
    }

    public class NyxVert173
    {
        public Element173[] elements { get; set; }
    }

    public class Element173
    {
        public NyxButtons173[] nyxbuttons { get; set; }
    }

    public class NyxButtons173
    {
        public string label { get; set; }
        public Command273[] commands { get; set; }
    }

    public class Command273
    {
        public Goto270 _goto { get; set; }
    }

    public class Goto270
    {
        public string target { get; set; }
    }

    public class T4slut
    {
        public NyxPage226 nyxpage { get; set; }
    }

    public class NyxPage226
    {
        public Media226 media { get; set; }
        public string text { get; set; }
        public Action222 action { get; set; }
    }

    public class Media226
    {
        public string nyximage { get; set; }
    }

    public class Action222
    {
        public NyxVert174 nyxvert { get; set; }
    }

    public class NyxVert174
    {
        public Element174[] elements { get; set; }
    }

    public class Element174
    {
        public NyxButtons174[] nyxbuttons { get; set; }
    }

    public class NyxButtons174
    {
        public string label { get; set; }
        public Command274[] commands { get; set; }
    }

    public class Command274
    {
        public Goto271 _goto { get; set; }
    }

    public class Goto271
    {
        public string target { get; set; }
    }

    public class T4start
    {
        public NyxPage227 nyxpage { get; set; }
    }

    public class NyxPage227
    {
        public Media227 media { get; set; }
        public string text { get; set; }
        public Action223 action { get; set; }
    }

    public class Media227
    {
        public string nyximage { get; set; }
    }

    public class Action223
    {
        public NyxVert175 nyxvert { get; set; }
    }

    public class NyxVert175
    {
        public Element175[] elements { get; set; }
    }

    public class Element175
    {
        public NyxButtons175[] nyxbuttons { get; set; }
    }

    public class NyxButtons175
    {
        public string label { get; set; }
        public Command275[] commands { get; set; }
    }

    public class Command275
    {
        public Goto272 _goto { get; set; }
    }

    public class Goto272
    {
        public string target { get; set; }
    }

    public class T4toy
    {
        public NyxPage228 nyxpage { get; set; }
    }

    public class NyxPage228
    {
        public Media228 media { get; set; }
        public string text { get; set; }
        public Action224 action { get; set; }
    }

    public class Media228
    {
        public string nyximage { get; set; }
    }

    public class Action224
    {
        public NyxVert176 nyxvert { get; set; }
    }

    public class NyxVert176
    {
        public Element176[] elements { get; set; }
    }

    public class Element176
    {
        public NyxButtons176[] nyxbuttons { get; set; }
    }

    public class NyxButtons176
    {
        public string label { get; set; }
        public Command276[] commands { get; set; }
    }

    public class Command276
    {
        public Goto273 _goto { get; set; }
    }

    public class Goto273
    {
        public string target { get; set; }
    }

    public class T4win
    {
        public NyxPage229 nyxpage { get; set; }
    }

    public class NyxPage229
    {
        public Media229 media { get; set; }
        public string text { get; set; }
        public Action225 action { get; set; }
    }

    public class Media229
    {
        public string nyximage { get; set; }
    }

    public class Action225
    {
        public NyxVert177 nyxvert { get; set; }
    }

    public class NyxVert177
    {
        public Element177[] elements { get; set; }
    }

    public class Element177
    {
        public NyxButtons177[] nyxbuttons { get; set; }
    }

    public class NyxButtons177
    {
        public string label { get; set; }
        public Command277[] commands { get; set; }
    }

    public class Command277
    {
        public Goto274 _goto { get; set; }
    }

    public class Goto274
    {
        public string target { get; set; }
    }

    public class T5E1
    {
        public NyxPage230 nyxpage { get; set; }
    }

    public class NyxPage230
    {
        public Media230 media { get; set; }
        public string text { get; set; }
        public Action226 action { get; set; }
        public Instruc60 instruc { get; set; }
    }

    public class Media230
    {
        public string nyximage { get; set; }
    }

    public class Action226
    {
        public NyxVert178 nyxvert { get; set; }
    }

    public class NyxVert178
    {
        public Element178[] elements { get; set; }
    }

    public class Element178
    {
        public NyxButtons178[] nyxbuttons { get; set; }
    }

    public class NyxButtons178
    {
        public string label { get; set; }
        public Command278[] commands { get; set; }
    }

    public class Command278
    {
        public Goto275 _goto { get; set; }
    }

    public class Goto275
    {
        public string target { get; set; }
    }

    public class Instruc60
    {
        public NyxTimer100 nyxtimer { get; set; }
    }

    public class NyxTimer100
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command279[] commands { get; set; }
    }

    public class Command279
    {
        public Goto276 _goto { get; set; }
    }

    public class Goto276
    {
        public string target { get; set; }
    }

    public class T5E10
    {
        public NyxPage231 nyxpage { get; set; }
    }

    public class NyxPage231
    {
        public Media231 media { get; set; }
        public string text { get; set; }
        public Action227 action { get; set; }
    }

    public class Media231
    {
        public string nyximage { get; set; }
    }

    public class Action227
    {
        public NyxVert179 nyxvert { get; set; }
    }

    public class NyxVert179
    {
        public Element179[] elements { get; set; }
    }

    public class Element179
    {
        public NyxButtons179[] nyxbuttons { get; set; }
    }

    public class NyxButtons179
    {
        public string label { get; set; }
        public Command280[] commands { get; set; }
    }

    public class Command280
    {
        public Goto277 _goto { get; set; }
    }

    public class Goto277
    {
        public string target { get; set; }
    }

    public class T5E2
    {
        public NyxPage232 nyxpage { get; set; }
    }

    public class NyxPage232
    {
        public Media232 media { get; set; }
        public string text { get; set; }
        public Action228 action { get; set; }
        public Instruc61 instruc { get; set; }
    }

    public class Media232
    {
        public string nyximage { get; set; }
    }

    public class Action228
    {
        public NyxVert180 nyxvert { get; set; }
    }

    public class NyxVert180
    {
        public Element180[] elements { get; set; }
    }

    public class Element180
    {
        public NyxButtons180[] nyxbuttons { get; set; }
    }

    public class NyxButtons180
    {
        public string label { get; set; }
        public Command281[] commands { get; set; }
    }

    public class Command281
    {
        public Goto278 _goto { get; set; }
    }

    public class Goto278
    {
        public string target { get; set; }
    }

    public class Instruc61
    {
        public NyxTimer101 nyxtimer { get; set; }
    }

    public class NyxTimer101
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command282[] commands { get; set; }
    }

    public class Command282
    {
        public Goto279 _goto { get; set; }
    }

    public class Goto279
    {
        public string target { get; set; }
    }

    public class T5E3
    {
        public NyxPage233 nyxpage { get; set; }
    }

    public class NyxPage233
    {
        public Media233 media { get; set; }
        public string text { get; set; }
        public Action229 action { get; set; }
        public Instruc62 instruc { get; set; }
    }

    public class Media233
    {
        public string nyximage { get; set; }
    }

    public class Action229
    {
        public NyxVert181 nyxvert { get; set; }
    }

    public class NyxVert181
    {
        public Element181[] elements { get; set; }
    }

    public class Element181
    {
        public NyxButtons181[] nyxbuttons { get; set; }
    }

    public class NyxButtons181
    {
        public string label { get; set; }
        public Command283[] commands { get; set; }
    }

    public class Command283
    {
        public Goto280 _goto { get; set; }
    }

    public class Goto280
    {
        public string target { get; set; }
    }

    public class Instruc62
    {
        public NyxTimer102 nyxtimer { get; set; }
    }

    public class NyxTimer102
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command284[] commands { get; set; }
    }

    public class Command284
    {
        public Goto281 _goto { get; set; }
    }

    public class Goto281
    {
        public string target { get; set; }
    }

    public class T5E4
    {
        public NyxPage234 nyxpage { get; set; }
    }

    public class NyxPage234
    {
        public Media234 media { get; set; }
        public string text { get; set; }
        public Action230 action { get; set; }
        public Instruc63 instruc { get; set; }
    }

    public class Media234
    {
        public string nyximage { get; set; }
    }

    public class Action230
    {
        public NyxVert182 nyxvert { get; set; }
    }

    public class NyxVert182
    {
        public Element182[] elements { get; set; }
    }

    public class Element182
    {
        public NyxButtons182[] nyxbuttons { get; set; }
    }

    public class NyxButtons182
    {
        public string label { get; set; }
        public Command285[] commands { get; set; }
    }

    public class Command285
    {
        public Goto282 _goto { get; set; }
    }

    public class Goto282
    {
        public string target { get; set; }
    }

    public class Instruc63
    {
        public NyxTimer103 nyxtimer { get; set; }
    }

    public class NyxTimer103
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command286[] commands { get; set; }
    }

    public class Command286
    {
        public Goto283 _goto { get; set; }
    }

    public class Goto283
    {
        public string target { get; set; }
    }

    public class T5E5
    {
        public NyxPage235 nyxpage { get; set; }
    }

    public class NyxPage235
    {
        public Media235 media { get; set; }
        public string text { get; set; }
        public Action231 action { get; set; }
        public Instruc64 instruc { get; set; }
    }

    public class Media235
    {
        public string nyximage { get; set; }
    }

    public class Action231
    {
        public NyxVert183 nyxvert { get; set; }
    }

    public class NyxVert183
    {
        public Element183[] elements { get; set; }
    }

    public class Element183
    {
        public NyxButtons183[] nyxbuttons { get; set; }
    }

    public class NyxButtons183
    {
        public string label { get; set; }
        public Command287[] commands { get; set; }
    }

    public class Command287
    {
        public Goto284 _goto { get; set; }
    }

    public class Goto284
    {
        public string target { get; set; }
    }

    public class Instruc64
    {
        public NyxTimer104 nyxtimer { get; set; }
    }

    public class NyxTimer104
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command288[] commands { get; set; }
    }

    public class Command288
    {
        public Goto285 _goto { get; set; }
    }

    public class Goto285
    {
        public string target { get; set; }
    }

    public class T5E6
    {
        public NyxPage236 nyxpage { get; set; }
    }

    public class NyxPage236
    {
        public Media236 media { get; set; }
        public string text { get; set; }
        public Action232 action { get; set; }
        public Instruc65 instruc { get; set; }
    }

    public class Media236
    {
        public string nyximage { get; set; }
    }

    public class Action232
    {
        public NyxVert184 nyxvert { get; set; }
    }

    public class NyxVert184
    {
        public Element184[] elements { get; set; }
    }

    public class Element184
    {
        public NyxButtons184[] nyxbuttons { get; set; }
    }

    public class NyxButtons184
    {
        public string label { get; set; }
        public Command289[] commands { get; set; }
    }

    public class Command289
    {
        public Goto286 _goto { get; set; }
    }

    public class Goto286
    {
        public string target { get; set; }
    }

    public class Instruc65
    {
        public NyxTimer105 nyxtimer { get; set; }
    }

    public class NyxTimer105
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command290[] commands { get; set; }
    }

    public class Command290
    {
        public Goto287 _goto { get; set; }
    }

    public class Goto287
    {
        public string target { get; set; }
    }

    public class T5E7
    {
        public NyxPage237 nyxpage { get; set; }
    }

    public class NyxPage237
    {
        public Media237 media { get; set; }
        public string text { get; set; }
        public Action233 action { get; set; }
        public Instruc66 instruc { get; set; }
    }

    public class Media237
    {
        public string nyximage { get; set; }
    }

    public class Action233
    {
        public NyxVert185 nyxvert { get; set; }
    }

    public class NyxVert185
    {
        public Element185[] elements { get; set; }
    }

    public class Element185
    {
        public NyxButtons185[] nyxbuttons { get; set; }
    }

    public class NyxButtons185
    {
        public string label { get; set; }
        public Command291[] commands { get; set; }
    }

    public class Command291
    {
        public Goto288 _goto { get; set; }
    }

    public class Goto288
    {
        public string target { get; set; }
    }

    public class Instruc66
    {
        public NyxTimer106 nyxtimer { get; set; }
    }

    public class NyxTimer106
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command292[] commands { get; set; }
    }

    public class Command292
    {
        public Goto289 _goto { get; set; }
    }

    public class Goto289
    {
        public string target { get; set; }
    }

    public class T5E8
    {
        public NyxPage238 nyxpage { get; set; }
    }

    public class NyxPage238
    {
        public Media238 media { get; set; }
        public string text { get; set; }
        public Action234 action { get; set; }
        public Instruc67 instruc { get; set; }
    }

    public class Media238
    {
        public string nyximage { get; set; }
    }

    public class Action234
    {
        public NyxVert186 nyxvert { get; set; }
    }

    public class NyxVert186
    {
        public Element186[] elements { get; set; }
    }

    public class Element186
    {
        public NyxButtons186[] nyxbuttons { get; set; }
    }

    public class NyxButtons186
    {
        public string label { get; set; }
        public Command293[] commands { get; set; }
    }

    public class Command293
    {
        public Goto290 _goto { get; set; }
    }

    public class Goto290
    {
        public string target { get; set; }
    }

    public class Instruc67
    {
        public NyxTimer107 nyxtimer { get; set; }
    }

    public class NyxTimer107
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command294[] commands { get; set; }
    }

    public class Command294
    {
        public Goto291 _goto { get; set; }
    }

    public class Goto291
    {
        public string target { get; set; }
    }

    public class T5E9
    {
        public NyxPage239 nyxpage { get; set; }
    }

    public class NyxPage239
    {
        public Media239 media { get; set; }
        public string text { get; set; }
        public Action235 action { get; set; }
        public Instruc68 instruc { get; set; }
    }

    public class Media239
    {
        public string nyximage { get; set; }
    }

    public class Action235
    {
        public NyxVert187 nyxvert { get; set; }
    }

    public class NyxVert187
    {
        public Element187[] elements { get; set; }
    }

    public class Element187
    {
        public NyxButtons187[] nyxbuttons { get; set; }
    }

    public class NyxButtons187
    {
        public string label { get; set; }
        public Command295[] commands { get; set; }
    }

    public class Command295
    {
        public Goto292 _goto { get; set; }
    }

    public class Goto292
    {
        public string target { get; set; }
    }

    public class Instruc68
    {
        public NyxTimer108 nyxtimer { get; set; }
    }

    public class NyxTimer108
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command296[] commands { get; set; }
    }

    public class Command296
    {
        public Goto293 _goto { get; set; }
    }

    public class Goto293
    {
        public string target { get; set; }
    }

    public class T5lose
    {
        public NyxPage240 nyxpage { get; set; }
    }

    public class NyxPage240
    {
        public Media240 media { get; set; }
        public string text { get; set; }
        public Action236 action { get; set; }
    }

    public class Media240
    {
        public string nyximage { get; set; }
    }

    public class Action236
    {
        public NyxVert188 nyxvert { get; set; }
    }

    public class NyxVert188
    {
        public Element188[] elements { get; set; }
    }

    public class Element188
    {
        public NyxButtons188[] nyxbuttons { get; set; }
    }

    public class NyxButtons188
    {
        public string label { get; set; }
        public Command297[] commands { get; set; }
    }

    public class Command297
    {
        public Goto294 _goto { get; set; }
    }

    public class Goto294
    {
        public string target { get; set; }
    }

    public class T5slut
    {
        public NyxPage241 nyxpage { get; set; }
    }

    public class NyxPage241
    {
        public Media241 media { get; set; }
        public string text { get; set; }
        public Action237 action { get; set; }
    }

    public class Media241
    {
        public string nyximage { get; set; }
    }

    public class Action237
    {
        public NyxVert189 nyxvert { get; set; }
    }

    public class NyxVert189
    {
        public Element189[] elements { get; set; }
    }

    public class Element189
    {
        public NyxButtons189[] nyxbuttons { get; set; }
    }

    public class NyxButtons189
    {
        public string label { get; set; }
        public Command298[] commands { get; set; }
    }

    public class Command298
    {
        public Goto295 _goto { get; set; }
    }

    public class Goto295
    {
        public string target { get; set; }
    }

    public class T5toy
    {
        public NyxPage242 nyxpage { get; set; }
    }

    public class NyxPage242
    {
        public Media242 media { get; set; }
        public string text { get; set; }
        public Action238 action { get; set; }
    }

    public class Media242
    {
        public string nyximage { get; set; }
    }

    public class Action238
    {
        public NyxVert190 nyxvert { get; set; }
    }

    public class NyxVert190
    {
        public Element190[] elements { get; set; }
    }

    public class Element190
    {
        public NyxButtons190[] nyxbuttons { get; set; }
    }

    public class NyxButtons190
    {
        public string label { get; set; }
        public Command299[] commands { get; set; }
    }

    public class Command299
    {
        public Goto296 _goto { get; set; }
    }

    public class Goto296
    {
        public string target { get; set; }
    }

    public class T5win
    {
        public NyxPage243 nyxpage { get; set; }
    }

    public class NyxPage243
    {
        public Media243 media { get; set; }
        public string text { get; set; }
        public Action239 action { get; set; }
    }

    public class Media243
    {
        public string nyximage { get; set; }
    }

    public class Action239
    {
        public NyxVert191 nyxvert { get; set; }
    }

    public class NyxVert191
    {
        public Element191[] elements { get; set; }
    }

    public class Element191
    {
        public NyxButtons191[] nyxbuttons { get; set; }
    }

    public class NyxButtons191
    {
        public string label { get; set; }
        public Command300[] commands { get; set; }
    }

    public class Command300
    {
        public Goto297 _goto { get; set; }
    }

    public class Goto297
    {
        public string target { get; set; }
    }

    public class T6N1
    {
        public NyxPage244 nyxpage { get; set; }
    }

    public class NyxPage244
    {
        public Media244 media { get; set; }
        public string text { get; set; }
        public Action240 action { get; set; }
    }

    public class Media244
    {
        public string nyximage { get; set; }
    }

    public class Action240
    {
        public NyxVert192 nyxvert { get; set; }
    }

    public class NyxVert192
    {
        public Element192[] elements { get; set; }
    }

    public class Element192
    {
        public NyxButtons192[] nyxbuttons { get; set; }
    }

    public class NyxButtons192
    {
        public string label { get; set; }
        public Command301[] commands { get; set; }
    }

    public class Command301
    {
        public Goto298 _goto { get; set; }
    }

    public class Goto298
    {
        public string target { get; set; }
    }

    public class T6N10
    {
        public NyxPage245 nyxpage { get; set; }
    }

    public class NyxPage245
    {
        public Media245 media { get; set; }
        public string text { get; set; }
        public Action241 action { get; set; }
        public Instruc69 instruc { get; set; }
    }

    public class Media245
    {
        public string nyximage { get; set; }
    }

    public class Action241
    {
        public NyxVert193 nyxvert { get; set; }
    }

    public class NyxVert193
    {
        public Element193[] elements { get; set; }
    }

    public class Element193
    {
        public NyxButtons193[] nyxbuttons { get; set; }
    }

    public class NyxButtons193
    {
        public string label { get; set; }
        public Command302[] commands { get; set; }
    }

    public class Command302
    {
        public Goto299 _goto { get; set; }
    }

    public class Goto299
    {
        public string target { get; set; }
    }

    public class Instruc69
    {
        public NyxTimer109 nyxtimer { get; set; }
    }

    public class NyxTimer109
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command303[] commands { get; set; }
    }

    public class Command303
    {
        public Goto300 _goto { get; set; }
    }

    public class Goto300
    {
        public string target { get; set; }
    }

    public class T6N11
    {
        public NyxPage246 nyxpage { get; set; }
    }

    public class NyxPage246
    {
        public Media246 media { get; set; }
        public string text { get; set; }
        public Action242 action { get; set; }
        public Instruc70 instruc { get; set; }
    }

    public class Media246
    {
        public string nyximage { get; set; }
    }

    public class Action242
    {
        public NyxVert194 nyxvert { get; set; }
    }

    public class NyxVert194
    {
        public Element194[] elements { get; set; }
    }

    public class Element194
    {
        public NyxButtons194[] nyxbuttons { get; set; }
    }

    public class NyxButtons194
    {
        public string label { get; set; }
        public Command304[] commands { get; set; }
    }

    public class Command304
    {
        public Goto301 _goto { get; set; }
    }

    public class Goto301
    {
        public string target { get; set; }
    }

    public class Instruc70
    {
        public NyxTimer110 nyxtimer { get; set; }
    }

    public class NyxTimer110
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command305[] commands { get; set; }
    }

    public class Command305
    {
        public Goto302 _goto { get; set; }
    }

    public class Goto302
    {
        public string target { get; set; }
    }

    public class T6N12
    {
        public NyxPage247 nyxpage { get; set; }
    }

    public class NyxPage247
    {
        public Media247 media { get; set; }
        public string text { get; set; }
        public Action243 action { get; set; }
        public Instruc71 instruc { get; set; }
    }

    public class Media247
    {
        public string nyximage { get; set; }
    }

    public class Action243
    {
        public NyxVert195 nyxvert { get; set; }
    }

    public class NyxVert195
    {
        public Element195[] elements { get; set; }
    }

    public class Element195
    {
        public NyxButtons195[] nyxbuttons { get; set; }
    }

    public class NyxButtons195
    {
        public string label { get; set; }
        public Command306[] commands { get; set; }
    }

    public class Command306
    {
        public Goto303 _goto { get; set; }
    }

    public class Goto303
    {
        public string target { get; set; }
    }

    public class Instruc71
    {
        public NyxTimer111 nyxtimer { get; set; }
    }

    public class NyxTimer111
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command307[] commands { get; set; }
    }

    public class Command307
    {
        public Goto304 _goto { get; set; }
    }

    public class Goto304
    {
        public string target { get; set; }
    }

    public class T6N13
    {
        public NyxPage248 nyxpage { get; set; }
    }

    public class NyxPage248
    {
        public Media248 media { get; set; }
        public string text { get; set; }
        public Action244 action { get; set; }
        public Instruc72 instruc { get; set; }
    }

    public class Media248
    {
        public string nyximage { get; set; }
    }

    public class Action244
    {
        public NyxVert196 nyxvert { get; set; }
    }

    public class NyxVert196
    {
        public Element196[] elements { get; set; }
    }

    public class Element196
    {
        public NyxButtons196[] nyxbuttons { get; set; }
    }

    public class NyxButtons196
    {
        public string label { get; set; }
        public Command308[] commands { get; set; }
    }

    public class Command308
    {
        public Goto305 _goto { get; set; }
    }

    public class Goto305
    {
        public string target { get; set; }
    }

    public class Instruc72
    {
        public NyxTimer112 nyxtimer { get; set; }
    }

    public class NyxTimer112
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command309[] commands { get; set; }
    }

    public class Command309
    {
        public Goto306 _goto { get; set; }
    }

    public class Goto306
    {
        public string target { get; set; }
    }

    public class T6N14
    {
        public NyxPage249 nyxpage { get; set; }
    }

    public class NyxPage249
    {
        public Media249 media { get; set; }
        public string text { get; set; }
        public Action245 action { get; set; }
        public Instruc73 instruc { get; set; }
    }

    public class Media249
    {
        public string nyximage { get; set; }
    }

    public class Action245
    {
        public NyxVert197 nyxvert { get; set; }
    }

    public class NyxVert197
    {
        public Element197[] elements { get; set; }
    }

    public class Element197
    {
        public NyxButtons197[] nyxbuttons { get; set; }
    }

    public class NyxButtons197
    {
        public string label { get; set; }
        public Command310[] commands { get; set; }
    }

    public class Command310
    {
        public Goto307 _goto { get; set; }
    }

    public class Goto307
    {
        public string target { get; set; }
    }

    public class Instruc73
    {
        public NyxTimer113 nyxtimer { get; set; }
    }

    public class NyxTimer113
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command311[] commands { get; set; }
    }

    public class Command311
    {
        public Goto308 _goto { get; set; }
    }

    public class Goto308
    {
        public string target { get; set; }
    }

    public class T6N15
    {
        public NyxPage250 nyxpage { get; set; }
    }

    public class NyxPage250
    {
        public Media250 media { get; set; }
        public string text { get; set; }
        public Action246 action { get; set; }
        public Instruc74 instruc { get; set; }
    }

    public class Media250
    {
        public string nyximage { get; set; }
    }

    public class Action246
    {
        public NyxVert198 nyxvert { get; set; }
    }

    public class NyxVert198
    {
        public Element198[] elements { get; set; }
    }

    public class Element198
    {
        public NyxButtons198[] nyxbuttons { get; set; }
    }

    public class NyxButtons198
    {
        public string label { get; set; }
        public Command312[] commands { get; set; }
    }

    public class Command312
    {
        public Goto309 _goto { get; set; }
    }

    public class Goto309
    {
        public string target { get; set; }
    }

    public class Instruc74
    {
        public NyxTimer114 nyxtimer { get; set; }
    }

    public class NyxTimer114
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command313[] commands { get; set; }
    }

    public class Command313
    {
        public Goto310 _goto { get; set; }
    }

    public class Goto310
    {
        public string target { get; set; }
    }

    public class T6N16
    {
        public NyxPage251 nyxpage { get; set; }
    }

    public class NyxPage251
    {
        public Media251 media { get; set; }
        public string text { get; set; }
        public Action247 action { get; set; }
        public Instruc75 instruc { get; set; }
    }

    public class Media251
    {
        public string nyximage { get; set; }
    }

    public class Action247
    {
        public NyxVert199 nyxvert { get; set; }
    }

    public class NyxVert199
    {
        public Element199[] elements { get; set; }
    }

    public class Element199
    {
        public NyxButtons199[] nyxbuttons { get; set; }
    }

    public class NyxButtons199
    {
        public string label { get; set; }
        public Command314[] commands { get; set; }
    }

    public class Command314
    {
        public Goto311 _goto { get; set; }
    }

    public class Goto311
    {
        public string target { get; set; }
    }

    public class Instruc75
    {
        public NyxTimer115 nyxtimer { get; set; }
    }

    public class NyxTimer115
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command315[] commands { get; set; }
    }

    public class Command315
    {
        public Goto312 _goto { get; set; }
    }

    public class Goto312
    {
        public string target { get; set; }
    }

    public class T6N17
    {
        public NyxPage252 nyxpage { get; set; }
    }

    public class NyxPage252
    {
        public Media252 media { get; set; }
        public string text { get; set; }
        public Action248 action { get; set; }
        public Instruc76 instruc { get; set; }
    }

    public class Media252
    {
        public string nyximage { get; set; }
    }

    public class Action248
    {
        public NyxVert200 nyxvert { get; set; }
    }

    public class NyxVert200
    {
        public Element200[] elements { get; set; }
    }

    public class Element200
    {
        public NyxButtons200[] nyxbuttons { get; set; }
    }

    public class NyxButtons200
    {
        public string label { get; set; }
        public Command316[] commands { get; set; }
    }

    public class Command316
    {
        public Goto313 _goto { get; set; }
    }

    public class Goto313
    {
        public string target { get; set; }
    }

    public class Instruc76
    {
        public NyxTimer116 nyxtimer { get; set; }
    }

    public class NyxTimer116
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command317[] commands { get; set; }
    }

    public class Command317
    {
        public Goto314 _goto { get; set; }
    }

    public class Goto314
    {
        public string target { get; set; }
    }

    public class T6N18
    {
        public NyxPage253 nyxpage { get; set; }
    }

    public class NyxPage253
    {
        public Media253 media { get; set; }
        public string text { get; set; }
        public Action249 action { get; set; }
    }

    public class Media253
    {
        public string nyximage { get; set; }
    }

    public class Action249
    {
        public NyxVert201 nyxvert { get; set; }
    }

    public class NyxVert201
    {
        public Element201[] elements { get; set; }
    }

    public class Element201
    {
        public NyxButtons201[] nyxbuttons { get; set; }
    }

    public class NyxButtons201
    {
        public string label { get; set; }
        public Command318[] commands { get; set; }
    }

    public class Command318
    {
        public Goto315 _goto { get; set; }
    }

    public class Goto315
    {
        public string target { get; set; }
    }

    public class T6N19
    {
        public NyxPage254 nyxpage { get; set; }
    }

    public class NyxPage254
    {
        public Media254 media { get; set; }
        public string text { get; set; }
        public Action250 action { get; set; }
        public Instruc77 instruc { get; set; }
    }

    public class Media254
    {
        public string nyximage { get; set; }
    }

    public class Action250
    {
        public NyxVert202 nyxvert { get; set; }
    }

    public class NyxVert202
    {
        public Element202[] elements { get; set; }
    }

    public class Element202
    {
        public NyxButtons202[] nyxbuttons { get; set; }
    }

    public class NyxButtons202
    {
        public string label { get; set; }
        public Command319[] commands { get; set; }
    }

    public class Command319
    {
        public Goto316 _goto { get; set; }
    }

    public class Goto316
    {
        public string target { get; set; }
    }

    public class Instruc77
    {
        public NyxTimer117 nyxtimer { get; set; }
    }

    public class NyxTimer117
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command320[] commands { get; set; }
    }

    public class Command320
    {
        public Pcm2Range3 pcm2range { get; set; }
    }

    public class Pcm2Range3
    {
        public int to { get; set; }
        public int from { get; set; }
        public string prefix { get; set; }
    }

    public class T6N2
    {
        public NyxPage255 nyxpage { get; set; }
    }

    public class NyxPage255
    {
        public Media255 media { get; set; }
        public string text { get; set; }
        public Action251 action { get; set; }
        public Instruc78 instruc { get; set; }
    }

    public class Media255
    {
        public string nyximage { get; set; }
    }

    public class Action251
    {
        public NyxVert203 nyxvert { get; set; }
    }

    public class NyxVert203
    {
        public Element203[] elements { get; set; }
    }

    public class Element203
    {
        public NyxButtons203[] nyxbuttons { get; set; }
    }

    public class NyxButtons203
    {
        public string label { get; set; }
        public Command321[] commands { get; set; }
    }

    public class Command321
    {
        public Goto317 _goto { get; set; }
    }

    public class Goto317
    {
        public string target { get; set; }
    }

    public class Instruc78
    {
        public NyxTimer118 nyxtimer { get; set; }
    }

    public class NyxTimer118
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command322[] commands { get; set; }
    }

    public class Command322
    {
        public Goto318 _goto { get; set; }
    }

    public class Goto318
    {
        public string target { get; set; }
    }

    public class T6N3
    {
        public NyxPage256 nyxpage { get; set; }
    }

    public class NyxPage256
    {
        public Media256 media { get; set; }
        public string text { get; set; }
        public Action252 action { get; set; }
        public Instruc79 instruc { get; set; }
    }

    public class Media256
    {
        public string nyximage { get; set; }
    }

    public class Action252
    {
        public NyxVert204 nyxvert { get; set; }
    }

    public class NyxVert204
    {
        public Element204[] elements { get; set; }
    }

    public class Element204
    {
        public NyxButtons204[] nyxbuttons { get; set; }
    }

    public class NyxButtons204
    {
        public string label { get; set; }
        public Command323[] commands { get; set; }
    }

    public class Command323
    {
        public Goto319 _goto { get; set; }
    }

    public class Goto319
    {
        public string target { get; set; }
    }

    public class Instruc79
    {
        public NyxTimer119 nyxtimer { get; set; }
    }

    public class NyxTimer119
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command324[] commands { get; set; }
    }

    public class Command324
    {
        public Goto320 _goto { get; set; }
    }

    public class Goto320
    {
        public string target { get; set; }
    }

    public class T6N4
    {
        public NyxPage257 nyxpage { get; set; }
    }

    public class NyxPage257
    {
        public Media257 media { get; set; }
        public string text { get; set; }
        public Action253 action { get; set; }
        public Instruc80 instruc { get; set; }
    }

    public class Media257
    {
        public string nyximage { get; set; }
    }

    public class Action253
    {
        public NyxVert205 nyxvert { get; set; }
    }

    public class NyxVert205
    {
        public Element205[] elements { get; set; }
    }

    public class Element205
    {
        public NyxButtons205[] nyxbuttons { get; set; }
    }

    public class NyxButtons205
    {
        public string label { get; set; }
        public Command325[] commands { get; set; }
    }

    public class Command325
    {
        public Goto321 _goto { get; set; }
    }

    public class Goto321
    {
        public string target { get; set; }
    }

    public class Instruc80
    {
        public NyxTimer120 nyxtimer { get; set; }
    }

    public class NyxTimer120
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command326[] commands { get; set; }
    }

    public class Command326
    {
        public Goto322 _goto { get; set; }
    }

    public class Goto322
    {
        public string target { get; set; }
    }

    public class T6N5
    {
        public NyxPage258 nyxpage { get; set; }
    }

    public class NyxPage258
    {
        public Media258 media { get; set; }
        public string text { get; set; }
        public Action254 action { get; set; }
        public Instruc81 instruc { get; set; }
    }

    public class Media258
    {
        public string nyximage { get; set; }
    }

    public class Action254
    {
        public NyxVert206 nyxvert { get; set; }
    }

    public class NyxVert206
    {
        public Element206[] elements { get; set; }
    }

    public class Element206
    {
        public NyxButtons206[] nyxbuttons { get; set; }
    }

    public class NyxButtons206
    {
        public string label { get; set; }
        public Command327[] commands { get; set; }
    }

    public class Command327
    {
        public Goto323 _goto { get; set; }
    }

    public class Goto323
    {
        public string target { get; set; }
    }

    public class Instruc81
    {
        public NyxTimer121 nyxtimer { get; set; }
    }

    public class NyxTimer121
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command328[] commands { get; set; }
    }

    public class Command328
    {
        public Goto324 _goto { get; set; }
    }

    public class Goto324
    {
        public string target { get; set; }
    }

    public class T6N6
    {
        public NyxPage259 nyxpage { get; set; }
    }

    public class NyxPage259
    {
        public Media259 media { get; set; }
        public string text { get; set; }
        public Action255 action { get; set; }
        public Instruc82 instruc { get; set; }
    }

    public class Media259
    {
        public string nyximage { get; set; }
    }

    public class Action255
    {
        public NyxVert207 nyxvert { get; set; }
    }

    public class NyxVert207
    {
        public Element207[] elements { get; set; }
    }

    public class Element207
    {
        public NyxButtons207[] nyxbuttons { get; set; }
    }

    public class NyxButtons207
    {
        public string label { get; set; }
        public Command329[] commands { get; set; }
    }

    public class Command329
    {
        public Goto325 _goto { get; set; }
    }

    public class Goto325
    {
        public string target { get; set; }
    }

    public class Instruc82
    {
        public NyxTimer122 nyxtimer { get; set; }
    }

    public class NyxTimer122
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command330[] commands { get; set; }
    }

    public class Command330
    {
        public Goto326 _goto { get; set; }
    }

    public class Goto326
    {
        public string target { get; set; }
    }

    public class T6N7
    {
        public NyxPage260 nyxpage { get; set; }
    }

    public class NyxPage260
    {
        public Media260 media { get; set; }
        public string text { get; set; }
        public Action256 action { get; set; }
        public Instruc83 instruc { get; set; }
    }

    public class Media260
    {
        public string nyximage { get; set; }
    }

    public class Action256
    {
        public NyxVert208 nyxvert { get; set; }
    }

    public class NyxVert208
    {
        public Element208[] elements { get; set; }
    }

    public class Element208
    {
        public NyxButtons208[] nyxbuttons { get; set; }
    }

    public class NyxButtons208
    {
        public string label { get; set; }
        public Command331[] commands { get; set; }
    }

    public class Command331
    {
        public Goto327 _goto { get; set; }
    }

    public class Goto327
    {
        public string target { get; set; }
    }

    public class Instruc83
    {
        public NyxTimer123 nyxtimer { get; set; }
    }

    public class NyxTimer123
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command332[] commands { get; set; }
    }

    public class Command332
    {
        public Goto328 _goto { get; set; }
    }

    public class Goto328
    {
        public string target { get; set; }
    }

    public class T6N8
    {
        public NyxPage261 nyxpage { get; set; }
    }

    public class NyxPage261
    {
        public Media261 media { get; set; }
        public string text { get; set; }
        public Action257 action { get; set; }
        public Instruc84 instruc { get; set; }
    }

    public class Media261
    {
        public string nyximage { get; set; }
    }

    public class Action257
    {
        public NyxVert209 nyxvert { get; set; }
    }

    public class NyxVert209
    {
        public Element209[] elements { get; set; }
    }

    public class Element209
    {
        public NyxButtons209[] nyxbuttons { get; set; }
    }

    public class NyxButtons209
    {
        public string label { get; set; }
        public Command333[] commands { get; set; }
    }

    public class Command333
    {
        public Goto329 _goto { get; set; }
    }

    public class Goto329
    {
        public string target { get; set; }
    }

    public class Instruc84
    {
        public NyxTimer124 nyxtimer { get; set; }
    }

    public class NyxTimer124
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command334[] commands { get; set; }
    }

    public class Command334
    {
        public Goto330 _goto { get; set; }
    }

    public class Goto330
    {
        public string target { get; set; }
    }

    public class T6N9
    {
        public NyxPage262 nyxpage { get; set; }
    }

    public class NyxPage262
    {
        public Media262 media { get; set; }
        public string text { get; set; }
        public Action258 action { get; set; }
        public Instruc85 instruc { get; set; }
    }

    public class Media262
    {
        public string nyximage { get; set; }
    }

    public class Action258
    {
        public NyxVert210 nyxvert { get; set; }
    }

    public class NyxVert210
    {
        public Element210[] elements { get; set; }
    }

    public class Element210
    {
        public NyxButtons210[] nyxbuttons { get; set; }
    }

    public class NyxButtons210
    {
        public string label { get; set; }
        public Command335[] commands { get; set; }
    }

    public class Command335
    {
        public Goto331 _goto { get; set; }
    }

    public class Goto331
    {
        public string target { get; set; }
    }

    public class Instruc85
    {
        public NyxTimer125 nyxtimer { get; set; }
    }

    public class NyxTimer125
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command336[] commands { get; set; }
    }

    public class Command336
    {
        public Goto332 _goto { get; set; }
    }

    public class Goto332
    {
        public string target { get; set; }
    }

    public class T6slut
    {
        public NyxPage263 nyxpage { get; set; }
    }

    public class NyxPage263
    {
        public Media263 media { get; set; }
        public string text { get; set; }
        public Action259 action { get; set; }
    }

    public class Media263
    {
        public string nyximage { get; set; }
    }

    public class Action259
    {
        public NyxVert211 nyxvert { get; set; }
    }

    public class NyxVert211
    {
        public Element211[] elements { get; set; }
    }

    public class Element211
    {
        public NyxButtons211[] nyxbuttons { get; set; }
    }

    public class NyxButtons211
    {
        public string label { get; set; }
        public Command337[] commands { get; set; }
    }

    public class Command337
    {
        public Goto333 _goto { get; set; }
    }

    public class Goto333
    {
        public string target { get; set; }
    }

    public class T6toy
    {
        public NyxPage264 nyxpage { get; set; }
    }

    public class NyxPage264
    {
        public Media264 media { get; set; }
        public string text { get; set; }
        public Action260 action { get; set; }
    }

    public class Media264
    {
        public string nyximage { get; set; }
    }

    public class Action260
    {
        public NyxVert212 nyxvert { get; set; }
    }

    public class NyxVert212
    {
        public Element212[] elements { get; set; }
    }

    public class Element212
    {
        public NyxButtons212[] nyxbuttons { get; set; }
    }

    public class NyxButtons212
    {
        public string label { get; set; }
        public Command338[] commands { get; set; }
    }

    public class Command338
    {
        public Goto334 _goto { get; set; }
    }

    public class Goto334
    {
        public string target { get; set; }
    }

    public class T7A1
    {
        public NyxPage265 nyxpage { get; set; }
    }

    public class NyxPage265
    {
        public Media265 media { get; set; }
        public string text { get; set; }
        public Action261 action { get; set; }
    }

    public class Media265
    {
        public string nyximage { get; set; }
    }

    public class Action261
    {
        public NyxVert213 nyxvert { get; set; }
    }

    public class NyxVert213
    {
        public Element213[] elements { get; set; }
    }

    public class Element213
    {
        public NyxButtons213[] nyxbuttons { get; set; }
    }

    public class NyxButtons213
    {
        public string label { get; set; }
        public Command339[] commands { get; set; }
    }

    public class Command339
    {
        public Goto335 _goto { get; set; }
    }

    public class Goto335
    {
        public string target { get; set; }
    }

    public class T7A2
    {
        public NyxPage266 nyxpage { get; set; }
    }

    public class NyxPage266
    {
        public Media266 media { get; set; }
        public string text { get; set; }
        public Action262 action { get; set; }
    }

    public class Media266
    {
        public string nyximage { get; set; }
    }

    public class Action262
    {
        public NyxVert214 nyxvert { get; set; }
    }

    public class NyxVert214
    {
        public Element214[] elements { get; set; }
    }

    public class Element214
    {
        public NyxButtons214[] nyxbuttons { get; set; }
    }

    public class NyxButtons214
    {
        public string label { get; set; }
        public Command340[] commands { get; set; }
    }

    public class Command340
    {
        public Goto336 _goto { get; set; }
    }

    public class Goto336
    {
        public string target { get; set; }
    }

    public class T7A3
    {
        public NyxPage267 nyxpage { get; set; }
    }

    public class NyxPage267
    {
        public Media267 media { get; set; }
        public string text { get; set; }
        public Action263 action { get; set; }
    }

    public class Media267
    {
        public string nyximage { get; set; }
    }

    public class Action263
    {
        public NyxVert215 nyxvert { get; set; }
    }

    public class NyxVert215
    {
        public Element215[] elements { get; set; }
    }

    public class Element215
    {
        public NyxButtons215[] nyxbuttons { get; set; }
    }

    public class NyxButtons215
    {
        public string label { get; set; }
        public Command341[] commands { get; set; }
    }

    public class Command341
    {
        public Goto337 _goto { get; set; }
    }

    public class Goto337
    {
        public string target { get; set; }
    }

    public class T7A4
    {
        public NyxPage268 nyxpage { get; set; }
    }

    public class NyxPage268
    {
        public Media268 media { get; set; }
        public string text { get; set; }
        public Action264 action { get; set; }
    }

    public class Media268
    {
        public string nyximage { get; set; }
    }

    public class Action264
    {
        public NyxVert216 nyxvert { get; set; }
    }

    public class NyxVert216
    {
        public Element216[] elements { get; set; }
    }

    public class Element216
    {
        public NyxButtons216[] nyxbuttons { get; set; }
    }

    public class NyxButtons216
    {
        public string label { get; set; }
        public Command342[] commands { get; set; }
    }

    public class Command342
    {
        public Goto338 _goto { get; set; }
    }

    public class Goto338
    {
        public string target { get; set; }
    }

    public class T7B1
    {
        public NyxPage269 nyxpage { get; set; }
    }

    public class NyxPage269
    {
        public Media269 media { get; set; }
        public string text { get; set; }
        public Action265 action { get; set; }
    }

    public class Media269
    {
        public string nyximage { get; set; }
    }

    public class Action265
    {
        public NyxVert217 nyxvert { get; set; }
    }

    public class NyxVert217
    {
        public Element217[] elements { get; set; }
    }

    public class Element217
    {
        public NyxButtons217[] nyxbuttons { get; set; }
    }

    public class NyxButtons217
    {
        public string label { get; set; }
        public Command343[] commands { get; set; }
    }

    public class Command343
    {
        public Goto339 _goto { get; set; }
    }

    public class Goto339
    {
        public string target { get; set; }
    }

    public class T7B2
    {
        public NyxPage270 nyxpage { get; set; }
    }

    public class NyxPage270
    {
        public Media270 media { get; set; }
        public string text { get; set; }
        public Action266 action { get; set; }
    }

    public class Media270
    {
        public string nyximage { get; set; }
    }

    public class Action266
    {
        public NyxVert218 nyxvert { get; set; }
    }

    public class NyxVert218
    {
        public Element218[] elements { get; set; }
    }

    public class Element218
    {
        public NyxButtons218[] nyxbuttons { get; set; }
    }

    public class NyxButtons218
    {
        public string label { get; set; }
        public Command344[] commands { get; set; }
    }

    public class Command344
    {
        public Goto340 _goto { get; set; }
    }

    public class Goto340
    {
        public string target { get; set; }
    }

    public class T7B3
    {
        public NyxPage271 nyxpage { get; set; }
    }

    public class NyxPage271
    {
        public Media271 media { get; set; }
        public string text { get; set; }
        public Action267 action { get; set; }
    }

    public class Media271
    {
        public string nyximage { get; set; }
    }

    public class Action267
    {
        public NyxVert219 nyxvert { get; set; }
    }

    public class NyxVert219
    {
        public Element219[] elements { get; set; }
    }

    public class Element219
    {
        public NyxButtons219[] nyxbuttons { get; set; }
    }

    public class NyxButtons219
    {
        public string label { get; set; }
        public Command345[] commands { get; set; }
    }

    public class Command345
    {
        public Goto341 _goto { get; set; }
    }

    public class Goto341
    {
        public string target { get; set; }
    }

    public class T7B4
    {
        public NyxPage272 nyxpage { get; set; }
    }

    public class NyxPage272
    {
        public Media272 media { get; set; }
        public string text { get; set; }
        public Action268 action { get; set; }
    }

    public class Media272
    {
        public string nyximage { get; set; }
    }

    public class Action268
    {
        public NyxVert220 nyxvert { get; set; }
    }

    public class NyxVert220
    {
        public Element220[] elements { get; set; }
    }

    public class Element220
    {
        public NyxButtons220[] nyxbuttons { get; set; }
    }

    public class NyxButtons220
    {
        public string label { get; set; }
        public Command346[] commands { get; set; }
    }

    public class Command346
    {
        public Goto342 _goto { get; set; }
    }

    public class Goto342
    {
        public string target { get; set; }
    }

    public class T7C1
    {
        public NyxPage273 nyxpage { get; set; }
    }

    public class NyxPage273
    {
        public Media273 media { get; set; }
        public string text { get; set; }
        public Action269 action { get; set; }
    }

    public class Media273
    {
        public string nyximage { get; set; }
    }

    public class Action269
    {
        public NyxVert221 nyxvert { get; set; }
    }

    public class NyxVert221
    {
        public Element221[] elements { get; set; }
    }

    public class Element221
    {
        public NyxButtons221[] nyxbuttons { get; set; }
    }

    public class NyxButtons221
    {
        public string label { get; set; }
        public Command347[] commands { get; set; }
    }

    public class Command347
    {
        public Goto343 _goto { get; set; }
    }

    public class Goto343
    {
        public string target { get; set; }
    }

    public class T7C2
    {
        public NyxPage274 nyxpage { get; set; }
    }

    public class NyxPage274
    {
        public Media274 media { get; set; }
        public string text { get; set; }
        public Action270 action { get; set; }
    }

    public class Media274
    {
        public string nyximage { get; set; }
    }

    public class Action270
    {
        public NyxVert222 nyxvert { get; set; }
    }

    public class NyxVert222
    {
        public Element222[] elements { get; set; }
    }

    public class Element222
    {
        public NyxButtons222[] nyxbuttons { get; set; }
    }

    public class NyxButtons222
    {
        public string label { get; set; }
        public Command348[] commands { get; set; }
    }

    public class Command348
    {
        public Goto344 _goto { get; set; }
    }

    public class Goto344
    {
        public string target { get; set; }
    }

    public class T7C3
    {
        public NyxPage275 nyxpage { get; set; }
    }

    public class NyxPage275
    {
        public Media275 media { get; set; }
        public string text { get; set; }
        public Action271 action { get; set; }
    }

    public class Media275
    {
        public string nyximage { get; set; }
    }

    public class Action271
    {
        public NyxVert223 nyxvert { get; set; }
    }

    public class NyxVert223
    {
        public Element223[] elements { get; set; }
    }

    public class Element223
    {
        public NyxButtons223[] nyxbuttons { get; set; }
    }

    public class NyxButtons223
    {
        public string label { get; set; }
        public Command349[] commands { get; set; }
    }

    public class Command349
    {
        public Goto345 _goto { get; set; }
    }

    public class Goto345
    {
        public string target { get; set; }
    }

    public class T7C4
    {
        public NyxPage276 nyxpage { get; set; }
    }

    public class NyxPage276
    {
        public Media276 media { get; set; }
        public string text { get; set; }
        public Action272 action { get; set; }
    }

    public class Media276
    {
        public string nyximage { get; set; }
    }

    public class Action272
    {
        public NyxVert224 nyxvert { get; set; }
    }

    public class NyxVert224
    {
        public Element224[] elements { get; set; }
    }

    public class Element224
    {
        public NyxButtons224[] nyxbuttons { get; set; }
    }

    public class NyxButtons224
    {
        public string label { get; set; }
        public Command350[] commands { get; set; }
    }

    public class Command350
    {
        public Goto346 _goto { get; set; }
    }

    public class Goto346
    {
        public string target { get; set; }
    }

    public class T7D1
    {
        public NyxPage277 nyxpage { get; set; }
    }

    public class NyxPage277
    {
        public Media277 media { get; set; }
        public string text { get; set; }
        public Action273 action { get; set; }
    }

    public class Media277
    {
        public string nyximage { get; set; }
    }

    public class Action273
    {
        public NyxVert225 nyxvert { get; set; }
    }

    public class NyxVert225
    {
        public Element225[] elements { get; set; }
    }

    public class Element225
    {
        public NyxButtons225[] nyxbuttons { get; set; }
    }

    public class NyxButtons225
    {
        public string label { get; set; }
        public Command351[] commands { get; set; }
    }

    public class Command351
    {
        public Goto347 _goto { get; set; }
    }

    public class Goto347
    {
        public string target { get; set; }
    }

    public class T7D2
    {
        public NyxPage278 nyxpage { get; set; }
    }

    public class NyxPage278
    {
        public Media278 media { get; set; }
        public string text { get; set; }
        public Action274 action { get; set; }
    }

    public class Media278
    {
        public string nyximage { get; set; }
    }

    public class Action274
    {
        public NyxVert226 nyxvert { get; set; }
    }

    public class NyxVert226
    {
        public Element226[] elements { get; set; }
    }

    public class Element226
    {
        public NyxButtons226[] nyxbuttons { get; set; }
    }

    public class NyxButtons226
    {
        public string label { get; set; }
        public Command352[] commands { get; set; }
    }

    public class Command352
    {
        public Goto348 _goto { get; set; }
    }

    public class Goto348
    {
        public string target { get; set; }
    }

    public class T7D3
    {
        public NyxPage279 nyxpage { get; set; }
    }

    public class NyxPage279
    {
        public Media279 media { get; set; }
        public string text { get; set; }
        public Action275 action { get; set; }
    }

    public class Media279
    {
        public string nyximage { get; set; }
    }

    public class Action275
    {
        public NyxVert227 nyxvert { get; set; }
    }

    public class NyxVert227
    {
        public Element227[] elements { get; set; }
    }

    public class Element227
    {
        public NyxButtons227[] nyxbuttons { get; set; }
    }

    public class NyxButtons227
    {
        public string label { get; set; }
        public Command353[] commands { get; set; }
    }

    public class Command353
    {
        public Goto349 _goto { get; set; }
    }

    public class Goto349
    {
        public string target { get; set; }
    }

    public class T7D4
    {
        public NyxPage280 nyxpage { get; set; }
    }

    public class NyxPage280
    {
        public Media280 media { get; set; }
        public string text { get; set; }
        public Action276 action { get; set; }
    }

    public class Media280
    {
        public string nyximage { get; set; }
    }

    public class Action276
    {
        public NyxVert228 nyxvert { get; set; }
    }

    public class NyxVert228
    {
        public Element228[] elements { get; set; }
    }

    public class Element228
    {
        public NyxButtons228[] nyxbuttons { get; set; }
    }

    public class NyxButtons228
    {
        public string label { get; set; }
        public Command354[] commands { get; set; }
    }

    public class Command354
    {
        public Goto350 _goto { get; set; }
    }

    public class Goto350
    {
        public string target { get; set; }
    }

    public class T7E1
    {
        public NyxPage281 nyxpage { get; set; }
    }

    public class NyxPage281
    {
        public Media281 media { get; set; }
        public string text { get; set; }
        public Action277 action { get; set; }
    }

    public class Media281
    {
        public string nyximage { get; set; }
    }

    public class Action277
    {
        public NyxVert229 nyxvert { get; set; }
    }

    public class NyxVert229
    {
        public Element229[] elements { get; set; }
    }

    public class Element229
    {
        public NyxButtons229[] nyxbuttons { get; set; }
    }

    public class NyxButtons229
    {
        public string label { get; set; }
        public Command355[] commands { get; set; }
    }

    public class Command355
    {
        public Goto351 _goto { get; set; }
    }

    public class Goto351
    {
        public string target { get; set; }
    }

    public class T7E2
    {
        public NyxPage282 nyxpage { get; set; }
    }

    public class NyxPage282
    {
        public Media282 media { get; set; }
        public string text { get; set; }
        public Action278 action { get; set; }
    }

    public class Media282
    {
        public string nyximage { get; set; }
    }

    public class Action278
    {
        public NyxTimer126 nyxtimer { get; set; }
    }

    public class NyxTimer126
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command356[] commands { get; set; }
    }

    public class Command356
    {
        public Pcm2Range4 pcm2range { get; set; }
    }

    public class Pcm2Range4
    {
        public int to { get; set; }
        public int from { get; set; }
        public string prefix { get; set; }
    }

    public class T7finish
    {
        public NyxPage283 nyxpage { get; set; }
    }

    public class NyxPage283
    {
        public Media283 media { get; set; }
        public string text { get; set; }
        public Action279 action { get; set; }
    }

    public class Media283
    {
        public string nyximage { get; set; }
    }

    public class Action279
    {
        public NyxVert230 nyxvert { get; set; }
    }

    public class NyxVert230
    {
        public Element230[] elements { get; set; }
    }

    public class Element230
    {
        public NyxButtons230[] nyxbuttons { get; set; }
    }

    public class NyxButtons230
    {
        public string label { get; set; }
        public Command357[] commands { get; set; }
    }

    public class Command357
    {
        public Goto352 _goto { get; set; }
    }

    public class Goto352
    {
        public string target { get; set; }
    }

    public class T8C1
    {
        public NyxPage284 nyxpage { get; set; }
    }

    public class NyxPage284
    {
        public Media284 media { get; set; }
        public string text { get; set; }
        public Action280 action { get; set; }
    }

    public class Media284
    {
        public string nyximage { get; set; }
    }

    public class Action280
    {
        public NyxVert231 nyxvert { get; set; }
    }

    public class NyxVert231
    {
        public Element231[] elements { get; set; }
    }

    public class Element231
    {
        public NyxButtons231[] nyxbuttons { get; set; }
    }

    public class NyxButtons231
    {
        public string label { get; set; }
        public Command358[] commands { get; set; }
    }

    public class Command358
    {
        public Goto353 _goto { get; set; }
    }

    public class Goto353
    {
        public string target { get; set; }
    }

    public class T8C2
    {
        public NyxPage285 nyxpage { get; set; }
    }

    public class NyxPage285
    {
        public Media285 media { get; set; }
        public string text { get; set; }
        public Action281 action { get; set; }
    }

    public class Media285
    {
        public string nyximage { get; set; }
    }

    public class Action281
    {
        public NyxTimer127 nyxtimer { get; set; }
    }

    public class NyxTimer127
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command359[] commands { get; set; }
    }

    public class Command359
    {
        public Goto354 _goto { get; set; }
    }

    public class Goto354
    {
        public string target { get; set; }
    }

    public class T8C3
    {
        public NyxPage286 nyxpage { get; set; }
    }

    public class NyxPage286
    {
        public Media286 media { get; set; }
        public string text { get; set; }
        public Action282 action { get; set; }
    }

    public class Media286
    {
        public string nyximage { get; set; }
    }

    public class Action282
    {
        public NyxVert232 nyxvert { get; set; }
    }

    public class NyxVert232
    {
        public Element232[] elements { get; set; }
    }

    public class Element232
    {
        public NyxButtons232[] nyxbuttons { get; set; }
    }

    public class NyxButtons232
    {
        public string label { get; set; }
        public Command360[] commands { get; set; }
    }

    public class Command360
    {
        public Goto355 _goto { get; set; }
    }

    public class Goto355
    {
        public string target { get; set; }
    }

    public class T8C4
    {
        public NyxPage287 nyxpage { get; set; }
    }

    public class NyxPage287
    {
        public Media287 media { get; set; }
        public string text { get; set; }
        public Action283 action { get; set; }
    }

    public class Media287
    {
        public string nyximage { get; set; }
    }

    public class Action283
    {
        public NyxTimer128 nyxtimer { get; set; }
    }

    public class NyxTimer128
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command361[] commands { get; set; }
    }

    public class Command361
    {
        public Goto356 _goto { get; set; }
    }

    public class Goto356
    {
        public string target { get; set; }
    }

    public class T8C5
    {
        public NyxPage288 nyxpage { get; set; }
    }

    public class NyxPage288
    {
        public Media288 media { get; set; }
        public string text { get; set; }
        public Action284 action { get; set; }
    }

    public class Media288
    {
        public string nyximage { get; set; }
    }

    public class Action284
    {
        public NyxVert233 nyxvert { get; set; }
    }

    public class NyxVert233
    {
        public Element233[] elements { get; set; }
    }

    public class Element233
    {
        public NyxButtons233[] nyxbuttons { get; set; }
    }

    public class NyxButtons233
    {
        public string label { get; set; }
        public Command362[] commands { get; set; }
    }

    public class Command362
    {
        public Goto357 _goto { get; set; }
    }

    public class Goto357
    {
        public string target { get; set; }
    }

    public class T8C6
    {
        public NyxPage289 nyxpage { get; set; }
    }

    public class NyxPage289
    {
        public Media289 media { get; set; }
        public string text { get; set; }
        public Action285 action { get; set; }
    }

    public class Media289
    {
        public string nyximage { get; set; }
    }

    public class Action285
    {
        public NyxVert234 nyxvert { get; set; }
    }

    public class NyxVert234
    {
        public Element234[] elements { get; set; }
    }

    public class Element234
    {
        public NyxButtons234[] nyxbuttons { get; set; }
    }

    public class NyxButtons234
    {
        public string label { get; set; }
        public Command363[] commands { get; set; }
    }

    public class Command363
    {
        public Goto358 _goto { get; set; }
    }

    public class Goto358
    {
        public string target { get; set; }
    }

    public class T8C7
    {
        public NyxPage290 nyxpage { get; set; }
    }

    public class NyxPage290
    {
        public Media290 media { get; set; }
        public string text { get; set; }
        public Action286 action { get; set; }
    }

    public class Media290
    {
        public string nyximage { get; set; }
    }

    public class Action286
    {
        public NyxTimer129 nyxtimer { get; set; }
    }

    public class NyxTimer129
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command364[] commands { get; set; }
    }

    public class Command364
    {
        public Pcm2Range5 pcm2range { get; set; }
    }

    public class Pcm2Range5
    {
        public int to { get; set; }
        public int from { get; set; }
        public string prefix { get; set; }
    }

    public class T8P2
    {
        public NyxPage291 nyxpage { get; set; }
    }

    public class NyxPage291
    {
        public Media291 media { get; set; }
        public string text { get; set; }
        public Action287 action { get; set; }
    }

    public class Media291
    {
        public string nyximage { get; set; }
    }

    public class Action287
    {
        public NyxVert235 nyxvert { get; set; }
    }

    public class NyxVert235
    {
        public Element235[] elements { get; set; }
    }

    public class Element235
    {
        public NyxButtons235[] nyxbuttons { get; set; }
    }

    public class NyxButtons235
    {
        public string label { get; set; }
        public Command365[] commands { get; set; }
    }

    public class Command365
    {
        public Goto359 _goto { get; set; }
    }

    public class Goto359
    {
        public string target { get; set; }
    }

    public class T8P3
    {
        public NyxPage292 nyxpage { get; set; }
    }

    public class NyxPage292
    {
        public Media292 media { get; set; }
        public string text { get; set; }
        public Action288 action { get; set; }
    }

    public class Media292
    {
        public string nyximage { get; set; }
    }

    public class Action288
    {
        public NyxVert236 nyxvert { get; set; }
    }

    public class NyxVert236
    {
        public Element236[] elements { get; set; }
    }

    public class Element236
    {
        public NyxButtons236[] nyxbuttons { get; set; }
    }

    public class NyxButtons236
    {
        public string label { get; set; }
        public Command366[] commands { get; set; }
    }

    public class Command366
    {
        public Goto360 _goto { get; set; }
    }

    public class Goto360
    {
        public string target { get; set; }
    }

    public class T8P4
    {
        public NyxPage293 nyxpage { get; set; }
    }

    public class NyxPage293
    {
        public Media293 media { get; set; }
        public string text { get; set; }
        public Action289 action { get; set; }
    }

    public class Media293
    {
        public string nyximage { get; set; }
    }

    public class Action289
    {
        public NyxVert237 nyxvert { get; set; }
    }

    public class NyxVert237
    {
        public Element237[] elements { get; set; }
    }

    public class Element237
    {
        public NyxButtons237[] nyxbuttons { get; set; }
    }

    public class NyxButtons237
    {
        public string label { get; set; }
        public Command367[] commands { get; set; }
    }

    public class Command367
    {
        public Goto361 _goto { get; set; }
    }

    public class Goto361
    {
        public string target { get; set; }
    }

    public class T8P4B
    {
        public NyxPage294 nyxpage { get; set; }
    }

    public class NyxPage294
    {
        public Media294 media { get; set; }
        public string text { get; set; }
        public Action290 action { get; set; }
    }

    public class Media294
    {
        public string nyximage { get; set; }
    }

    public class Action290
    {
        public NyxVert238 nyxvert { get; set; }
    }

    public class NyxVert238
    {
        public Element238[] elements { get; set; }
    }

    public class Element238
    {
        public NyxButtons238[] nyxbuttons { get; set; }
    }

    public class NyxButtons238
    {
        public string label { get; set; }
        public Command368[] commands { get; set; }
    }

    public class Command368
    {
        public Goto362 _goto { get; set; }
    }

    public class Goto362
    {
        public string target { get; set; }
    }

    public class T8P4C
    {
        public NyxPage295 nyxpage { get; set; }
    }

    public class NyxPage295
    {
        public Media295 media { get; set; }
        public string text { get; set; }
        public Action291 action { get; set; }
    }

    public class Media295
    {
        public string nyximage { get; set; }
    }

    public class Action291
    {
        public NyxVert239 nyxvert { get; set; }
    }

    public class NyxVert239
    {
        public Element239[] elements { get; set; }
    }

    public class Element239
    {
        public NyxButtons239[] nyxbuttons { get; set; }
    }

    public class NyxButtons239
    {
        public string label { get; set; }
        public Command369[] commands { get; set; }
    }

    public class Command369
    {
        public Goto363 _goto { get; set; }
    }

    public class Goto363
    {
        public string target { get; set; }
    }

    public class T8P4D
    {
        public NyxPage296 nyxpage { get; set; }
    }

    public class NyxPage296
    {
        public Media296 media { get; set; }
        public string text { get; set; }
        public Action292 action { get; set; }
    }

    public class Media296
    {
        public string nyximage { get; set; }
    }

    public class Action292
    {
        public NyxVert240 nyxvert { get; set; }
    }

    public class NyxVert240
    {
        public Element240[] elements { get; set; }
    }

    public class Element240
    {
        public NyxButtons240[] nyxbuttons { get; set; }
    }

    public class NyxButtons240
    {
        public string label { get; set; }
        public Command370[] commands { get; set; }
    }

    public class Command370
    {
        public Goto364 _goto { get; set; }
    }

    public class Goto364
    {
        public string target { get; set; }
    }

    public class T8P4E
    {
        public NyxPage297 nyxpage { get; set; }
    }

    public class NyxPage297
    {
        public Media297 media { get; set; }
        public string text { get; set; }
        public Action293 action { get; set; }
    }

    public class Media297
    {
        public string nyximage { get; set; }
    }

    public class Action293
    {
        public NyxVert241 nyxvert { get; set; }
    }

    public class NyxVert241
    {
        public Element241[] elements { get; set; }
    }

    public class Element241
    {
        public NyxButtons241[] nyxbuttons { get; set; }
    }

    public class NyxButtons241
    {
        public string label { get; set; }
        public Command371[] commands { get; set; }
    }

    public class Command371
    {
        public Goto365 _goto { get; set; }
    }

    public class Goto365
    {
        public string target { get; set; }
    }

    public class T8P4F
    {
        public NyxPage298 nyxpage { get; set; }
    }

    public class NyxPage298
    {
        public Media298 media { get; set; }
        public string text { get; set; }
        public Action294 action { get; set; }
    }

    public class Media298
    {
        public string nyximage { get; set; }
    }

    public class Action294
    {
        public NyxVert242 nyxvert { get; set; }
    }

    public class NyxVert242
    {
        public Element242[] elements { get; set; }
    }

    public class Element242
    {
        public NyxButtons242[] nyxbuttons { get; set; }
    }

    public class NyxButtons242
    {
        public string label { get; set; }
        public Command372[] commands { get; set; }
    }

    public class Command372
    {
        public Goto366 _goto { get; set; }
    }

    public class Goto366
    {
        public string target { get; set; }
    }

    public class T8P4G
    {
        public NyxPage299 nyxpage { get; set; }
    }

    public class NyxPage299
    {
        public Media299 media { get; set; }
        public string text { get; set; }
        public Action295 action { get; set; }
    }

    public class Media299
    {
        public string nyximage { get; set; }
    }

    public class Action295
    {
        public NyxVert243 nyxvert { get; set; }
    }

    public class NyxVert243
    {
        public Element243[] elements { get; set; }
    }

    public class Element243
    {
        public NyxButtons243[] nyxbuttons { get; set; }
    }

    public class NyxButtons243
    {
        public string label { get; set; }
        public Command373[] commands { get; set; }
    }

    public class Command373
    {
        public Goto367 _goto { get; set; }
    }

    public class Goto367
    {
        public string target { get; set; }
    }

    public class T8P5
    {
        public NyxPage300 nyxpage { get; set; }
    }

    public class NyxPage300
    {
        public Media300 media { get; set; }
        public string text { get; set; }
        public Action296 action { get; set; }
    }

    public class Media300
    {
        public string nyximage { get; set; }
    }

    public class Action296
    {
        public NyxVert244 nyxvert { get; set; }
    }

    public class NyxVert244
    {
        public Element244[] elements { get; set; }
    }

    public class Element244
    {
        public NyxButtons244[] nyxbuttons { get; set; }
    }

    public class NyxButtons244
    {
        public string label { get; set; }
        public Command374[] commands { get; set; }
    }

    public class Command374
    {
        public Goto368 _goto { get; set; }
    }

    public class Goto368
    {
        public string target { get; set; }
    }

    public class T8p5a
    {
        public NyxPage301 nyxpage { get; set; }
    }

    public class NyxPage301
    {
        public Media301 media { get; set; }
        public string text { get; set; }
        public Action297 action { get; set; }
    }

    public class Media301
    {
        public string nyximage { get; set; }
    }

    public class Action297
    {
        public NyxVert245 nyxvert { get; set; }
    }

    public class NyxVert245
    {
        public Element245[] elements { get; set; }
    }

    public class Element245
    {
        public NyxButtons245[] nyxbuttons { get; set; }
    }

    public class NyxButtons245
    {
        public string label { get; set; }
        public Command375[] commands { get; set; }
    }

    public class Command375
    {
        public Goto369 _goto { get; set; }
    }

    public class Goto369
    {
        public string target { get; set; }
    }

    public class T8p5b
    {
        public NyxPage302 nyxpage { get; set; }
    }

    public class NyxPage302
    {
        public Media302 media { get; set; }
        public string text { get; set; }
        public Action298 action { get; set; }
        public Instruc86 instruc { get; set; }
    }

    public class Media302
    {
        public string nyximage { get; set; }
    }

    public class Action298
    {
        public NyxVert246 nyxvert { get; set; }
    }

    public class NyxVert246
    {
        public Element246[] elements { get; set; }
    }

    public class Element246
    {
        public NyxButtons246[] nyxbuttons { get; set; }
    }

    public class NyxButtons246
    {
        public string label { get; set; }
        public Command376[] commands { get; set; }
    }

    public class Command376
    {
        public Goto370 _goto { get; set; }
    }

    public class Goto370
    {
        public string target { get; set; }
    }

    public class Instruc86
    {
        public NyxTimer130 nyxtimer { get; set; }
    }

    public class NyxTimer130
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command377[] commands { get; set; }
    }

    public class Command377
    {
        public Goto371 _goto { get; set; }
    }

    public class Goto371
    {
        public string target { get; set; }
    }

    public class T9slut
    {
        public NyxPage303 nyxpage { get; set; }
    }

    public class NyxPage303
    {
        public Media303 media { get; set; }
        public string text { get; set; }
        public Action299 action { get; set; }
    }

    public class Media303
    {
        public string nyximage { get; set; }
    }

    public class Action299
    {
        public NyxVert247 nyxvert { get; set; }
    }

    public class NyxVert247
    {
        public Element247[] elements { get; set; }
    }

    public class Element247
    {
        public NyxButtons247[] nyxbuttons { get; set; }
    }

    public class NyxButtons247
    {
        public string label { get; set; }
        public Command378[] commands { get; set; }
    }

    public class Command378
    {
        public Goto372 _goto { get; set; }
    }

    public class Goto372
    {
        public string target { get; set; }
    }

    public class _22
    {
        public NyxPage304 nyxpage { get; set; }
    }

    public class NyxPage304
    {
        public Media304 media { get; set; }
        public string text { get; set; }
        public Action300 action { get; set; }
    }

    public class Media304
    {
        public string nyximage { get; set; }
    }

    public class Action300
    {
        public NyxTimer131 nyxtimer { get; set; }
    }

    public class NyxTimer131
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command379[] commands { get; set; }
    }

    public class Command379
    {
        public Pcm2Range6 pcm2range { get; set; }
    }

    public class Pcm2Range6
    {
        public int to { get; set; }
        public int from { get; set; }
        public string prefix { get; set; }
    }

    public class _23
    {
        public NyxPage305 nyxpage { get; set; }
    }

    public class NyxPage305
    {
        public Media305 media { get; set; }
        public string text { get; set; }
        public Action301 action { get; set; }
        public Hidden9 hidden { get; set; }
        public Instruc87 instruc { get; set; }
    }

    public class Media305
    {
        public string nyximage { get; set; }
    }

    public class Action301
    {
        public NyxTimer132 nyxtimer { get; set; }
    }

    public class NyxTimer132
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command380[] commands { get; set; }
    }

    public class Command380
    {
        public Goto373 _goto { get; set; }
    }

    public class Goto373
    {
        public string target { get; set; }
    }

    public class Hidden9
    {
        public Pcm2Unset8 pcm2unset { get; set; }
    }

    public class Pcm2Unset8
    {
        public string[] pages { get; set; }
    }

    public class Instruc87
    {
        public Pcm2Set8 pcm2set { get; set; }
    }

    public class Pcm2Set8
    {
        public string[] pages { get; set; }
    }

    public class _24
    {
        public NyxPage306 nyxpage { get; set; }
    }

    public class NyxPage306
    {
        public Media306 media { get; set; }
        public string text { get; set; }
        public Action302 action { get; set; }
        public Hidden10 hidden { get; set; }
        public Instruc88 instruc { get; set; }
    }

    public class Media306
    {
        public string nyximage { get; set; }
    }

    public class Action302
    {
        public NyxTimer133 nyxtimer { get; set; }
    }

    public class NyxTimer133
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command381[] commands { get; set; }
    }

    public class Command381
    {
        public Goto374 _goto { get; set; }
    }

    public class Goto374
    {
        public string target { get; set; }
    }

    public class Hidden10
    {
        public Pcm2Unset9 pcm2unset { get; set; }
    }

    public class Pcm2Unset9
    {
        public string[] pages { get; set; }
    }

    public class Instruc88
    {
        public Pcm2Set9 pcm2set { get; set; }
    }

    public class Pcm2Set9
    {
        public string[] pages { get; set; }
    }

    public class _25
    {
        public NyxPage307 nyxpage { get; set; }
    }

    public class NyxPage307
    {
        public Media307 media { get; set; }
        public string text { get; set; }
        public Action303 action { get; set; }
        public Hidden11 hidden { get; set; }
        public Instruc89 instruc { get; set; }
    }

    public class Media307
    {
        public string nyximage { get; set; }
    }

    public class Action303
    {
        public NyxTimer134 nyxtimer { get; set; }
    }

    public class NyxTimer134
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command382[] commands { get; set; }
    }

    public class Command382
    {
        public Goto375 _goto { get; set; }
    }

    public class Goto375
    {
        public string target { get; set; }
    }

    public class Hidden11
    {
        public Pcm2Unset10 pcm2unset { get; set; }
    }

    public class Pcm2Unset10
    {
        public string[] pages { get; set; }
    }

    public class Instruc89
    {
        public Pcm2Set10 pcm2set { get; set; }
    }

    public class Pcm2Set10
    {
        public string[] pages { get; set; }
    }

    public class _26
    {
        public NyxPage308 nyxpage { get; set; }
    }

    public class NyxPage308
    {
        public Media308 media { get; set; }
        public string text { get; set; }
        public Action304 action { get; set; }
        public Hidden12 hidden { get; set; }
        public Instruc90 instruc { get; set; }
    }

    public class Media308
    {
        public string nyximage { get; set; }
    }

    public class Action304
    {
        public NyxTimer135 nyxtimer { get; set; }
    }

    public class NyxTimer135
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command383[] commands { get; set; }
    }

    public class Command383
    {
        public Goto376 _goto { get; set; }
    }

    public class Goto376
    {
        public string target { get; set; }
    }

    public class Hidden12
    {
        public Pcm2Unset11 pcm2unset { get; set; }
    }

    public class Pcm2Unset11
    {
        public string[] pages { get; set; }
    }

    public class Instruc90
    {
        public Pcm2Set11 pcm2set { get; set; }
    }

    public class Pcm2Set11
    {
        public string[] pages { get; set; }
    }

    public class _27
    {
        public NyxPage309 nyxpage { get; set; }
    }

    public class NyxPage309
    {
        public Media309 media { get; set; }
        public string text { get; set; }
        public Action305 action { get; set; }
        public Hidden13 hidden { get; set; }
        public Instruc91 instruc { get; set; }
    }

    public class Media309
    {
        public string nyximage { get; set; }
    }

    public class Action305
    {
        public NyxTimer136 nyxtimer { get; set; }
    }

    public class NyxTimer136
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command384[] commands { get; set; }
    }

    public class Command384
    {
        public Goto377 _goto { get; set; }
    }

    public class Goto377
    {
        public string target { get; set; }
    }

    public class Hidden13
    {
        public Pcm2Unset12 pcm2unset { get; set; }
    }

    public class Pcm2Unset12
    {
        public string[] pages { get; set; }
    }

    public class Instruc91
    {
        public Pcm2Set12 pcm2set { get; set; }
    }

    public class Pcm2Set12
    {
        public string[] pages { get; set; }
    }

    public class _28
    {
        public NyxPage310 nyxpage { get; set; }
    }

    public class NyxPage310
    {
        public Media310 media { get; set; }
        public string text { get; set; }
        public Action306 action { get; set; }
        public Hidden14 hidden { get; set; }
        public Instruc92 instruc { get; set; }
    }

    public class Media310
    {
        public string nyximage { get; set; }
    }

    public class Action306
    {
        public NyxTimer137 nyxtimer { get; set; }
    }

    public class NyxTimer137
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command385[] commands { get; set; }
    }

    public class Command385
    {
        public Goto378 _goto { get; set; }
    }

    public class Goto378
    {
        public string target { get; set; }
    }

    public class Hidden14
    {
        public Pcm2Unset13 pcm2unset { get; set; }
    }

    public class Pcm2Unset13
    {
        public string[] pages { get; set; }
    }

    public class Instruc92
    {
        public Pcm2Set13 pcm2set { get; set; }
    }

    public class Pcm2Set13
    {
        public string[] pages { get; set; }
    }

    public class _29
    {
        public NyxPage311 nyxpage { get; set; }
    }

    public class NyxPage311
    {
        public Media311 media { get; set; }
        public string text { get; set; }
        public Action307 action { get; set; }
        public Hidden15 hidden { get; set; }
        public Instruc93 instruc { get; set; }
    }

    public class Media311
    {
        public string nyximage { get; set; }
    }

    public class Action307
    {
        public NyxTimer138 nyxtimer { get; set; }
    }

    public class NyxTimer138
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command386[] commands { get; set; }
    }

    public class Command386
    {
        public Goto379 _goto { get; set; }
    }

    public class Goto379
    {
        public string target { get; set; }
    }

    public class Hidden15
    {
        public Pcm2Unset14 pcm2unset { get; set; }
    }

    public class Pcm2Unset14
    {
        public string[] pages { get; set; }
    }

    public class Instruc93
    {
        public Pcm2Set14 pcm2set { get; set; }
    }

    public class Pcm2Set14
    {
        public string[] pages { get; set; }
    }

    public class _30
    {
        public NyxPage312 nyxpage { get; set; }
    }

    public class NyxPage312
    {
        public Media312 media { get; set; }
        public string text { get; set; }
        public Action308 action { get; set; }
        public Hidden16 hidden { get; set; }
        public Instruc94 instruc { get; set; }
    }

    public class Media312
    {
        public string nyximage { get; set; }
    }

    public class Action308
    {
        public NyxTimer139 nyxtimer { get; set; }
    }

    public class NyxTimer139
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command387[] commands { get; set; }
    }

    public class Command387
    {
        public Goto380 _goto { get; set; }
    }

    public class Goto380
    {
        public string target { get; set; }
    }

    public class Hidden16
    {
        public Pcm2Unset15 pcm2unset { get; set; }
    }

    public class Pcm2Unset15
    {
        public string[] pages { get; set; }
    }

    public class Instruc94
    {
        public Pcm2Set15 pcm2set { get; set; }
    }

    public class Pcm2Set15
    {
        public string[] pages { get; set; }
    }

    public class _31
    {
        public NyxPage313 nyxpage { get; set; }
    }

    public class NyxPage313
    {
        public Media313 media { get; set; }
        public string text { get; set; }
        public Action309 action { get; set; }
        public Hidden17 hidden { get; set; }
        public Instruc95 instruc { get; set; }
    }

    public class Media313
    {
        public string nyximage { get; set; }
    }

    public class Action309
    {
        public NyxTimer140 nyxtimer { get; set; }
    }

    public class NyxTimer140
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command388[] commands { get; set; }
    }

    public class Command388
    {
        public Goto381 _goto { get; set; }
    }

    public class Goto381
    {
        public string target { get; set; }
    }

    public class Hidden17
    {
        public Pcm2Unset16 pcm2unset { get; set; }
    }

    public class Pcm2Unset16
    {
        public string[] pages { get; set; }
    }

    public class Instruc95
    {
        public Pcm2Set16 pcm2set { get; set; }
    }

    public class Pcm2Set16
    {
        public string[] pages { get; set; }
    }

    public class _32
    {
        public NyxPage314 nyxpage { get; set; }
    }

    public class NyxPage314
    {
        public Media314 media { get; set; }
        public string text { get; set; }
        public Action310 action { get; set; }
        public Hidden18 hidden { get; set; }
        public Instruc96 instruc { get; set; }
    }

    public class Media314
    {
        public string nyximage { get; set; }
    }

    public class Action310
    {
        public NyxTimer141 nyxtimer { get; set; }
    }

    public class NyxTimer141
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command389[] commands { get; set; }
    }

    public class Command389
    {
        public Goto382 _goto { get; set; }
    }

    public class Goto382
    {
        public string target { get; set; }
    }

    public class Hidden18
    {
        public Pcm2Unset17 pcm2unset { get; set; }
    }

    public class Pcm2Unset17
    {
        public string[] pages { get; set; }
    }

    public class Instruc96
    {
        public Pcm2Set17 pcm2set { get; set; }
    }

    public class Pcm2Set17
    {
        public string[] pages { get; set; }
    }

    public class _33
    {
        public NyxPage315 nyxpage { get; set; }
    }

    public class NyxPage315
    {
        public Media315 media { get; set; }
        public string text { get; set; }
        public Action311 action { get; set; }
        public Hidden19 hidden { get; set; }
        public Instruc97 instruc { get; set; }
    }

    public class Media315
    {
        public string nyximage { get; set; }
    }

    public class Action311
    {
        public NyxTimer142 nyxtimer { get; set; }
    }

    public class NyxTimer142
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command390[] commands { get; set; }
    }

    public class Command390
    {
        public Goto383 _goto { get; set; }
    }

    public class Goto383
    {
        public string target { get; set; }
    }

    public class Hidden19
    {
        public Pcm2Unset18 pcm2unset { get; set; }
    }

    public class Pcm2Unset18
    {
        public string[] pages { get; set; }
    }

    public class Instruc97
    {
        public Pcm2Set18 pcm2set { get; set; }
    }

    public class Pcm2Set18
    {
        public string[] pages { get; set; }
    }

    public class _34
    {
        public NyxPage316 nyxpage { get; set; }
    }

    public class NyxPage316
    {
        public Media316 media { get; set; }
        public string text { get; set; }
        public Action312 action { get; set; }
    }

    public class Media316
    {
        public string nyximage { get; set; }
    }

    public class Action312
    {
        public NyxTimer143 nyxtimer { get; set; }
    }

    public class NyxTimer143
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command391[] commands { get; set; }
    }

    public class Command391
    {
        public Pcm2Range7 pcm2range { get; set; }
    }

    public class Pcm2Range7
    {
        public int to { get; set; }
        public int from { get; set; }
        public string prefix { get; set; }
    }

    public class _35
    {
        public NyxPage317 nyxpage { get; set; }
    }

    public class NyxPage317
    {
        public Media317 media { get; set; }
        public string text { get; set; }
        public Action313 action { get; set; }
        public Hidden20 hidden { get; set; }
        public Instruc98 instruc { get; set; }
    }

    public class Media317
    {
        public string nyximage { get; set; }
    }

    public class Action313
    {
        public NyxTimer144 nyxtimer { get; set; }
    }

    public class NyxTimer144
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command392[] commands { get; set; }
    }

    public class Command392
    {
        public Goto384 _goto { get; set; }
    }

    public class Goto384
    {
        public string target { get; set; }
    }

    public class Hidden20
    {
        public Pcm2Unset19 pcm2unset { get; set; }
    }

    public class Pcm2Unset19
    {
        public string[] pages { get; set; }
    }

    public class Instruc98
    {
        public Pcm2Set19 pcm2set { get; set; }
    }

    public class Pcm2Set19
    {
        public string[] pages { get; set; }
    }

    public class _36
    {
        public NyxPage318 nyxpage { get; set; }
    }

    public class NyxPage318
    {
        public Media318 media { get; set; }
        public string text { get; set; }
        public Action314 action { get; set; }
        public Hidden21 hidden { get; set; }
        public Instruc99 instruc { get; set; }
    }

    public class Media318
    {
        public string nyximage { get; set; }
    }

    public class Action314
    {
        public NyxTimer145 nyxtimer { get; set; }
    }

    public class NyxTimer145
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command393[] commands { get; set; }
    }

    public class Command393
    {
        public Goto385 _goto { get; set; }
    }

    public class Goto385
    {
        public string target { get; set; }
    }

    public class Hidden21
    {
        public Pcm2Unset20 pcm2unset { get; set; }
    }

    public class Pcm2Unset20
    {
        public string[] pages { get; set; }
    }

    public class Instruc99
    {
        public Pcm2Set20 pcm2set { get; set; }
    }

    public class Pcm2Set20
    {
        public string[] pages { get; set; }
    }

    public class _37
    {
        public NyxPage319 nyxpage { get; set; }
    }

    public class NyxPage319
    {
        public Media319 media { get; set; }
        public string text { get; set; }
        public Action315 action { get; set; }
        public Hidden22 hidden { get; set; }
        public Instruc100 instruc { get; set; }
    }

    public class Media319
    {
        public string nyximage { get; set; }
    }

    public class Action315
    {
        public NyxTimer146 nyxtimer { get; set; }
    }

    public class NyxTimer146
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command394[] commands { get; set; }
    }

    public class Command394
    {
        public Goto386 _goto { get; set; }
    }

    public class Goto386
    {
        public string target { get; set; }
    }

    public class Hidden22
    {
        public Pcm2Unset21 pcm2unset { get; set; }
    }

    public class Pcm2Unset21
    {
        public string[] pages { get; set; }
    }

    public class Instruc100
    {
        public Pcm2Set21 pcm2set { get; set; }
    }

    public class Pcm2Set21
    {
        public string[] pages { get; set; }
    }

    public class _38
    {
        public NyxPage320 nyxpage { get; set; }
    }

    public class NyxPage320
    {
        public Media320 media { get; set; }
        public string text { get; set; }
        public Action316 action { get; set; }
        public Hidden23 hidden { get; set; }
        public Instruc101 instruc { get; set; }
    }

    public class Media320
    {
        public string nyximage { get; set; }
    }

    public class Action316
    {
        public NyxTimer147 nyxtimer { get; set; }
    }

    public class NyxTimer147
    {
        public string duration { get; set; }
        public string style { get; set; }
        public Command395[] commands { get; set; }
    }

    public class Command395
    {
        public Goto387 _goto { get; set; }
    }

    public class Goto387
    {
        public string target { get; set; }
    }

    public class Hidden23
    {
        public Pcm2Unset22 pcm2unset { get; set; }
    }

    public class Pcm2Unset22
    {
        public string[] pages { get; set; }
    }

    public class Instruc101
    {
        public Pcm2Set22 pcm2set { get; set; }
    }

    public class Pcm2Set22
    {
        public string[] pages { get; set; }
    }

    public class Child
    {
        public object[] nyxdummy { get; set; }
    }

}
