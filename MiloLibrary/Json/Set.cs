﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Set
    {
        [JsonProperty("pages")]
        public string[] Pages { get; set; }
    }
}
