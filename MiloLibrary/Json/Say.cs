﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Say
    {
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("mode")]
        public string Mode { get; set; }
        [JsonProperty("duration")]
        public string Duration { get; set; }
        [JsonProperty("allowSkip")]
        public bool AllowSkip { get; set; }
    }
}
