﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class NyxVert
    {
        [JsonProperty("elements")]
        public Element[] Elements { get; set; }
    }
}
