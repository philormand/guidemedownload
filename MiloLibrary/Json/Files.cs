﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Files
    {
        [JsonProperty("file")]
        public EosFile[] File { get; set; }
    }



}
