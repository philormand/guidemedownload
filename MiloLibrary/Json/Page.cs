﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Page
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("audio.play")]
        public AudioPlay Audioplay { get; set; }
        [JsonProperty("eosUi")]
        public EosUI[] EosUis { get; set; }
        [JsonProperty("choice")]
        public Choice Choice { get; set; }
        [JsonProperty("timer")]
        public Timer[] Timers { get; set; }
        [JsonProperty("goto")]
        public EosGoto EosGoto { get; set; }
        [JsonProperty("nyx.page")]
        public NyxPage NyxPage { get; set; }

    }



}
