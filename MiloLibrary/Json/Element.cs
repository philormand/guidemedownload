﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Element
    {
        [JsonProperty("nyx.buttons")]
        public NyxButton[] Nyxbuttons { get; set; }
        [JsonProperty("nyx.timer")]
        public NyxTimer NyxTimer { get; set; }
    }
}
