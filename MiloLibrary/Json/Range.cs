﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Range
    {
        [JsonProperty("to")]
        public int To { get; set; }
        [JsonProperty("from")]
        public int From { get; set; }
        [JsonProperty("prefix")]
        public string Prefix { get; set; }
    }
}
