﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Command
    {
        [JsonProperty("goto")]
        public EosGoto EosGoto { get; set; }
        [JsonProperty("image")]
        public Image Image { get; set; }
        [JsonProperty("say")]
        public Say Say { get; set; }
        [JsonProperty("choice")]
        public Choice Choice { get; set; }
        [JsonProperty("pcm2.range")]
        public Range Range { get; set; }

    }

}
