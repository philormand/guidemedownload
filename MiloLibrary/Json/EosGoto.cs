﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class EosGoto
    {
        [JsonProperty("target")]
        public string Target { get; set; }
    }



}
