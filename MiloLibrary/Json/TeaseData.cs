﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MiloLibrary.Json
{
    public class TeaseData
    {
        public TeaseData()
        {
            Pages = new List<Page>();
            Galleries = new List<Gallery>();
            Files = new List<EosFile>();
        }

        [JsonProperty("authorId")]
        public string AuthorId { get; set; }
        [JsonProperty("authorName")]
        public string AuthorName { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("teaseUrl")]
        public Uri TeaseUrl { get; set; }
        [JsonProperty("teaseId")]
        public string TeaseId { get; set; }
        [JsonProperty("mediaDirectory")]
        public string MediaDirectory { get; set; }
        [JsonProperty("files")]
        public List<EosFile> Files { get; }
        [JsonProperty("pages")]
        public List<Page> Pages { get;  }
        [JsonProperty("galleries")]
        public List<Gallery> Galleries { get; }
    }
}
