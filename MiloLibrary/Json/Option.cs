﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Option
    {
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("commands")]
        public Command[] Commands { get; set; }
        [JsonProperty("color")]
        public string Color { get; set; }
    }
}
