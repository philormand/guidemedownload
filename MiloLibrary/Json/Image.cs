﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Image
    {
        [JsonProperty("locator")]
        public string Locator { get; set; }
    }
}
