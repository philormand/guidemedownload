﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Media
    {
        [JsonProperty("nyx.image")]
        public string Nyximage { get; set; }
    }

}
