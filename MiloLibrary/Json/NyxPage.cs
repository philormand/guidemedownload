﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class NyxPage
    {
        [JsonProperty("media")]
        public Media Media { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("action")]
        public Action Action { get; set; }
        [JsonProperty("hidden")]
        public Hidden Hidden { get; set; }
        [JsonProperty("instruc")]
        public Instruc Instruc { get; set; }
    }

}
