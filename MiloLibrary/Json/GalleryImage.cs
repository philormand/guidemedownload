﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class GalleryImage
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("hash")]
        public string Hash { get; set; }
        [JsonProperty("size")]
        public int Size { get; set; }
        [JsonProperty("width")]
        public int Width { get; set; }
        [JsonProperty("height")]
        public int Height { get; set; }
    }



}
