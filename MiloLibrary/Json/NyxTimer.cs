﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiloLibrary.Json
{
    public class NyxTimer
    {
        [JsonProperty("duration")]
        public string Duration { get; set; }
        [JsonProperty("style")]
        public string Style { get; set; }
        [JsonProperty("commands")]
        public Command[] Commands { get; set; }
    }

}
