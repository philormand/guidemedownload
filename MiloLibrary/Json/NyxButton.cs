﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class NyxButton
    {
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("commands")]
        public Command[] Commands { get; set; }
    }

}
