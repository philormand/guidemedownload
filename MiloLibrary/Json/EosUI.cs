﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class EosUI
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("mode")]
        public string Mode { get; set; }
        [JsonProperty("duration")]
        public string Duration { get; set; }
        [JsonProperty("allowSkip")]
        public bool AllowSkip { get; set; }
        [JsonProperty("locator")]
        public string Locator { get; set; }
        [JsonProperty("target")]
        public string Target { get; set; }
    }
}
