﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Timer
    {
        [JsonProperty("duration")]
        public string Duration { get; set; }
        [JsonProperty("style")]
        public string Style { get; set; }
        [JsonProperty("isAsync")]
        public bool IsAsync { get; set; }
        [JsonProperty("commands")]
        public Command[] Commands { get; set; }
    }



}
