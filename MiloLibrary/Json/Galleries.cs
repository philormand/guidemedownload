﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Galleries
    {
        [JsonProperty("gallery")]
        public Gallery Gallery { get; set; }
    }



}
