﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Unset
    {
        [JsonProperty("pages")]
        public string[] Pages { get; set; }
    }

}
