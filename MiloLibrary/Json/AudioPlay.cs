﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class AudioPlay
    {
        [JsonProperty("locator")]
        public string Locator { get; set; }
        [JsonProperty("volume")]
        public float Volume { get; set; }
    }



}
