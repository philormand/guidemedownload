﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Action
    {
        [JsonProperty("nyx.vert")]
        public NyxVert NyxVert { get; set; }
        [JsonProperty("nyx.timer")]
        public NyxTimer NyxTimer { get; set; }
        [JsonProperty("nyx.buttons")]
        public NyxButton[] NyxButtons { get; set; }
        [JsonProperty("audio.play")]
        public AudioPlay AudioPlay { get; set; }
    }
}
