﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Instruc
    {
        [JsonProperty("nyx.timer")]
        public NyxTimer nyxtimer { get; set; }
        [JsonProperty("nyx.buttons")]
        public NyxButton[] NyxButtons { get; set; }
        [JsonProperty("pcm2.set")]
        public Set Set { get; set; }
        [JsonProperty("pcm2.unset")]
        public Unset Unset { get; set; }
    }
}
