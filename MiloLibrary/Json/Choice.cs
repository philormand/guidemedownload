﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Choice
    {
        [JsonProperty("options")]
        public Option[] Options { get; set; }
    }
}
