﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Hidden
    {
        [JsonProperty("audio.play")]
        public AudioPlay Audioplay { get; set; }
        [JsonProperty("pcm2.set")]
        public Set Set { get; set; }
        [JsonProperty("pcm2.unset")]
        public Unset Unset { get; set; }
    }

}
