﻿using Newtonsoft.Json;

namespace MiloLibrary.Json
{
    public class Gallery
    {
        [JsonProperty("guid")]
        public string Guid { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("images")]
        public GalleryImage[] Images { get; set; }
    }



}
