﻿using MiloLibrary.Helpers.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace MiloDnl
{
    public class ConsoleApplication
    {
        private readonly IEosToGuideMe _eosToGuideMe;
        private readonly IHtmlToGuideMe _htmlToGuideMe;
        private readonly ILogger<ConsoleApplication> _logger;
        private readonly IMiloLibraryConfig _libConfig;

        public ConsoleApplication(ILogger<ConsoleApplication> logger, IEosToGuideMe eosToGuideMe, IHtmlToGuideMe htmlToGuideMe, IMiloLibraryConfig libConfig)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eosToGuideMe = eosToGuideMe ?? throw new ArgumentNullException(nameof(eosToGuideMe));
            _htmlToGuideMe = htmlToGuideMe ?? throw new ArgumentNullException(nameof(htmlToGuideMe));
            _libConfig = libConfig ?? throw new ArgumentNullException(nameof(libConfig));
        }

        public async Task Run(Uri miloUri)
        {
            if (miloUri == null) throw new ArgumentNullException(nameof(miloUri));

            try
            {
                await _eosToGuideMe.GetTease(miloUri, _libConfig.SaveDirectory).ConfigureAwait(false);
                await _htmlToGuideMe.GetTease(miloUri, _libConfig.SaveDirectory).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                Exception ex1 = ex;
                while (ex1 != null)
                {
                    _logger.LogError($"Exception {ex1.GetType().ToString()} Message {ex1.Message} Trace {ex1.StackTrace}");
                    ex1 = ex1.InnerException;
                }
            }
            //add delay to allow logs to be written
            await Task.Delay(250).ConfigureAwait(false);
        }
    }
}
