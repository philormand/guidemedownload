﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiloDnl
{
    public class MiloDnlConfig : IMiloDnlConfig
    {
        public int Timeout { get; set; }
        public string LogLevel { get; set; }

    }
}
