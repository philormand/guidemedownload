﻿using MiloLibrary.Helpers;
using MiloLibrary.Helpers.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Net.Http;

namespace MiloDnl
{
    public static class InjectorSetup
    {
        public static IServiceCollection ConfigureServices(IConfiguration config)
        {
            if (config == null) throw new ArgumentNullException(nameof(config));

            IServiceCollection services = new ServiceCollection();

            IMiloDnlConfig appConfig = config.GetSection("MiloDnl").Get<MiloDnlConfig>();
            services.AddSingleton(appConfig);


            services.AddLogging(logging =>
            {
                logging.AddConfiguration(config.GetSection("Logging"));
                logging.AddLog4Net();
                logging.AddConsole();
            }).Configure<LoggerFilterOptions>(options => options.MinLevel =
                                              GetLogLevel(appConfig.LogLevel));

            services.AddSingleton(config);
            IMiloLibraryConfig libConfig = config.GetSection("MiloLibrary").Get<MiloLibraryConfig>();
            services.AddSingleton(libConfig);

            services.AddSingleton<IEosParser, EosParser>();
            services.AddHttpClient<IEosParser, EosParser>(client =>
            {
                client.Timeout = TimeSpan.FromSeconds(appConfig.Timeout);
            }).AddPolicyHandler(GetRetryPolicy());

            services.AddSingleton<IMediaDownloader, MediaDownloader>();
            services.AddHttpClient<IMediaDownloader, MediaDownloader>(client =>
            {
                client.Timeout = TimeSpan.FromSeconds(appConfig.Timeout);
            }).AddPolicyHandler(GetRetryPolicy());

            services.AddSingleton<IEosToGuideMe, EosToGuideMe>();
            services.AddHttpClient<IEosToGuideMe, EosToGuideMe>(client =>
            {
                client.Timeout = TimeSpan.FromSeconds(appConfig.Timeout);
            }).AddPolicyHandler(GetRetryPolicy());

            services.AddSingleton<IHtmlToGuideMe, HtmlToGuideMe>();
            services.AddHttpClient<IHtmlToGuideMe, HtmlToGuideMe>(client =>
            {
                client.Timeout = TimeSpan.FromSeconds(appConfig.Timeout);
            }).AddPolicyHandler(GetRetryPolicy());

            services.AddSingleton<ConsoleApplication>();

            return services;
        }

        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .RetryAsync(3);
        }

        private static LogLevel GetLogLevel(string level)
        {
            if (Enum.TryParse(level, true, out LogLevel logLevel))
            {
                return logLevel;
            }
            return LogLevel.Information;
        }
    }
}
