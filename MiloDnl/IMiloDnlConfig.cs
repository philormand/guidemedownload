﻿namespace MiloDnl
{
    public interface IMiloDnlConfig
    {
        int Timeout { get; set; }
        string LogLevel { get; set; }
    }
}