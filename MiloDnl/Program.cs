﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Threading.Tasks;

namespace MiloDnl
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length > 0)
                {
                    Uri MiloUri = new Uri(args[0]);
                    var config = LoadConfiguration();
                    IMiloDnlConfig appConfig = config.GetSection("MiloDnl").Get<MiloDnlConfig>();

                    var services = InjectorSetup.ConfigureServices(config);
                    var serviceProvider = services.BuildServiceProvider();
                    Task task = serviceProvider.GetService<ConsoleApplication>().Run(MiloUri);
                    task.Wait();
                }
                else
                {
                    Console.WriteLine(@"Error: Need to pass the url for the Milo tease");
                }
            }
            catch (Exception ex)
            {
                Exception ex1 = ex;
                while (ex1 != null)
                {
                    Console.WriteLine($"Exception {ex1.GetType().ToString()} Message {ex1.Message} Trace {ex1.StackTrace}");
                    ex1 = ex1.InnerException;
                }
            }
            Console.WriteLine(@"Press Enter to continue");
            Console.ReadLine();
        }

        public static IConfiguration LoadConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true,
                             reloadOnChange: true);
            return builder.Build();
        }


    }
}
